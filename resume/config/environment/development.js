module.exports = {
  // Mongodb
  mongo: {
    uri: 'mongodb://localhost:27017/portfolio-dev',
    options: {
      db: {
        safe: true
      }
    }
  },
  // Server Port
  port: 5010,
  // Server IP
  ip: '127.0.0.1',
  corsDomain: '*',
  env: 'development',
  jwt_secret: 'portfolio-dev'
};
  