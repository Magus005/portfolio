const gulp = require('gulp');
const babel = require('gulp-babel');
const nodemon = require('nodemon');
const mocha = require('gulp-mocha');
const gulpLoadPlugins = require('gulp-load-plugins'); 

var plugins = gulpLoadPlugins();

function onServerLog(log) {
  console.log('[nodemon]' +
    log.message
  );
}


gulp.task('start:dev', () => {
  process.env.NODE_ENV = 'development';
  config = require(`./config/environment`);
  nodemon(`-w ./index.js`).on(
    'log',
    onServerLog
  );
});

gulp.task('start:prod', () => {
  process.env.NODE_ENV = 'production';
  config = require(`./config/environment`);
  nodemon(`-w ./index.js`).on(
    'log',
    onServerLog
  );
});

gulp.task('start:test', () => {
    process.env.NODE_ENV = 'test';
    config = require(`./config/environment`);
    gulp.src(['./tests/index.js'], {read: false})
      .pipe(mocha({reporter: 'list', exit: true}))
      .on('error', console.error)
  }
);