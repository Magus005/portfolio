'use strict';
// Transpile all code following this line with babel and use 'env' (aka ES6) preset.

// Set default node environment to development
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if(env === 'development') {
  require('babel-register')({
    presets: ['env']
  })
  require('babel-polyfill')
}

// Import the rest of our application.
module.exports = require('./app.js')
