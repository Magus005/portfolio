const CenterOfInterest = `
  type CenterOfInterest {
    _id: String
    name: String
    icon: String
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [CenterOfInterest];

export const typeResolvers = {

};
