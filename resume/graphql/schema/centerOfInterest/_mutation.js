import logger from '../../../components/logger'
import CenterOfInterest from './model';
const Mutation = `
  extend type Mutation {
    createCenterOfInterest(centerOfInterest: InputCenterOfInterest): CenterOfInterest,
    updateCenterOfInterest(id: ID!, centerOfInterest: InputCenterOfInterest): CenterOfInterest,
    deleteCenterOfInterest(id: ID!): CenterOfInterest
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createCenterOfInterest: async (parent, { centerOfInterest }) => {
      logger.info(`--MUTATION.CENTEROFINTEREST.CREATECENTEROFINTEREST-- object centerOfInterest = ${JSON.stringify(centerOfInterest)}`)
      let centerOfInterestModel = new CenterOfInterest(centerOfInterest);
      return await centerOfInterestModel.save();
    },
    updateCenterOfInterest: async (parent, { id, centerOfInterest }) => {
      logger.info(`--MUTATION.CENTEROFINTEREST.UPDATECENTEROFINTEREST-- id centerOfInterest = ${id}`);
      logger.info(`--MUTATION.CENTEROFINTEREST.UPDATECENTEROFINTEREST-- object centerOfInterest = ${JSON.stringify(centerOfInterest)}`);
      let data = await CenterOfInterest.findOneAndUpdate({_id: id}, centerOfInterest, {new: true});
      if(!data) {
        logger.info(`--MUTATION.CENTEROFINTEREST.UPDATECENTEROFINTEREST-- response object = ${data}`);
        return null;
      }
      return data;
    },
    deleteCenterOfInterest: async (parent, {id}) => {
      logger.info(`--MUTATION.CENTEROFINTEREST.DELETECENTEROFINTEREST-- id centerOfInterest = ${id}`);
      let data = await CenterOfInterest.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.CENTEROFINTEREST.DELETECENTEROFINTEREST-- response object = ${data}`);
        return null;
      }
      logger.info(`--MUTATION.CENTEROFINTEREST.DELETECENTEROFINTEREST-- response object = ${data}`);
      return data;
    }
  }
};
