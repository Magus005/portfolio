const Input = `
  input InputCenterOfInterest {
    name: String
    icon: String
    state: Boolean
  }
`;

export default () => [Input];
