import logger from '../../../components/logger'
import CenterOfInterest from './model'
const Query = `
  extend type Query {
    getAllCenterOfInterests: [CenterOfInterest],
    getCenterOfInterest(id: ID!): CenterOfInterest
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllCenterOfInterests: async () => {
      return await CenterOfInterest.find();
    },
    getCenterOfInterest: async (parent, { id }) => {
      logger.info(`--QUERY.CENTEROFINTEREST.GETCENTEROFINTEREST-- id centerOfInterest = ${id}`);
      let data = await CenterOfInterest.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.CENTEROFINTEREST.GETCENTEROFINTEREST-- response object = ${data}`);
      return data
    }
  }
};
