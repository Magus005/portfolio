'use strict';
import mongoose from 'mongoose';

var CenterOfInterestModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  icon: {
    type: String,
    required: true,
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

export default mongoose.model('CenterOfInterest', CenterOfInterestModel);
