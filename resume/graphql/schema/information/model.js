'use strict';
import mongoose from 'mongoose';

var InformationModel = new mongoose.Schema({
  fullname: {
    type: String,
    required: true
  },
  dateOfBirth: {
    type: Date,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

InformationModel.index({
  title: 1
});
export default mongoose.model('Information', InformationModel);
