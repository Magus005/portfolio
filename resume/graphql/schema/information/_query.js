import logger from '../../../components/logger'
import Information from './model'
const Query = `
  extend type Query {
    getAllInformations: [Information],
    getInformation(id: ID!): Information,
    getInformationActive: Information,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllInformations: async () => {
      return await Information.find();
    },
    getInformation: async (parent, { id }) => {
      logger.info(`--QUERY.INFORMATION.GETINFORMATION-- id information = ${id}`);
      let data = await Information.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.INFORMATION.GETINFORMATION-- response object = ${data}`);
      return data
    },
    getInformationActive: async () => {
      let data = await Information.findOne({state: true});
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.INFORMATION.GETINFORMATIONACTIVE-- response object = ${data}`);
      return data
    }
  }
};
