import logger from '../../../components/logger'
import Information from './model';
const Mutation = `
  extend type Mutation {
    createInformation(information: InputInformation): Information,
    updateInformation(id: ID!, information: InputInformation): Information,
    deleteInformation(id: ID!): Information
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createInformation: async (parent, { information }) => {
      logger.info(`--MUTATION.INFORMATION.CREATEINFORMATION-- object information = ${JSON.stringify(information)}`);
      let informationModel = new Information(information);
      return await informationModel.save();
    },
    updateInformation: async (parent, { id, information }) => {
      logger.info(`--MUTATION.INFORMATION.UPDATEINFORMATION-- id information = ${id}`);
      logger.info(`--MUTATION.INFORMATION.UPDATEINFORMATION-- object information = ${JSON.stringify(information)}`);
      let data = await Information.findOneAndUpdate({_id: id}, information, {new: true});
      if(!data) {
        logger.info(`--MUTATION.INFORMATION.UPDATEINFORMATION-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteInformation: async (parent, {id}) => {
      logger.info(`--MUTATION.INFORMATION.DELETEINFORMATION-- id information = ${id}`)
      let data = await Information.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.INFORMATION.DELETEINFORMATION-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.INFORMATION.DELETEINFORMATION-- response object = ${data}`)
      return data;
    }
  }
};
