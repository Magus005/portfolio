const Information = `
  type Information {
    _id: String
    fullname: String
    dateOfBirth: Date
    address: String
    email: Date
    phoneNumber: Date
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Information];

export const typeResolvers = {

};
