import logger from '../../../components/logger'
import Skill from './model';
const Mutation = `
  extend type Mutation {
    createSkill(skill: InputSkill): Skill,
    updateSkill(id: ID!, skill: InputSkill): Skill,
    deleteSkill(id: ID!): Skill
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createSkill: async (parent, { skill }) => {
      logger.info(`--MUTATION.SKILL.CREATESKILL-- object skill = ${JSON.stringify(skill)}`);
      let skillModel = new Skill(skill);
      return await skillModel.save();
    },
    updateSkill: async (parent, { id, skill }) => {
      logger.info(`--MUTATION.SKILL.UPDATESKILL-- id skill = ${id}`);
      logger.info(`--MUTATION.SKILL.UPDATESKILL-- object skill = ${JSON.stringify(skill)}`);
      let data = await Skill.findOneAndUpdate({_id: id}, skill, {new: true});
      if(!data) {
        logger.info(`--MUTATION.SKILL.UPDATESKILL-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteSkill: async (parent, {id}) => {
      logger.info(`--MUTATION.SKILL.DELETESKILL-- id skill = ${id}`)
      let data = await Skill.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.SKILL.DELETESKILL-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.SKILL.DELETESKILL-- response object = ${data}`)
      return data;
    }
  }
};
