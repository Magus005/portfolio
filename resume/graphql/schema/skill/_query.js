import logger from '../../../components/logger'
import Skill from './model'
const Query = `
  extend type Query {
    getAllSkills: [Skill],
    getSkill(id: ID!): Skill,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllSkills: async () => {
      return await Skill.find();
    },
    getSkill: async (parent, { id }) => {
      logger.info(`--QUERY.SKILL.GETSKILL-- id skill = ${id}`);
      let data = await Skill.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.SKILL.GETSKILL-- response object = ${data}`);
      return data
    }
  }
};
