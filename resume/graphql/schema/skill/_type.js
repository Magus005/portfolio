const Skill = `
  type Skill {
    _id: String
    name: String
    level: String
    type: String
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Skill];

export const typeResolvers = {

};