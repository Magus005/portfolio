'use strict';
import mongoose from 'mongoose';

var SkillModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  level: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true,
    enum: ['line', 'circle']
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

export default mongoose.model('Skill', SkillModel);
