const Input = `
  input InputSkill {
    name: String
    level: String
    type: String
    state: Boolean
  }
`;

export default () => [Input];
