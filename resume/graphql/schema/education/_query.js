import logger from '../../../components/logger'
import Education from './model'
const Query = `
  extend type Query {
    getAllEducations: [Education],
    getEducation(id: ID!): Education,
    getEducationActive: Education,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllEducations: async () => {
      return await Education.find();
    },
    getEducation: async (parent, { id }) => {
      logger.info(`--QUERY.EDUCATION.GETEDUCATION-- id education = ${id}`);
      let data = await Education.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.EDUCATION.GETEDUCATION-- response object = ${data}`);
      return data
    },
    getEducationActive: async () => {
      let data = await Education.findOne({state: true});
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.EDUCATION.GETEDUCATIONACTIVE-- response object = ${data}`);
      return data
    }
  }
};
