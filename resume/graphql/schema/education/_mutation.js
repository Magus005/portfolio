import logger from '../../../components/logger'
import Education from './model';
const Mutation = `
  extend type Mutation {
    createEducation(education: InputEducation): Education,
    updateEducation(id: ID!, education: InputEducation): Education,
    deleteEducation(id: ID!): Education
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createEducation: async (parent, { education }) => {
      logger.info(`--MUTATION.EDUCATION.CREATEEDUCATION-- object education = ${JSON.stringify(education)}`);
      let educationModel = new Education(education);
      return await educationModel.save();
    },
    updateEducation: async (parent, { id, education }) => {
      logger.info(`--MUTATION.EDUCATION.UPDATEEDUCATION-- id education = ${id}`);
      logger.info(`--MUTATION.EDUCATION.UPDATEEDUCATION-- object education = ${JSON.stringify(education)}`);
      let data = await Education.findOneAndUpdate({_id: id}, education, {new: true});
      if(!data) {
        logger.info(`--MUTATION.EDUCATION.UPDATEEDUCATION-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteEducation: async (parent, {id}) => {
      logger.info(`--MUTATION.EDUCATION.DELETEEDUCATION-- id education = ${id}`)
      let data = await Education.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.EDUCATION.DELETEEDUCATION-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.EDUCATION.DELETEEDUCATION-- response object = ${data}`)
      return data;
    }
  }
};
