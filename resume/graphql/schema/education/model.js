'use strict';
import mongoose from 'mongoose';

var EducationModel = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    index: true
  },
  schoolName: {
    type: String,
    required: true
  },
  startDate: {
    type: Date,
    default: Date.now
  },
  endDate: {
    type: Date,
    default: Date.now
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

EducationModel.index({
  title: 1
});
export default mongoose.model('Education', EducationModel);
