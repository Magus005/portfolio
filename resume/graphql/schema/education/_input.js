const Input = `
  input InputEducation {
    title: String
    schoolName: String
    startDate: Date
    endDate: Date
    state: Boolean
  }
`;

export default () => [Input];
