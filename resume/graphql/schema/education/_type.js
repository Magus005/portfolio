const Education = `
  type Education {
    _id: String
    title: String
    schoolName: String
    startDate: Date
    endDate: Date
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Education];

export const typeResolvers = {

};
