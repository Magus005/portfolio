import logger from '../../../components/logger'
import About from './model'
const Query = `
  extend type Query {
    getAllAbouts: [About],
    getAbout(id: ID!): About,
    getAboutActive: About,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllAbouts: async () => {
      return await About.find();
    },
    getAbout: async (parent, { id }) => {
      logger.info(`--QUERY.ABOUT.GETABOUT-- id about = ${id}`);
      let data = await About.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.ABOUT.GETABOUT-- response object = ${data}`);
      return data
    },
    getAboutActive: async () => {
      let data = await About.findOne({state: true});
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.ABOUT.GETABOUTACTIVE-- response object = ${data}`);
      return data
    }
  }
};
