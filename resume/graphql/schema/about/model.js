'use strict';
import mongoose from 'mongoose';

var AboutModel = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    index: true
  },
  content: {
    type: String,
    required: true,
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

AboutModel.index({
  title: 1
});
AboutModel.index({
  state: 1
});
export default mongoose.model('About', AboutModel);
