const About = `
  type About {
    _id: String
    title: String
    content: String
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [About];

export const typeResolvers = {

};
