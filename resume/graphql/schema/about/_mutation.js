import logger from '../../../components/logger'
import About from './model';
const Mutation = `
  extend type Mutation {
    createAbout(about: InputAbout): About,
    updateAbout(id: ID!, about: InputAbout): About,
    deleteAbout(id: ID!): About
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createAbout: async (parent, { about }) => {
      logger.info(`--MUTATION.ABOUT.CREATEABOUT-- object about = ${JSON.stringify(about)}`)
      let aboutModel = new About(about);
      return await aboutModel.save();
    },
    updateAbout: async (parent, { id, about }) => {
      logger.info(`--MUTATION.ABOUT.UPDATEABOUT-- id about = ${id}`);
      logger.info(`--MUTATION.ABOUT.UPDATEABOUT-- object about = ${JSON.stringify(about)}`);
      let data = await About.findOneAndUpdate({_id: id}, about, {new: true});
      if(!data) {
        logger.info(`--MUTATION.ABOUT.UPDATEABOUT-- response object = ${data}`);
        return null;
      }
      return data;
    },
    deleteAbout: async (parent, {id}) => {
      logger.info(`--MUTATION.ABOUT.DELETEABOUT-- id about = ${id}`);
      let data = await About.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.ABOUT.DELETEABOUT-- response object = ${data}`);
        return null;
      }
      logger.info(`--MUTATION.ABOUT.DELETEABOUT-- response object = ${data}`);
      return data;
    }
  }
};
