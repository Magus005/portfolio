const Input = `
  scalar Upload
  input InputAbout {
    title: String
    content: String
    state: Boolean
  }
`;

export default () => [Input];
