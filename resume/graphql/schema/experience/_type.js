const Experience = `
  type Experience {
    _id: String
    title: String
    logoEnterprise: String
    nameEnterprise: String
    content: String
    startDate: Date
    endDate: Date
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Experience];

export const typeResolvers = {

};
