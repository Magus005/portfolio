'use strict';
import mongoose from 'mongoose';

var ExperienceModel = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    index: true
  },
  logoEnterprise: {
    type: String,
    required: true
  },
  nameEnterprise: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  startDate: {
    type: Date,
    default: Date.now
  },
  endDate: {
    type: Date,
    default: null
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

ExperienceModel.index({
  title: 1
});
export default mongoose.model('Experience', ExperienceModel);
