const Input = `
  scalar Date
  scalar DateTime
  input InputExperience {
    title: String
    logoEnterprise: Upload
    nameEnterprise: String
    content: String
    startDate: Date
    endDate: Date
    state: Boolean
  }
`;

export default () => [Input];
