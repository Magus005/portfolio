import logger from '../../../components/logger'
import Experience from './model';
const Mutation = `
  extend type Mutation {
    createExperience(experience: InputExperience): Experience,
    updateExperience(id: ID!, experience: InputExperience): Experience,
    deleteExperience(id: ID!): Experience
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createExperience: async (parent, { experience }) => {
      logger.info(`--MUTATION.EXPERIENCE.CREATEEXPERIENCE-- object experience = ${JSON.stringify(experience)}`);
      let experienceModel = new Experience(experience);
      return await experienceModel.save();
    },
    updateExperience: async (parent, { id, experience }) => {
      logger.info(`--MUTATION.EXPERIENCE.UPDATEEXPERIENCE-- id experience = ${id}`);
      logger.info(`--MUTATION.EXPERIENCE.UPDATEEXPERIENCE-- object experience = ${JSON.stringify(experience)}`);
      let data = await Experience.findOneAndUpdate({_id: id}, experience, {new: true});
      if(!data) {
        logger.info(`--MUTATION.EXPERIENCE.UPDATEEXPERIENCE-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteExperience: async (parent, {id}) => {
      logger.info(`--MUTATION.EXPERIENCE.DELETEEXPERIENCE-- id experience = ${id}`)
      let data = await Experience.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.EXPERIENCE.DELETEEXPERIENCE-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.EXPERIENCE.DELETEEXPERIENCE-- response object = ${data}`)
      return data;
    }
  }
};
