import logger from '../../../components/logger'
import Experience from './model'
const Query = `
  extend type Query {
    getAllExperiences: [Experience],
    getExperience(id: ID!): Experience,
    getExperienceActive: Experience,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllExperiences: async () => {
      return await Experience.find();
    },
    getExperience: async (parent, { id }) => {
      logger.info(`--QUERY.EXPERIENCE.GETEXPERIENCE-- id experience = ${id}`);
      let data = await Experience.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.EXPERIENCE.GETEXPERIENCE-- response object = ${data}`);
      return data
    },
    getExperienceActive: async () => {
      let data = await Experience.findOne({state: true});
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.EXPERIENCE.GETEXPERIENCEACTIVE-- response object = ${data}`);
      return data
    }
  }
};
