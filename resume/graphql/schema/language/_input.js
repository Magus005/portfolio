const Input = `
  input InputLanguage {
    name: String
    level: String
    type: String
    state: Boolean
  }
`;

export default () => [Input];
