import logger from '../../../components/logger'
import Language from './model'
const Query = `
  extend type Query {
    getAllLanguages: [Language],
    getLanguage(id: ID!): Language,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllLanguages: async () => {
      return await Language.find();
    },
    getLanguage: async (parent, { id }) => {
      logger.info(`--QUERY.LANGUAGE.GETLANGUAGE-- id language = ${id}`);
      let data = await Language.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.LANGUAGE.GETLANGUAGE-- response object = ${data}`);
      return data
    }
  }
};
