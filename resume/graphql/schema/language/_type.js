const Language = `
  type Language {
    _id: String
    name: String
    level: String
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Language];

export const typeResolvers = {

};