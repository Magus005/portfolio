'use strict';
import mongoose from 'mongoose';

var LanguageModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  level: {
    type: String,
    required: true
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

LanguageModel.index({
  name: 1
});
export default mongoose.model('Language', LanguageModel);
