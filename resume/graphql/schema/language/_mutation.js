import logger from '../../../components/logger'
import Language from './model';
const Mutation = `
  extend type Mutation {
    createLanguage(language: InputLanguage): Language,
    updateLanguage(id: ID!, language: InputLanguage): Language,
    deleteLanguage(id: ID!): Language
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createLanguage: async (parent, { language }) => {
      logger.info(`--MUTATION.LANGUAGE.CREATELANGUAGE-- object language = ${JSON.stringify(language)}`);
      let languageModel = new Language(language);
      return await languageModel.save();
    },
    updateLanguage: async (parent, { id, language }) => {
      logger.info(`--MUTATION.LANGUAGE.UPDATELANGUAGE-- id language = ${id}`);
      logger.info(`--MUTATION.LANGUAGE.UPDATELANGUAGE-- object language = ${JSON.stringify(language)}`);
      let data = await Language.findOneAndUpdate({_id: id}, language, {new: true});
      if(!data) {
        logger.info(`--MUTATION.LANGUAGE.UPDATELANGUAGE-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteLanguage: async (parent, {id}) => {
      logger.info(`--MUTATION.LANGUAGE.DELETELANGUAGE-- id language = ${id}`)
      let data = await Language.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.LANGUAGE.DELETELANGUAGE-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.LANGUAGE.DELETELANGUAGE-- response object = ${data}`)
      return data;
    }
  }
};
