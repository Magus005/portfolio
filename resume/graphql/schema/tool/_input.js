const Input = `
  input InputTool {
    name: String
    style: String
    state: Boolean
  }
`;

export default () => [Input];
