'use strict';
import mongoose from 'mongoose';

var ToolModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  style: {
    type: String,
    required: true,
    enum: ['check', 'icon']
  },
  state: {
    type: Boolean,
    index: true,
    default: true
  }
},{timestamps: true});

export default mongoose.model('Tool', ToolModel);
