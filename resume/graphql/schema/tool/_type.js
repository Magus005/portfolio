const Tool = `
  type Tool {
    _id: String
    name: String
    style: String
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Tool];

export const typeResolvers = {

};
