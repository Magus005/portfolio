import logger from '../../../components/logger'
import Tool from './model';
const Mutation = `
  extend type Mutation {
    createTool(tool: InputTool): Tool,
    updateTool(id: ID!, tool: InputTool): Tool,
    deleteTool(id: ID!): Tool
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createTool: async (parent, { tool }) => {
      logger.info(`--MUTATION.TOOL.CREATETOOL-- object tool = ${JSON.stringify(tool)}`);
      let toolModel = new Tool(tool);
      return await toolModel.save();
    },
    updateTool: async (parent, { id, tool }) => {
      logger.info(`--MUTATION.TOOL.UPDATETOOL-- id tool = ${id}`);
      logger.info(`--MUTATION.TOOL.UPDATETOOL-- object tool = ${JSON.stringify(tool)}`);
      let data = await Tool.findOneAndUpdate({_id: id}, tool, {new: true});
      if(!data) {
        logger.info(`--MUTATION.TOOL.UPDATETOOL-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteTool: async (parent, {id}) => {
      logger.info(`--MUTATION.TOOL.DELETETOOL-- id tool = ${id}`)
      let data = await Tool.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.TOOL.DELETETOOL-- response object = ${data}`)
        return null
      }
      logger.info(`--MUTATION.TOOL.DELETETOOL-- response object = ${data}`)
      return data;
    }
  }
};
