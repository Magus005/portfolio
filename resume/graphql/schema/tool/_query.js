import logger from '../../../components/logger'
import Tool from './model'
const Query = `
  extend type Query {
    getAllTools: [Tool],
    getTool(id: ID!): Tool,
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllTools: async () => {
      return await Tool.find();
    },
    getTool: async (parent, { id }) => {
      logger.info(`--QUERY.TOOL.GETTOOL-- id tool = ${id}`);
      let data = await Tool.findById(id);
      if(!data) {
        return null;
      }
      logger.info(`--QUERY.TOOL.GETTOOL-- response object = ${data}`);
      return data
    }
  }
};
