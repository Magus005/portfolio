import { graphqlKoa, graphiqlKoa } from 'apollo-server-koa'
import { apolloUploadKoa } from 'apollo-upload-server'
import config from '../config/environment';
import schema from './schema';
import koaRouter from 'koa-router';
export default (app) => {
  const router = new koaRouter();
  if (config.env === 'development') {
    router.post('/api/graphiql', graphiqlKoa({ endpointURL: '/api' }));
    router.get('/api/graphiql', graphiqlKoa({ endpointURL: '/api' }));
  }
  
  router.get('/api',
    apolloUploadKoa({ uploadDir: '../assets/images/'}),
    graphqlKoa(() => ({
      schema,
      cors: true,
      debug: config.env === 'development'
    }))
  );
  router.post('/api',
    apolloUploadKoa({ uploadDir: '../assets/images/'}),
    graphqlKoa(() => ({
      schema,
      cors: true,
      debug: config.env === 'development'
    }))
  );
  app.use(router.routes());
};
