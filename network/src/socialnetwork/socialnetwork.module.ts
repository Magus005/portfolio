import { Module } from '@nestjs/common';
import { SocialNetworkController } from './socialnetwork.controller';
import { SocialNetworkService } from './socialnetwork.service';
import { SocialNetworkSchema } from './socialnetwork.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'SocialNetwork', schema: SocialNetworkSchema },
  ])],
  controllers: [SocialNetworkController],
  providers: [SocialNetworkService],
})
export class SocialNetworkModule {}
