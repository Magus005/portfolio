export class SocialNetworkDto {
  readonly icon: string;
  readonly name: string;
  readonly uri: string;
}
