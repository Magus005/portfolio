import { Controller, Get, Post, Body, Put, Delete, Req, Res, Param, Logger } from '@nestjs/common';
import { SocialNetworkDto } from './socialnetwork.dto';
import { SocialNetwork } from './socialnetwork.interface';
import { Request, Response } from 'express';
import { SocialNetworkService } from './socialnetwork.service';

@Controller('socialnetwork')
export class SocialNetworkController {

  constructor(private readonly socialNetworkService: SocialNetworkService) {}

  @Get()
  findAll(@Req() req: Request, @Res() res: Response): Response {
    try {
      const response = this.socialNetworkService.findAll();
      response.then((socialnetworks) => {
        Logger.log(`--SOCIALNETWORK.CONTROLLER.FINDALL-- object socialnetworks = ${socialnetworks}`);
        return res.status(200).json(socialnetworks);
      });
    } catch (error) {
      return error;
    }
  }

  @Get(':id')
  findOne(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.socialNetworkService.findById(param.id);
      response.then((socialnetwork) => {
        Logger.log(`--SOCIALNETWORK.CONTROLLER.FINDONE-- object socialnetwork : ${socialnetwork}`);
        return res.status(200).json(socialnetwork);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Post()
  create(@Req() req: Request, @Res() res: Response, @Body() socialNetworkDto: SocialNetworkDto): Response {
    try {
      const response = this.socialNetworkService.create(socialNetworkDto);
      response.then((socialnetwork) => {
        Logger.log(`--SOCIALNETWORK.CONTROLLER.CREATE-- object socialnetwork : ${socialnetwork}`);
        return res.status(200).json(socialnetwork);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Put(':id')
  update(@Req() req: Request, @Res() res: Response, @Param() param, @Body() socialNetworkDto: SocialNetworkDto): Response {
    try {
      const response = this.socialNetworkService.update(param.id, socialNetworkDto);
      response.then((socialnetwork) => {
        Logger.log(`--SOCIALNETWORK.CONTROLLER.UPDATE-- object socialnetwork : ${socialnetwork}`);
        return res.status(200).json(socialnetwork);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Delete(':id')
  delete(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.socialNetworkService.delete(param.id);
      response.then((socialnetwork) => {
        Logger.log(`--SOCIALNETWORK.CONTROLLER.DELETE-- object socialnetwork : ${socialnetwork}`);
        return res.status(200).json(socialnetwork);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}
