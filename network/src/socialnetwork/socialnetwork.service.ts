import { Injectable } from '@nestjs/common';
import { SocialNetwork } from './socialnetwork.interface';
import { SocialNetworkDto } from './socialnetwork.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class SocialNetworkService {
  constructor(@InjectModel('SocialNetwork') private readonly socialNetworkModel: Model<SocialNetwork>) {}

  async findAll(): Promise<SocialNetwork[]> {
    return await this.socialNetworkModel.find();
  }

  async findById(id: string): Promise<SocialNetwork[]> {
    return await this.socialNetworkModel.findById(id);
  }

  async create(socialNetwork: SocialNetworkDto): Promise<SocialNetwork> {
    const newSocialNetwork = new this.socialNetworkModel(socialNetwork);
    return await newSocialNetwork.save();
  }

  async update(id: string, socialNetwork: SocialNetworkDto): Promise<SocialNetwork> {
    return await this.socialNetworkModel.findOneAndUpdate({_id: id}, socialNetwork, {new: true});
  }

  async delete(id: string): Promise<SocialNetwork> {
    return await this.socialNetworkModel.findByIdAndRemove(id);
  }
}
