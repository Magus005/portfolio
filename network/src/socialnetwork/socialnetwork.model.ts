import * as mongoose from 'mongoose';

export const SocialNetworkSchema = new mongoose.Schema({
  icon: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  uri: {
    type: String,
    required: true,
  },
  state: {
    type: Boolean,
    default: true,
  },
},
{timestamps: true});
