import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ReferenceController } from './reference/reference.controller';
import { ReferenceService } from './reference/reference.service';
import { ReferenceModule } from './reference/reference.module';
import { FaqController } from './faq/faq.controller';
import { FaqService } from './faq/faq.service';
import { FaqModule } from './faq/faq.module';
import { MongooseModule } from '@nestjs/mongoose';
import { SocialNetworkController } from './socialnetwork/socialnetwork.controller';
import { SocialNetworkService } from './socialnetwork/socialnetwork.service';
import { SocialNetworkModule } from './socialnetwork/socialnetwork.module';
import { MessageController } from './message/message.controller';
import { MessageService } from './message/message.service';
import { MessageModule } from './message/message.module';
import config from '../config/environment/index';

@Module({
  imports: [ReferenceModule, FaqModule, SocialNetworkModule, MessageModule, MongooseModule.forRoot(config.mongo.uri, config.mongo.options)],
  controllers: [AppController, ReferenceController, FaqController, SocialNetworkController, MessageController],
  providers: [AppService, ReferenceService, FaqService, SocialNetworkService, MessageService],
})
export class AppModule {}
