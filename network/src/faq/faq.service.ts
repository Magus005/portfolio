import { Injectable } from '@nestjs/common';
import { Faq } from './faq.interface';
import { FaqDto } from './faq.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class FaqService {
  constructor(@InjectModel('Faq') private readonly faqModel: Model<Faq>) {}

  async findAll(): Promise<Faq[]> {
    return await this.faqModel.find();
  }

  async findById(id: string): Promise<Faq[]> {
    return await this.faqModel.findById(id);
  }

  async create(faq: FaqDto): Promise<Faq> {
    const newFaq = new this.faqModel(faq);
    return await newFaq.save();
  }

  async update(id: string, faq: FaqDto): Promise<Faq> {
    return await this.faqModel.findOneAndUpdate({_id: id}, faq, {new: true});
  }

  async delete(id: string): Promise<Faq> {
    return await this.faqModel.findByIdAndRemove(id);
  }
}
