import { Controller, Get, Post, Body, Put, Delete, Req, Res, Param, Logger } from '@nestjs/common';
import { FaqDto } from './faq.dto';
import { Request, Response } from 'express';
import { FaqService } from './faq.service';

@Controller('faq')
export class FaqController {

  constructor(private readonly faqService: FaqService) {}

  @Get()
  findAll(@Req() req: Request, @Res() res: Response): Response {
    try {
      const response = this.faqService.findAll();
      response.then((faqs) => {
        Logger.log(`--FAQ.CONTROLLER.FINDALL-- object faqs = ${faqs}`);
        return res.status(200).json(faqs);
      });
    } catch (error) {
      return error;
    }
  }

  @Get(':id')
  findOne(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.faqService.findById(param.id);
      response.then((faq) => {
        Logger.log(`--FAQ.CONTROLLER.FINDONE-- object faq : ${faq}`);
        return res.status(200).json(faq);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Post()
  create(@Req() req: Request, @Res() res: Response, @Body() faqDto: FaqDto): Response {
    try {
      const response = this.faqService.create(faqDto);
      response.then((faq) => {
        Logger.log(`--FAQ.CONTROLLER.CREATE-- object faq : ${faq}`);
        return res.status(200).json(faq);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Put(':id')
  update(@Req() req: Request, @Res() res: Response, @Param() param, @Body() faqDto: FaqDto): Response {
    try {
      const response = this.faqService.update(param.id, faqDto);
      response.then((faq) => {
        Logger.log(`--FAQ.CONTROLLER.UPDATE-- object faq : ${faq}`);
        return res.status(200).json(faq);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Delete(':id')
  delete(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.faqService.delete(param.id);
      response.then((faq) => {
        Logger.log(`--FAQ.CONTROLLER.DELETE-- object faq : ${faq}`);
        return res.status(200).json(faq);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}
