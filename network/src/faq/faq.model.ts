import * as mongoose from 'mongoose';

export const FaqSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
},
{timestamps: true});
