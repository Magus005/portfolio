import { Module } from '@nestjs/common';
import { FaqController } from './faq.controller';
import { FaqService } from './faq.service';
import { FaqSchema } from './faq.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Faq', schema: FaqSchema },
  ])],
  controllers: [FaqController],
  providers: [FaqService],
})
export class FaqModule {}
