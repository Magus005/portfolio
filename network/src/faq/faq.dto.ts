export class FaqDto {
  readonly name: string;
  readonly content: string;
}
