export interface Faq {
  _id: string;
  name?: string;
  content?: string;
}
