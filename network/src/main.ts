import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MyLogger } from '../config/error';
import * as helmet from 'helmet';
import * as csurf from 'csurf';
import * as rateLimit from 'express-rate-limit';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new MyLogger(),
  });
  app.enableCors();
  await app.listen(5020);
}
bootstrap();
