import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';
import { MessageSchema } from './message.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Message', schema: MessageSchema },
  ])],
  controllers: [MessageController],
  providers: [MessageService],
})
export class MessageModule {}
