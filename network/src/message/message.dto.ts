export class MessageDto {
  readonly name: string;
  readonly email: string;
  readonly subject: string;
  readonly content: string;
  readonly state: boolean;
}
