import { Controller, Get, Post, Body, Put, Delete, Req, Res, Param, Logger } from '@nestjs/common';
import { MessageDto } from './message.dto';
import { Message } from './message.interface';
import { Request, Response } from 'express';
import { MessageService } from './message.service';

@Controller('message')
export class MessageController {

  constructor(private readonly messageService: MessageService) {}

  @Get()
  findAll(@Req() req: Request, @Res() res: Response): Response {
    try {
      const response = this.messageService.findAll();
      response.then((messages) => {
        Logger.log(`--MESSAGE.CONTROLLER.FINDALL-- object messages = ${messages}`);
        return res.status(200).json(messages);
      });
    } catch (error) {
      return error;
    }
  }

  @Get(':id')
  findOne(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.messageService.findById(param.id);
      response.then((message) => {
        Logger.log(`--MESSAGE.CONTROLLER.FINDONE-- object message : ${message}`);
        return res.status(200).json(message);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Post()
  create(@Req() req: Request, @Res() res: Response, @Body() messageDto: MessageDto): Response {
    try {
      const response = this.messageService.create(messageDto);
      response.then((message) => {
        Logger.log(`--MESSAGE.CONTROLLER.CREATE-- object message : ${message}`);
        return res.status(200).json(message);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Put(':id')
  update(@Req() req: Request, @Res() res: Response, @Param() param, @Body() messageDto: MessageDto): Response {
    try {
      const response = this.messageService.update(param.id, messageDto);
      response.then((message) => {
        Logger.log(`--MESSAGE.CONTROLLER.UPDATE-- object message : ${message}`);
        return res.status(200).json(message);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Delete(':id')
  delete(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.messageService.delete(param.id);
      response.then((message) => {
        Logger.log(`--MESSAGE.CONTROLLER.DELETE-- object message : ${message}`);
        return res.status(200).json(message);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}
