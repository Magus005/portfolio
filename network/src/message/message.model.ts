import * as mongoose from 'mongoose';

export const MessageSchema = new mongoose.Schema({
  name: {
    type: String,
    default: null,
  },
  email: {
    type: String,
    required: true,
  },
  subject: {
    type: String,
    default: null,
  },
  content: {
    type: String,
    required: true,
  },
  state: {
    type: Boolean,
    default: true,
  },
},
{timestamps: true});
