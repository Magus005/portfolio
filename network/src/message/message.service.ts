import { Injectable } from '@nestjs/common';
import { Message } from './message.interface';
import { MessageDto } from './message.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class MessageService {
  constructor(@InjectModel('Message') private readonly messageModel: Model<Message>) {}

  async findAll(): Promise<Message[]> {
    return await this.messageModel.find();
  }

  async findById(id: string): Promise<Message[]> {
    return await this.messageModel.findById(id);
  }

  async create(message: MessageDto): Promise<Message> {
    const newMessage = new this.messageModel(message);
    return await newMessage.save();
  }

  async update(id: string, message: MessageDto): Promise<Message> {
    return await this.messageModel.findOneAndUpdate({_id: id}, message, {new: true});
  }

  async delete(id: string): Promise<Message> {
    return await this.messageModel.findByIdAndRemove(id);
  }
}
