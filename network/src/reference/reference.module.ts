import { Module } from '@nestjs/common';
import { ReferenceController } from './reference.controller';
import { ReferenceService } from './reference.service';
import { ReferenceSchema } from './reference.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Reference', schema: ReferenceSchema },
  ])],
  controllers: [ReferenceController],
  providers: [ReferenceService],
})
export class ReferenceModule {}
