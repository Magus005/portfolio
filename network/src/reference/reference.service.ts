import { Injectable } from '@nestjs/common';
import { Reference } from './reference.interface';
import { ReferenceDto } from './reference.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ReferenceService {
  constructor(@InjectModel('Reference') private readonly referenceModel: Model<Reference>) {}

  async findAll(): Promise<Reference[]> {
    return await this.referenceModel.find();
  }

  async findById(id: string): Promise<Reference[]> {
    return await this.referenceModel.findById(id);
  }

  async create(reference: ReferenceDto): Promise<Reference> {
    const newReference = new this.referenceModel(reference);
    return await newReference.save();
  }

  async update(id: string, reference: ReferenceDto): Promise<Reference> {
    return await this.referenceModel.findOneAndUpdate({_id: id}, reference, {new: true});
  }

  async delete(id: string): Promise<Reference> {
    return await this.referenceModel.findByIdAndRemove(id);
  }
}
