export class ReferenceDto {
  readonly userPicture: string;
  readonly userName: string;
  readonly userEnterprise: string;
  readonly content: string;
  readonly state: boolean;
}
