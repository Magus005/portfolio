import * as mongoose from 'mongoose';

export const ReferenceSchema = new mongoose.Schema({
  userPicture: {
    type: String,
    default: null,
  },
  userName: {
    type: String,
    required: true,
  },
  userEnterprise: {
    type: String,
    default: null,
  },
  content: {
    type: String,
    required: true,
  },
  state: {
    type: Boolean,
    default: true,
  },
},
{timestamps: true});
