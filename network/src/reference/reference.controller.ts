import { Controller, Get, Post, Body, Put, Delete, Req, Res, Param, Logger } from '@nestjs/common';
import { ReferenceDto } from './reference.dto';
import { Reference } from './reference.interface';
import { Request, Response } from 'express';
import { ReferenceService } from './reference.service';

@Controller('reference')
export class ReferenceController {

  constructor(private readonly referenceService: ReferenceService) {}

  @Get()
  findAll(@Req() req: Request, @Res() res: Response): Response {
    try {
      const response = this.referenceService.findAll();
      response.then((references) => {
        Logger.log(`--REFERENCE.CONTROLLER.FINDALL-- object references = ${references}`);
        return res.status(200).json(references);
      });
    } catch (error) {
      return error;
    }
  }

  @Get(':id')
  findOne(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.referenceService.findById(param.id);
      response.then((reference) => {
        Logger.log(`--REFERENCE.CONTROLLER.FINDONE-- object references : ${reference}`);
        return res.status(200).json(reference);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Post()
  create(@Req() req: Request, @Res() res: Response, @Body() referenceDto: ReferenceDto): Response {
    try {
      const response = this.referenceService.create(referenceDto);
      response.then((reference) => {
        Logger.log(`--REFERENCE.CONTROLLER.CREATE-- object reference : ${reference}`);
        return res.status(200).json(reference);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Put(':id')
  update(@Req() req: Request, @Res() res: Response, @Param() param, @Body() referenceDto: ReferenceDto): Response {
    try {
      const response = this.referenceService.update(param.id, referenceDto);
      response.then((reference) => {
        Logger.log(`--REFERENCE.CONTROLLER.UPDATE-- object reference : ${reference}`);
        return res.status(200).json(reference);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  @Delete(':id')
  delete(@Req() req: Request, @Res() res: Response, @Param() param): Response {
    try {
      const response = this.referenceService.delete(param.id);
      response.then((reference) => {
        Logger.log(`--REFERENCE.CONTROLLER.DELETE-- object reference : ${reference}`);
        return res.status(200).json(reference);
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}
