export default {
  mongo: {
    uri: 'mongodb://localhost:27017/portfolio-dev',
    options: {
      db: {
        safe: true,
      },
      useNewUrlParser: true,
    },
  },
};
