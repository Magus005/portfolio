import { LoggerService } from '@nestjs/common';

export class MyLogger implements LoggerService {
  log(message: string) { /** NAN */ }
  error(message: string, trace: string) { /** NAN */ }
  warn(message: string) { /** NAN */ }
  debug(message: string) { /** NAN */ }
  verbose(message: string) { /** NAN */ }
}
