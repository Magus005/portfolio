# Portfolio

## Architecture
![alt text](architecture.png)

## Les Services

### Blog
- Techno : NodeJS + Express + Apollo Server + GraphQL
- Description:
- run : sudo npm run start:server
### Network
- Techno : NodeJS + NestJS
- Description:
- run : sudo npm run start
### Personal
- Techno : Spring-boot
- Description:
- run : mvn spring-boot:run
### Resume
- Techno : NodeJS + Koa + Apollo Server + GraphQL
- Description:
- run : sudo npm run start:server
### BackOffice
- Techno : Angular + Apollo Client + GraphQL + HttpClient
- Description:
- run : sudo npm run client:open
### FrontOffice
- Techno : ReactJS + Apollo Client + GraphQL + Axios
- Description:
- run : sudo npm run start
### DataBase
- Techno : MongoDB

## Outil
### GCP Kubernetes Docker SonarQube TravisCI GitLab

## Contact
### Madiara Gassama <madiara.gassama005@gmail.com>