# set the base image to Node
FROM node:12.2.0

#Author
MAINTAINER Madiara Gassama <madiara.gassama005@gmail.com>

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY ../../frontoffice/package*.json ./
RUN npm install

#To bundle your app’s source code inside the Docker image, use the COPY instruction:
COPY ../../frontoffice/ .

#Your app binds to port 3000 so you’ll use the EXPOSE instruction to have it mapped by the docker daemon:
EXPOSE 3000
CMD [“npm”, “start”]