# set the base image to Node
FROM node:12.2.0

#Author
MAINTAINER Madiara Gassama <madiara.gassama005@gmail.com>

# Set environment variables
ENV appDir /var/www/app/current

# Set the work directory
RUN mkdir -p /var/www/app/current
WORKDIR ${appDir}

# Add our package.json and install *before* adding our application files
ADD ../../resume/package.json ./
ADD ../../resume/package-lock.json ./
RUN npm install
RUN npm install -g nodemon

COPY ../../resume/. .

#Expose the port
EXPOSE 5010

CMD ["npm", "start"]