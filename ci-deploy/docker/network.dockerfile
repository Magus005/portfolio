# set the base image to Node
FROM node:12.2.0

#Author
MAINTAINER Madiara Gassama <madiara.gassama005@gmail.com>

# Set environment variables
ENV appDir /var/www/app/current

# Set the work directory
RUN mkdir -p /var/www/app/current
WORKDIR ${appDir}

# Add our package.json and install *before* adding our application files
ADD ../../network/package.json ./
ADD ../../network/package-lock.json ./
RUN npm install
RUN npm install -g nodemon

COPY ../../network/. .

#Expose the port
EXPOSE 5020

CMD ["npm", "start"]