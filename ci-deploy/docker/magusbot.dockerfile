# set the base image to Alpine - Python
FROM python:alpine3.7

#Author
MAINTAINER Madiara Gassama <madiara.gassama005@gmail.com>

COPY ../../magusbot/ /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5030
CMD python ./index.py