#Readme
# Google Cloud Deployment

### Build Image docker
```
$ docker build -t gcr.io/<PROJECT_NAME_GCP>/backoffice:0.0.1 -f ./docker/backoffice.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/backoffice

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/blog:0.0.1 -f ./docker/rabbitmq.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/blog

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/network:0.0.1 -f ./docker/redis.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/network

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/resume:0.0.1 -f ./docker/consumer.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/resume

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/personal:0.0.1 -f ./docker/producer.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/personal

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/frontoffice:0.0.1 -f ./docker/producer.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/frontoffice

$ docker build -t gcr.io/<PROJECT_NAME_GCP>/backoffice:0.0.1 -f ./docker/producer.dockerfile .
$ gcloud docker -- push gcr.io/<PROJECT_NAME_GCP>/backoffice
```

