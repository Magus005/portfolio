/**
 * Main application file
 */
'use strict';

import express from 'express';
import mongoose from 'mongoose';
import config from './config/environment'
import logger from './components/logger'
import http, {Server} from 'http';
import cors from 'cors';
import apollo from './graphql';

//import expressConfig from './config/express';
//import registerRoutes from './routes';

// Setup server
var app = express();
var server = http.createServer(app);

// Connect to MongoDB
mongoose.connect(config.mongo.uri, config.mongo.options);

mongoose.connection.on('error', function(err) {
  logger.error('MongoDB connection error', err);
  process.exit(-1); // eslint-disable-line no-process-exit
});

app.use(cors({
  origin: config.corsDomain, // Be sure to switch to your production domain
  optionsSuccessStatus: 200
}));
//expressConfig(app);
//registerRoutes(app);
process.on('unhandledRejection', (reason, promise) => {
  logger.info(`Promise : ${promise}`);
  logger.warn('Unhandled Rejection at:', reason.stack || reason);
});

// Start server
function setPort() {
  app.set('port', parseInt(config.port, 10));
}
function listen() {
  const port = app.get('port') || config.port;
  app.listen(port, config.ip, () => {
    logger.info(`The server is running and listening at http://${config.ip}:${port}`);
  });
}
  
// Append apollo to our API
apollo(app);
  
export default {
  getApp: () => app,
  setPort,
  listen
};
