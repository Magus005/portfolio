module.exports = {
  mongo: {
    uri: 'mongodb://35.243.153.213:27017/portfolio-magus-v1.0',
    options: {
      db: {
        safe: true
      }
    }
  },
  env: 'production',
  corsDomain: '',
  port: 25005,
  jwt_secret: 'portfolio-magus-v1.0'
};