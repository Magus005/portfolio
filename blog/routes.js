/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
const path = require('path');
//import connectApiMocker from 'connect-api-mocker';
import config from './config/environment';
//import * as auth from './auth/auth.service';

export default function(app) {
  // Insert routes below
  app.use('/api/blogs', require('./api/blog'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets|mock)/*').get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*').get((req, res) => {
    res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
  });
}
