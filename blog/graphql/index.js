import bodyParser from 'body-parser';
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
import { apolloUploadExpress } from 'apollo-upload-server'
import config from '../config/environment';
import schema from './schema';

export default (app) => {
  if (config.env === 'development') {
    app.use('/api/graphiql', graphiqlExpress({ endpointURL: '/api' }));
  }

  app.use(
    '/api',
    bodyParser.json(),
    apolloUploadExpress({ uploadDir: '../assets/images/'}),
    graphqlExpress(() => ({
      schema,
      debug: config.env === 'development'
    }))
  );
};
