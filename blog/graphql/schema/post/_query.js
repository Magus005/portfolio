import Post from './model';
import logger from '../../../components/logger'

const Query = `
  extend type Query {
    getAllposts: [Post]
    getPost(id: ID!): Post
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllposts: async () => {
      return await Post.find();
    },
    getPost: async (parent, { id }) => {
      logger.info(`--QUERY.POST.GETPOST-- id post = ${id}`)
      return await Post.findById(id);
    }
  }
};
