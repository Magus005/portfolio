'use strict';
import mongoose from 'mongoose';

var PostModel = new mongoose.Schema({
  picture: {
    type: String,
    default: null
  },
  movie: {
    type: String,
    default: null
  },
  title: {
    type: String,
    index: true,
    required: true
  },
  content: {
    type: String,
    default: null
  },
  userId: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    index: true,
    default: null
  },
  categories: {
    type: [mongoose.Schema.ObjectId],
    ref: 'Category',
    index: true,
  },
  dateCreated: {
    type: Date,
    index: true,
    default: Date.now
  },
  state: {
    type: Boolean,
    default: true
  }
},{timestamps: true});

PostModel.index({
  title: 1
});
PostModel.index({
  categories: 1
});
PostModel.index({
  userId: 1
});
export default mongoose.model('Post', PostModel);
