const Input = `
  scalar Upload
  scalar Date
  input InputPost {
    picture: Upload
    movie: Upload
    content: String!
    title: String!
    userId: String
    dateCreated: Date
    categories: [InputCategory!]!
    state: Boolean
  }
`;

export default () => [Input];
