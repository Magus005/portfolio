import logger from '../../../components/logger';
import Category from '../category/model';
import Post from './model';

const Mutation = `
  extend type Mutation {
    createPost(post: InputPost): Post
    deletePost(id: ID!): Post
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createPost: async (parent, { post }) => {
      logger.info(`--MUTATION.POST.CREATEPOST-- input ${JSON.stringify(post)}`);
      var arrayCategories = []
      let categories = post.categories;
      for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        logger.info(`--MUTATION.POST.CREATEPOST-- category name ${category.name}`);
        let objectCategory = await Category.findOne({name: category.name})
        logger.info(`--MUTATION.POST.CREATEPOST-- category object ${objectCategory}`);
        if(objectCategory) {
          arrayCategories.push(objectCategory._id);
        }
      }
      post.categories = arrayCategories;
      let postModel = new Post(post);
      logger.info(`--MUTATION.POST.CREATEPOST-- post model object ${postModel}`);
      return await postModel.save();
    },
    deletePost: async (parent, {id}) => {
      logger.info(`--MUTATION.POST.DELETEPOST-- id post = ${id}`)
      let data = await Post.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.POST.DELETEPOST-- response object = ${data}`)
        return null
      }
      return data;
    }
  }
};
