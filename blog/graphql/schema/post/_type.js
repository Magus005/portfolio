const Post = `
  type Post {
    _id: String
    picture: Upload
    movie: Upload
    content: String!
    title: String!
    userId: String
    dateCreated: Date
    categories: [String]
    state: Boolean
    createdAt: Date
    updatedAt: Date
  }
`;

export const types = () => [Post];

export const typeResolvers = {

};
