'use strict';
import mongoose from 'mongoose';

var CategoryModel = new mongoose.Schema({
  name: {
    type: String,
    index: true,
    required: true
  },
  state: {
    type: Boolean,
    default: true
  }
},{timestamps: true});

CategoryModel.index({
  name: 1
});
export default mongoose.model('Category', CategoryModel);
