const Category = `
  type Category {
    _id: String
    name: String!
  }
`;

export const types = () => [Category];

export const typeResolvers = {

};
