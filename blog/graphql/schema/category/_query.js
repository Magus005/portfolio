import logger from '../../../components/logger';
import Category from './model';

const Query = `
  extend type Query {
    getCategories: [Category]
    getCategory(id: ID!): Category
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getCategories: async () => {
      return await Category.find();
    },
    getCategory: async (parent, { id }) => {
      logger.info(`--QUERY.CATEGORY.GETCATEGORY-- id category = ${id}`)
      let data = await Category.findById(id);
      if(!data) {
        return null
      }
      logger.info(`--QUERY.CATEGORY.GETCATEGORY-- response object = ${data}`)
      return data
    }
  }
};
