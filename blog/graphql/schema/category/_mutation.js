import logger from '../../../components/logger';
import Category from './model';

const Mutation = `
  extend type Mutation {
    createCategory(name: String!): Category
    updateCategory(id: ID!, name: String!): Category
    deleteCategory(id: ID!): Category
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createCategory: async (parent, { name }) => {
      let category = new Category();
      category.name = name;
      logger.info(`--MUTATION.CATEGORY.CREATECATEGORY-- object ${category}`);
      return await category.save();
    },
    updateCategory: async (parent, {id, name}) => {
      logger.info(`--MUTATION.CATEGORY.UPDATECATEGORY-- id category = ${id}`)
      let data = await Category.findOneAndUpdate({_id: id}, {$set:{name}}, {new: true});
      if(!data) {
        logger.info(`--MUTATION.CATEGORY-- response object = ${data}`)
        return null
      }
      return data;
    },
    deleteCategory: async (parent, {id}) => {
      logger.info(`--MUTATION.CATEGORY.DELETECATEGORY-- id category = ${id}`)
      let data = await Category.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.CATEGORY.DELETECATEGORY-- response object = ${data}`)
        return null
      }
      return data;
    }
  }
};
