const Comment = `
  type Comment {
    firstname: String!
    lastname: String!
    content: String!
    createdAt: String!
    updatedAt: String!
    post: String!
  }
`;

export const types = () => [Comment];

export const typeResolvers = {

};
