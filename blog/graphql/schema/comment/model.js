'use strict';
import mongoose from 'mongoose';

var CommentModel = new mongoose.Schema({
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  post: {
    type: mongoose.Schema.ObjectId,
    ref: 'Post',
    index: true,
  },
  state: {
    type: Boolean,
    default: true
  }
},{timestamps: true});


CommentModel.index({
  post: 1
});
export default mongoose.model('Comment', CommentModel);
