const Input = `
  input InputComment {
    firstname: String!
    lastname: String!
    content: String!
    post: InputPost!
  }
`;

export default () => [Input];
