import Comment from './model';
import logger from '../../../components/logger'

const Query = `
  extend type Query {
    getAllComments: [Comment]
    getComment(id: ID!): Comment
    getAllCommentByPost(id: ID!): Comment
  }
`;

export const queryTypes = () => [Query];

export const queryResolvers = {
  Query: {
    getAllComments: async () => {
      return await Comment.find();
    },
    getComment: async (parent, { id }) => {
      logger.info(`--QUERY.COMMENT.GETCOMMENT-- id comment = ${id}`)
      let data = await Comment.findById(id);
      if(!data) {
        return null
      }
      logger.info(`--QUERY.COMMENT.GETCOMMENT-- response object = ${data}`)
      return data
    },
    getAllCommentByPost: async (parent, { id }) => {
      logger.info(`--QUERY.COMMENT.GETALLCOMMENTBYPOST-- id post = ${id}`)
      let data = await Comment.findOne({post: `${id}`});
      if(!data) {
        return null
      }
      logger.info(`--QUERY.COMMENT.GETALLCOMMENTBYPOST-- response object = ${data}`)
      return data
    }
  }
};
