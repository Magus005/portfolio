import logger from '../../../components/logger';
import Post from '../post/model';
import Comment from './model';

const Mutation = `
  extend type Mutation {
    createComment(comment: InputComment): Comment
    deleteComment(id: ID!): Comment
  }
`;

export const mutationTypes = () => [Mutation];

export const mutationResolvers = {
  Mutation: {
    createComment: async (parent, { comment }) => {
      let commentModel = new Comment(comment);
      try {
        logger.info(`--MUTATION.COMMENT.CREATECOMMENT-- object model comment = ${JSON.stringify(commentModel)}`)
        return await commentModel.save();
      } catch (error) {
        logger.info(`--MUTATION.COMMENT.CREATECOMMENT-- error save comment = ${error}`)
        return error
      }
    },
    deleteComment: async (parent, {id}) => {
      logger.info(`--MUTATION.COMMENT.DELETECOMMENT-- id comment = ${id}`)
      let data = await Post.findByIdAndRemove(id);
      if(!data) {
        logger.info(`--MUTATION.COMMENT.DELETECOMMENT-- response object = ${data}`)
        return null
      }
      return data;
    }
  }
};
