package com.madiara.personal.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Cv {
  @Id
  public ObjectId _id;
  
  public String path;
  public String name;
  public Boolean state;

  public Cv() {}
  public Cv(ObjectId _id, String path, String name, Boolean state) {
    this._id = _id;
    this.name = name;
    this.path = path;
    this.state = state;
  }

  public String getId() {
    return this._id.toHexString();
  }
  public void setId(ObjectId _id) {
    this._id = _id;
  }

  public String getPath() {
    return this.path;
  }
  public void setPath(String path) {
    this.path = path;
  }

  public Boolean getState() {
    return this.state;
  }
  public void setState(Boolean state) {
    this.state = state;
  }

  public String getName() {
    return this.name;
  }
  public void setName(String name) {
    this.name = name;
  }
}