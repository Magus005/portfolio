package com.madiara.personal.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Location {
  @Id
  public ObjectId _id;
  
  public String latitude;
  public String longitude;
  public String zoom;

  public Location() {}
  public Location(ObjectId _id, String latitude, String longitude, String zoom) {
    this._id = _id;
    this.latitude = latitude;
    this.longitude = longitude;
    this.zoom = zoom;
  }

  public String getId() {
    return this._id.toHexString();
  }
  public void setId(ObjectId _id) {
    this._id = _id;
  }

  public String getLatitude() {
    return this.latitude;
  }
  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return this.longitude;
  }
  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getZoom() {
    return this.zoom;
  }
  public void setZoom(String zoom) {
    this.zoom = zoom;
  }

}