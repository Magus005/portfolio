package com.madiara.personal.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Users {
  @Id
  public ObjectId _id;
  
  public String name;
  public String password;
  public String email;
  public String role;
  public Boolean state;

  public Users() {}
  public Users(String name, String password, String email, String role) {
    //this._id = _id;
    this.name = name;
    this.password = password;
    this.email = email;
    this.role = role;
    this.state = true;
  }

  public String getId() {
    return this._id.toHexString();
  }
  public void setId(ObjectId _id) {
    this._id = _id;
  }
  
  public String getName() {
    return this.name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public String getPassword() {
    return this.password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  
  public String getEmail() {
    return this.email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getRole() {
    return this.role;
  }
  public void setRole(String role) {
    this.role = role;
  }
  
  public Boolean getState() {
    return this.state;
  }
  public void setState(Boolean state) {
    this.state = state;
  }
}