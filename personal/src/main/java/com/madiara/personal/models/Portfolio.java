package com.madiara.personal.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Portfolio {
  @Id
  public ObjectId _id;
  
  public String title;
  public String picture;
  public String customer;
  public String delivered;
  public String content;
  public Boolean state;

  public Portfolio() {}
  public Portfolio(ObjectId _id, String title, String picture, String customer, String delivered, String content, Boolean state) {
    this._id = _id;
    this.title = title;
    this.picture = picture;
    this.customer = customer;
    this.delivered = delivered;
    this.content = content;
    this.state = state;
  }

  public String getId() {
    return this._id.toHexString();
  }
  public void setId(ObjectId _id) {
    this._id = _id;
  }
  
  public String getTitle() {
    return this.title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getPicture() {
    return this.picture;
  }
  public void setPicture(String picture) {
    this.picture = picture;
  }
  
  public String getCustomer() {
    return this.customer;
  }
  public void setCustomer(String customer) {
    this.customer = customer;
  }
  
  public String getDelivered() {
    return this.delivered;
  }
  public void setDelivered(String delivered) {
    this.delivered = delivered;
  }
  
  public String getContent() {
    return this.content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  
  public Boolean getState() {
    return this.state;
  }
  public void setState(Boolean state) {
    this.state = state;
  }
}