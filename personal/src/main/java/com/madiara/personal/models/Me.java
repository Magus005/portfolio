package com.madiara.personal.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Me {
  @Id
  public ObjectId _id;
  
  public String picture;
  public String name;
  public String logo;
  public String title;
  public Boolean state;

  public Me() {}
  public Me(ObjectId _id, String picture, String name, String logo, String title, Boolean state) {
    this._id = _id;
    this.picture = picture;
    this.name = name;
    this.logo = logo;
    this.title = title;
    this.state = state;
  }

  public String getId() {
    return this._id.toHexString();
  }
  public void setId(ObjectId _id) {
    this._id = _id;
  }

  public String getPicture() {
    return this.picture;
  }
  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getName() {
    return this.name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return this.logo;
  }
  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getTitle() {
    return this.title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  
  public Boolean getState() {
    return this.state;
  }
  public void setState(Boolean state) {
    this.state = state;
  }
}