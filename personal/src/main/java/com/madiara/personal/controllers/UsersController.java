package com.madiara.personal.controllers;

import java.util.List;

import com.madiara.personal.models.Users;
import com.madiara.personal.repositories.UsersRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RestController
@RequestMapping("/users")
public class UsersController {
  @Autowired
  private UsersRepository repository;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  @CrossOrigin()
  public List<Users> getAllUsers() {
    return repository.findAll();
  }

  @RequestMapping(value="/{id}", method=RequestMethod.GET)
  @CrossOrigin()
  public Users getUserById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  @CrossOrigin()
	public @ResponseBody Users createUser(@RequestBody Users user) {
    user.setId(ObjectId.get());
    user.setState(true);
    user.setPassword(hash(user.password));
    System.out.println("--PERSONAL.USERS.CREATEUSER-- Object user = "+user);
    repository.save(user);
		return user;
  }

  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  @CrossOrigin()
  public void deleteUser(@PathVariable ObjectId id){
    repository.delete(repository.findBy_id(id));
  }

  private String hash(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String hashedPassword = passwordEncoder.encode(password);
    return hashedPassword;
  }

  private Boolean verif(String password, String hash) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    return passwordEncoder.matches(password, hash);
  }
}