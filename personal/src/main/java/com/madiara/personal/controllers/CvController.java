package com.madiara.personal.controllers;

import com.madiara.personal.models.Cv;
import com.madiara.personal.repositories.CvRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/cv")
public class CvController {
  @Autowired
  private CvRepository repository;

  String root = "/Volumes/PC-1/MAC/OwnProject/portfolio/personal/src";
  String UPLOAD_DIR = root + "/main/resources/static/images/";

  @RequestMapping(value = "/", method = RequestMethod.GET)
  @CrossOrigin()
  public List<Cv> getAllCv() {
    return repository.findAll();
  }

  @RequestMapping(value="/{id}", method=RequestMethod.GET)
  @CrossOrigin()
  public Cv getCvById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  @CrossOrigin()
	public @ResponseBody Cv createCv(@RequestBody Cv cv) throws IOException {
    cv.setId(ObjectId.get());
    cv.setState(true);
    System.out.println("--PERSONAL.CV.CREATECV-- Object cv = "+cv);
    repository.save(cv);
		return cv;
  }

  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  @CrossOrigin()
  public void deleteCv(@PathVariable ObjectId id){
    repository.delete(repository.findBy_id(id));
  }
}