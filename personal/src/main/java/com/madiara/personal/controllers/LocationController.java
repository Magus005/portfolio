package com.madiara.personal.controllers;

import com.madiara.personal.models.Location;
import com.madiara.personal.repositories.LocationRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {
  @Autowired
  private LocationRepository repository;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  @CrossOrigin()
  public List<Location> getAllLocation() {
    return repository.findAll();
  }

  @RequestMapping(value="/{id}", method=RequestMethod.GET)
  @CrossOrigin()
  public Location getLocationById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  @CrossOrigin()
	public @ResponseBody Location createLocation(@RequestBody Location location) throws IOException {
    location.setId(ObjectId.get());
    System.out.println("--PERSONAL.Location.CREATELOCATION-- Object location = "+location);
    repository.save(location);
		return location;
  }

  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  @CrossOrigin()
  public void deleteLocation(@PathVariable ObjectId id){
    repository.delete(repository.findBy_id(id));
  }
}