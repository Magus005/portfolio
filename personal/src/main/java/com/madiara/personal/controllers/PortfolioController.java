package com.madiara.personal.controllers;

import com.madiara.personal.models.Portfolio;
import com.madiara.personal.repositories.PortfolioRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/portfolio")
public class PortfolioController {
  @Autowired
  private PortfolioRepository repository;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  @CrossOrigin()
  public List<Portfolio> getAllPortfolio() {
    return repository.findAll();
  }

  @RequestMapping(value="/{id}", method=RequestMethod.GET)
  @CrossOrigin()
  public Portfolio getPortfolioById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  @CrossOrigin()
	public @ResponseBody Portfolio createPortfolio(@RequestBody Portfolio portfolio) throws IOException {
    portfolio.setId(ObjectId.get());
    portfolio.setState(true);
    System.out.println("--PERSONAL.PORTFOLIO.CREATEME-- Object portfolio = "+portfolio);
    repository.save(portfolio);
		return portfolio;
  }

  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
  @CrossOrigin()
  public void deletePortfolio(@PathVariable ObjectId id){
    repository.delete(repository.findBy_id(id));
  }
}