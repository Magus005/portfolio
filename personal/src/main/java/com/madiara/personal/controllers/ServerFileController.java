package com.madiara.personal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.Random;
 
@RestController
public class ServerFileController {

  String root = "/Users/teamx/OwnProject/portfolio/personal/src";
	String UPLOAD_DIR = root + "/main/resources/static/images/";
 
	@Autowired
	public ServerFileController() {}
 
	@PostMapping(value = "/api/files")
  @ResponseStatus(HttpStatus.OK)
  @CrossOrigin()
	public @ResponseBody String handleFileUpload(@RequestParam(value = "file") MultipartFile file) throws IOException {
    String filename = uploadFile(file);
    return filename;
	}

  private String uploadFile(MultipartFile file) throws IOException {
		String fileExtension = getFileExtension(file);
		String filename = getRandomString();
 
		File targetFile = getTargetFile(fileExtension, filename);
 
		byte[] bytes = file.getBytes();
    System.out.println(bytes);
		file.transferTo(targetFile);
    String UploadedDirectory = targetFile.getAbsolutePath();
    System.out.println(String.format("--PERSONAL.SERVERFILE-- uploaded directory = %s", UploadedDirectory));
    return String.format("%s%s", filename, fileExtension);
  }
 
	private String getFileExtension(MultipartFile inFile) {
		String fileExtention = inFile.getOriginalFilename().substring(inFile.getOriginalFilename().lastIndexOf('.'));
		return fileExtention;
  }
  
	private String getRandomString() {
		return new Random().nextInt(999999) + "_" + System.currentTimeMillis();
	}
 
	private File getTargetFile(String fileExtn, String fileName) {
		File targetFile = new File(UPLOAD_DIR + fileName + fileExtn);
		return targetFile;
	}
}