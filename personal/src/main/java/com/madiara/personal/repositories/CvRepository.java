package com.madiara.personal.repositories;

import com.madiara.personal.models.Cv;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CvRepository extends MongoRepository<Cv, String> {
  Cv findBy_id(ObjectId _id);
}