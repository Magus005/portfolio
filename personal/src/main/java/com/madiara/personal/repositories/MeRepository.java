package com.madiara.personal.repositories;

import com.madiara.personal.models.Me;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MeRepository extends MongoRepository<Me, String> {
  Me findBy_id(ObjectId _id);
}