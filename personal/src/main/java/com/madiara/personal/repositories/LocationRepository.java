package com.madiara.personal.repositories;

import com.madiara.personal.models.Location;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LocationRepository extends MongoRepository<Location, String> {
  Location findBy_id(ObjectId _id);
}