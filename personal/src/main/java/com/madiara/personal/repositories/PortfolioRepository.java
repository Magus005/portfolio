package com.madiara.personal.repositories;

import com.madiara.personal.models.Portfolio;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PortfolioRepository extends MongoRepository<Portfolio, String> {
  Portfolio findBy_id(ObjectId _id);
}