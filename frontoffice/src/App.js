import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Home from './pages/home';
import ListBlog from './pages/list-blog';
import DetailBlog from './pages/detail-blog';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Router>
        <div>
          <Route path="/" component={Home} exact />
          <Route path="/blog" component={ListBlog} exact />
          <Route path="/detail-blog" component={DetailBlog} exact />
        </div>
      </Router>
    );
  }
}

export default App;
