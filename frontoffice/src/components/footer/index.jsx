import React from 'react';

const Footer = props => {
  return (
    <footer id="crtFooter" className="crt-container-lg">
      <div className="crt-container">
        <div className="crt-container-sm clear-mrg text-center">
          <p>Madiara Gassama @ All Rights Reserved 2019</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
