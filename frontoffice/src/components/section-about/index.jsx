import React, { Fragment } from 'react';
import SharedButton from '../shared/shared-button';
import AboutContent from './content';
import AboutIntro from './intro';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ABOUT_ACTIVE = gql`query getAboutActiveQuery {
  getAboutActive {
    _id,
    title,
    content
  }
}`;

const SectionAbout = props => {

  return (
    <ApolloProvider client={client}>
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="about" className="section padd-box">
            <div className="row">
              <div className="col-sm-12 clear-mrg text-box">
                <h2 className="title-lg text-upper">About Me</h2>
                <Query query={GET_ABOUT_ACTIVE}>
                  {
                    ({loading, error, data}) => {
                      if(loading) return <h2>Loading ...</h2>
                      if(error) console.log(`--SECTION.ABOUT.GET_ABOUT_ACTIVE-- ${JSON.stringify(error)}`);
                      return <Fragment>
                        <AboutIntro intro={data.getAboutActive.title} />
                        <AboutContent content={data.getAboutActive.content} />
                      </Fragment>
                    }
                  }
                </Query>
                <div className="share-box text-left clearfix">
                  <SharedButton />
                  <div className="addthis_inline_share_toolbox"></div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    </ApolloProvider>
  );
}

export default SectionAbout;
