import React, { useState, useEffect } from 'react';


const AboutContent = props => {
  const [content, setContent] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.ABOUT.AboutContent-- props = ${JSON.stringify(props)}`);
    setContent(props);
  }, [props]);

  return (
    <p>{content.content}</p>
  );
}


export default AboutContent;
