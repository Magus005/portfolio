import React, { useState, useEffect } from 'react';

const AboutIntro = props => {
  const [intro, setIntro] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.ABOUT.AboutIntro-- props = ${JSON.stringify(props)}`);
    setIntro(props);
  }, [props]);
  return (
    <p>
      <b>{intro.intro}</b>
    </p>
  );
}

export default AboutIntro;
