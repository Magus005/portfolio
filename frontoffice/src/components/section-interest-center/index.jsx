import React from 'react';
import Interest from './interest';


const SectionInterestCenter = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="interests" className="section padd-box">
            <Interest />
          </section>
        </div>
      </div>
    </div>
  );
}

export default SectionInterestCenter;
