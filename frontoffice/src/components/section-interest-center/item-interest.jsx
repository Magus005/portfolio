import React, { useState, useEffect } from 'react';

const ItemInterest = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.ABOUT.AboutContent-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <li>
      <span className={'crt-icon crt-icon ' + object.icon}></span>
      {object.name}
    </li>
  );
}

export default ItemInterest;
