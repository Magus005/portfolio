import React, { Fragment } from 'react';
import ItemInterest from './item-interest';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_CENTER_OF_INTERESTS = gql`query getAllCenterOfInterestsQuery {
  getAllCenterOfInterests {
    _id
    name
    icon
  }
}`;


const Interest = props => {
  return (
    <ApolloProvider client={client}>
      <div className="row">
        <div className="col-sm-12 clear-mrg">
          <h2 className="title-lg text-upper">Interests</h2>

          <ul className="crt-icon-list crt-icon-list-col3 clearfix">
            <Query query={GET_ALL_CENTER_OF_INTERESTS}>
              {
                ({loading, error, data}) => {
                  if(loading) return <h2>Loading ...</h2>
                  if(error) console.log(`--SECTION.INTEREST.CENTER-- ${JSON.stringify(error)}`);
                  return <Fragment>
                    {
                      data.getAllCenterOfInterests.map((centerOfInterest, index) => (
                        <ItemInterest value={centerOfInterest} key={centerOfInterest._id} />
                      ))
                    }
                  </Fragment>
                }
              }
            </Query>
          </ul>
        </div>
      </div>
    </ApolloProvider>
  );
}

export default Interest;
