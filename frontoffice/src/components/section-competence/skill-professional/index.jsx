import React, { Fragment } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import ColProgressBar from './col-progress-bar';
import ColCircleBar from './col-circle-bar';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_SKILLS = gql`query getAllSkillsQuery {
  getAllSkills {
    _id
    name
    level
    type
    state
  }
}`;


const SkillProfessional = props => {
  return (
    <ApolloProvider client={client}>
    <section id="data" className="section padd-box brd-btm">
      <div className="row">
        <Query query={GET_ALL_SKILLS}>
          {
            ({loading, error, data}) => {
              if(loading) return <h2>Loading ...</h2>
              if(error) console.log(`--SECTION.COMPETENCE.GET_ALL_SKILLS-- ${JSON.stringify(error)}`);
              console.log(`--SECTION.COMPETENCE.GET_ALL_SKILLS-- ${JSON.stringify(data)}`);
              let i = 0;
              var table2 = []
              var table1 = []
              data.getAllSkills.forEach(skill => {
                if(data.getAllSkills.length - i <= i) {
                  table2.push(skill);
                } else {
                  table1.push(skill);
                }
                console.log(`--SECTION.COMPETENCE.GET_ALL_SKILLS-- table1 ${data.getAllSkills.length - i}`);
                i += 1;
                console.log(`--SECTION.COMPETENCE.GET_ALL_SKILLS-- table1 ${JSON.stringify(table1)}`);
                console.log(`--SECTION.COMPETENCE.GET_ALL_SKILLS-- table2 ${JSON.stringify(table2)}`);
              });
              return <Fragment>
                <ColProgressBar value={table1} />
                <ColCircleBar value={table2} />
              </Fragment>
            }
          }
        </Query>

      </div>
    </section>
    </ApolloProvider>
  );
}

export default SkillProfessional;
