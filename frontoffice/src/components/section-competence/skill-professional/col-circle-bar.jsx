import React, { useState, useEffect } from 'react';
import ItemCircleBar from '../../shared/item-circle-bar';


const ColCircleBar = props => {
  const [objects, setObjects] = useState([{}]);
  useEffect(() => {
    console.log(`--SKILL-PROFESSIONAL.COLCIRCLEBAR-- props = ${JSON.stringify(props)}`);
    setObjects(props.value)
  }, [props]);


  return (
    <div className="col-sm-6 clear-mrg">
      <h3 className="title-thin text-muted">Professional Skills</h3>
      <div className="row">
      {
        objects.map(object => (
          <ItemCircleBar key={object} value={object} />
        ))
      }

      </div>
    </div>
  );
}

export default ColCircleBar;
