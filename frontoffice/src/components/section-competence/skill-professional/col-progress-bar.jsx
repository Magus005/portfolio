import React, { useState, useEffect } from 'react';
import ItemProgressBar from '../../shared/item-progress-bar';

const ColProgressBar = props => {
  const [objects, setObjects] = useState([{}]);
  useEffect(() => {
    console.log(`--SKILL-PROFESSIONAL.COLCIRCLEBAR-- props = ${JSON.stringify(props)}`);
    setObjects(props.value)
  }, [props]);

  return (
    <div className="col-sm-6 clear-mrg">
      <h3 className="title-thin text-muted">Professional Skills</h3>
      {
        objects.map((object, index) => (
          <ItemProgressBar key={index} name={object.name} level={object.level} />
        ))
      }

    </div>
  );
}

export default ColProgressBar;
