import React from 'react';
import Skill from './skill';
import SkillProfessional from './skill-professional';
import Tool from './tool';

const SectionCompetence = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <Skill />

          <SkillProfessional />
          
          <Tool />
        </div>
      </div>
    </div>
  );
}

export default SectionCompetence;
