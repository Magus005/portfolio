import React from 'react';
import PersonalInformation from './personal-information/index';
import SkillLanguage from './skill-language/index';


const Skill = props => {
  return (
    <section id="skills" className="section padd-box brd-btm">
      <div className="row">
        <PersonalInformation />

        <SkillLanguage />
      </div>
    </section>
  );
}

export default Skill;
