import React, { useState, useEffect } from 'react';
import ItemBullet from '../../../shared/item-bullet';


const ItemSkillLanguage = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--SECTION.COMPETENCE.ItemSkillLanguage-- props = ${JSON.stringify(props)}`);
    setObject(props);
  }, [props]);
  return (
    <div className="progress-bullets crt-animate" role="progressbar" aria-valuenow="97" aria-valuemin="0"
      aria-valuemax="10">
      <strong className="progress-title">{object.name}</strong>
      <span className="progress-bar">
        <ItemBullet />
        <ItemBullet />
        <ItemBullet />
        <ItemBullet />
        <ItemBullet />
      </span>

      <span className="progress-text text-muted">native</span>
    </div>
  );
}

export default ItemSkillLanguage;
