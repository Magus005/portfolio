import React, { Fragment } from 'react';
import ItemSkillLanguage from './item-skill-language';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_LANGUAGES = gql`query getAllLanguagesQuery {
  getAllLanguages {
    _id
    name
    level
  }
}`;

const SkillLanguage = props => {
  return (

    <div className="col-sm-6 clear-mrg">
      <h3 className="title-thin text-muted">languages</h3>
      <ApolloProvider client={client}>
        <Query query={GET_ALL_LANGUAGES}>
        {
          ({loading, error, data}) => {
            if(loading) return <h2>Loading ...</h2>
            if(error) console.log(`--SECTION.COMPETENCE.GET_ALL_LANGUAGES-- ${JSON.stringify(error)}`);
            console.log(`--SECTION.COMPETENCE.GET_ALL_LANGUAGES-- ${JSON.stringify(data)}`);
            return <Fragment>
              {
                data.getAllLanguages.map((language, index) => (
                  <ItemSkillLanguage key={index} name={language.name} level={language.level} />
                ))
              }
            </Fragment>
          }
        }
        </Query>
      </ApolloProvider>
    </div>
  );
}

export default SkillLanguage;
