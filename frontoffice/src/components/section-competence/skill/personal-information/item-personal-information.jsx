import React, { Fragment, useState, useEffect } from 'react';

const ItemPersonalInformation = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--SECTION.COMPETENCE.ItemPersonalInformation-- props = ${JSON.stringify(props)}`);
    setObject(props);
  }, [props]);

  return (
    <Fragment>
      <dt className="text-upper">{object.name}</dt>
      <dd>{object.value}</dd>
    </Fragment>
  );
}

export default ItemPersonalInformation;
