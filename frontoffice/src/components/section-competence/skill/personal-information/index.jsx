import React, { Fragment } from 'react';
import ItemPersonalInformation from './item-personal-information';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_INFORMATION_ACTIVE = gql`query getInformationActiveQuery {
  getInformationActive {
    _id
    fullname
    dateOfBirth
    address
    email
    phoneNumber
  }
}`;

const PersonalInformation = props => {
  return (
    <ApolloProvider client={client}>
    <div className="col-sm-6 clear-mrg">
      <h3 className="title-thin text-muted">personal information</h3>
      <dl className="dl-horizontal clear-mrg">
      <Query query={GET_INFORMATION_ACTIVE}>
      {
        ({loading, error, data}) => {
          if(loading) return <h2>Loading ...</h2>
          if(error) console.log(`--SECTION.COMPETENCE.GET_INFORMATION_ACTIVE-- ${JSON.stringify(error)}`);
          return <Fragment>
            <ItemPersonalInformation value={data.getInformationActive.fullname} name="Prénom & Nom" />
            <ItemPersonalInformation value={data.getInformationActive.address} name="Adresse" />
            <ItemPersonalInformation value={data.getInformationActive.email} name="E-mail" />
            <ItemPersonalInformation value={data.getInformationActive.phoneNumber} name="Téléphone" />
          </Fragment>
        }
      }
      </Query>
      </dl>
    </div>
    </ApolloProvider>
  );
}

export default PersonalInformation;
