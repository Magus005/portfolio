import React, { useState, useEffect } from 'react';

const ItemIconTool = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--ITEM.ICON.TOOL-- props = ${JSON.stringify(props)}`);
    setObject(props);
  }, [props]);
  return (
    <li>
      <span className="crt-icon crt-icon-music"></span>
      {object.name}
    </li>
  );
}

export default ItemIconTool;
