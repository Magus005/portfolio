import React, { useState, useEffect } from 'react';
import ToolTitle from '../title';
import ItemIconTool from './item-icon-tool';


const ColumnIcon = props => {
  const [objects, setObjects] = useState([{}]);
  useEffect(() => {
    console.log(`--COLUMN.ICON-- props = ${JSON.stringify(props)}`);
    setObjects(props.value);
  }, [props]);
  return (
    <div className="col-sm-6 clear-mrg">
      <ToolTitle />
      <ul className="crt-icon-list">
        {
          objects.map(object => (
            <ItemIconTool key={object._id} name={object.name} />
          ))
        }
      </ul>
    </div>
  );
}

export default ColumnIcon;
