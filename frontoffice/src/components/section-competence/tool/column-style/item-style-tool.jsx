import React, { useEffect, useState } from 'react';

const ItemStyleTool = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--ITEM.STYLE.TOOL-- props = ${JSON.stringify(props)}`);
    setObject(props);
  }, [props]);
  return (
    <li>{object.name}</li>
  );
}

export default ItemStyleTool;
