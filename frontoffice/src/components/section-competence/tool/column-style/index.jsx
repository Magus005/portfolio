import React, { useState, useEffect } from 'react';
import ToolTitle from '../title';
import ItemStyleTool from './item-style-tool';


const ColumnStyle = props => {
  const [objects, setObjects] = useState([{}]);
  useEffect(() => {
    console.log(`--COLUMN.ICON-- props = ${JSON.stringify(props)}`);
    setObjects(props.value);
  }, [props]);
  return (
    <div className="col-sm-6 clear-mrg">
      <ToolTitle />
      <ul className="styled-list clear-mrg">
        {
          objects.map(object => (
            <ItemStyleTool key={object._id} name={object.name} />
          ))
        }
      </ul>
    </div>
  );
}

export default ColumnStyle;
