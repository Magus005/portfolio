import React, { Fragment } from 'react';
import ColumnIcon from './column-icon';
import ColumnStyle from './column-style';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_TOOLS = gql`query getAllToolsQuery {
  getAllTools {
    _id
    name
  }
}`;


const Tool = props => {
  return (
    <ApolloProvider client={client}>
      <section id="data" className="section padd-box">
        <div className="row">
          <Query query={GET_ALL_TOOLS}>
          {
            ({loading, error, data}) => {
              if(loading) return <h2>Loading ...</h2>
              if(error) console.log(`--SECTION.TOOL.GET_ALL_TOOLS-- ${JSON.stringify(error)}`);
              console.log(`--SECTION.TOOL.GET_ALL_TOOLS-- ${JSON.stringify(data)}`);
              let i = 0;
              var table2 = []
              var table1 = []
              data.getAllTools.forEach(skill => {
                if(data.getAllTools.length - i <= i) {
                  table2.push(skill);
                } else {
                  table1.push(skill);
                }
                console.log(`--SECTION.TOOL.GET_ALL_TOOLS-- table1 ${data.getAllTools.length - i}`);
                i += 1;
                console.log(`--SECTION.TOOL.GET_ALL_TOOLS-- table1 ${JSON.stringify(table1)}`);
                console.log(`--SECTION.TOOL.GET_ALL_TOOLS-- table2 ${JSON.stringify(table2)}`);
              });
              return (
                <Fragment>
                  <ColumnStyle value={table1} />
      
                  <ColumnIcon value={table2} />
                </Fragment>
              )
            }
          }
          </Query>
        </div>
      </section>
    </ApolloProvider>
  );
}

export default Tool;
