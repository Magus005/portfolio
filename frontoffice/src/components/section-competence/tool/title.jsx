import React from 'react';

const ToolTitle = props => {
  return (
    <h3 className="title-thin text-muted">Icon List</h3>
  );
}

export default ToolTitle;
