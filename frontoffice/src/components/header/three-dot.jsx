import React from 'react';

const HeaderThreeDot = props => {
  return (
    <div id="crtNavTools" className="hidden">
      <span className="crt-icon crt-icon-dots-three-horizontal"></span>
      <button id="crtNavArrow" className="clear-btn">
        <span className="crt-icon crt-icon-chevron-thin-down"></span>
      </button>
    </div>
  );
}

export default HeaderThreeDot;
