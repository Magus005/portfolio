import React from 'react';

const HeaderButtonMenu = props => {
  return (
    <div id="crtHeadCol3" className="crt-head-col text-right">
      <button id="crtSidebarBtn" className="btn btn-icon btn-shade">
        <span className="crt-icon crt-icon-side-bar-icon"></span>
      </button>
    </div>
  );
}

export default HeaderButtonMenu;
