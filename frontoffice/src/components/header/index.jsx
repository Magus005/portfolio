import React, { useState, useEffect } from 'react';
import HeaderLogo from './logo';
import HeaderNavBar from './nav-bar/nav-bar';
import HeaderButtonMenu from './button-menu';
import HeaderNavBarMobile from './nav-bar-mobile/index';
import Axios from 'axios';

const Header = props => {
  const [me, setMe] = useState({})
  useEffect(() => {
    async function fetchDataMe() {
      let result = await Axios.get('http://127.0.0.1:8080/me/');
      console.log(`--HEADER-Component-- me ${JSON.stringify(result.data[0])}`);
      setMe(result.data[0]);
    }
    fetchDataMe();
  }, []);
  return (
    <header id="crtHeader" className="crt-logo-in">
      <div className="crt-head-inner crt-container">
        <div className="crt-container-sm">
          <div className="crt-head-row">
            <HeaderLogo />
            <HeaderNavBar />
            <HeaderButtonMenu />
          </div>
        </div>
      </div>
      <HeaderNavBarMobile avatarProfile={me} />
    </header>
  );
}

export default Header;
