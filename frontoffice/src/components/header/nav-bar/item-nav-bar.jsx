import React, { useState, useEffect } from 'react';

const ItemHeaderNavBar = props => {
  const [value, setValue] = useState("");
  const [link, setLink] = useState("");
  useEffect(() => {
    if(props.value) {
      console.log(`--SECTION.CAREER.PATH.EDUCATION.COMPANY--  props = ${JSON.stringify(props)}`);
      setValue(props.value);
    }
    if(props.link) {
      setLink(props.link);
    }
  }, [props]);
  return (
    <li id="menu-item-148" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-90 current_page_item menu-item-148">
      <a href={link}>{value}</a>
    </li>
  );
}

export default ItemHeaderNavBar;
