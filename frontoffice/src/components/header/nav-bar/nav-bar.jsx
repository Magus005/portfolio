import React from 'react';
import ItemHeaderNavBar from './item-nav-bar';

const HeaderNavBar = props => {
  return (
    <div id="crtHeadCol2" className="crt-head-col text-right">
      <div className="crt-nav-container crt-container hidden-sm hidden-xs">
        <nav id="crtMainNav">
          <ul className="clear-list">
            <ItemHeaderNavBar value="Home" link="/" />
            <ItemHeaderNavBar value="Blog" link="/blog" />
          </ul>
        </nav>
      </div>
    </div>
  );
}

export default HeaderNavBar;
