import React, { useEffect, useState } from 'react';

const AvatarNavBarMobile = props => {
  const [userAvatar, setUserAvatar] = useState({});
  useEffect(() => {
    if(props) {
      setUserAvatar(props.userAvatar);
    }
  }, [props, userAvatar]);
  return (
    <div className="crt-avatar">
      <a href="#about" data-tooltip="About">
        <img alt="" src={userAvatar.picture}
          srcset={userAvatar.picture+ ' 2x'}
          className="avatar avatar-42" />
      </a>
    </div>
  );
}

export default AvatarNavBarMobile;
