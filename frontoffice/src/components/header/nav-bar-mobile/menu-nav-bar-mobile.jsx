import React from 'react';
import ItemMenuNavBarMobile from './item-menu-nav-bar-mobile';


const MenuNavBarMobile = props => {
  return (
    <ul className="clear-list">
      <ItemMenuNavBarMobile name="Skills" icon="crt-icon-portfolio" link="#skills" />
      <ItemMenuNavBarMobile name="Experience" icon="crt-icon-experience" link="#exp" />
      <ItemMenuNavBarMobile name="References" icon="crt-icon-references" link="#ref" />
      <ItemMenuNavBarMobile name="Blog" icon="crt-icon-blog" link="#blog" />
      <ItemMenuNavBarMobile name="Contacts" icon="crt-icon-contact" link="#contacts" />
    </ul>
  );
}

export default MenuNavBarMobile;
