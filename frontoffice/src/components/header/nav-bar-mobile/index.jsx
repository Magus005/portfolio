import React, { useState, useEffect } from 'react';
import MenuNavBarMobile from './menu-nav-bar-mobile';
import AvatarNavBarMobile from './avatar-nav-bar-mobile';

const HeaderNavBarMobile = props => {
  const [avatarProfile, setAvatarProfile] = useState({});
  useEffect(() => {
    if(props) {
      console.log(`--HEADERNAVBARMOBILE.PROPS-- userInfo ${JSON.stringify(avatarProfile)}`);
      setAvatarProfile(props.avatarProfile);
    }
  }, [props, avatarProfile]);
  
  return (
    <nav id="crtNavSm" className="crt-nav hidden-lg hidden-md">
      <AvatarNavBarMobile userAvatar={avatarProfile} />
      <MenuNavBarMobile />
    </nav>
  );
}

export default HeaderNavBarMobile;
