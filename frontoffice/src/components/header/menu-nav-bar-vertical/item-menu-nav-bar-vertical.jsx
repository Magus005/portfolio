import React, { useState, useEffect } from 'react';

const ItemHeaderMenuNavBarVertical = props => {
  const [name, setName] = useState("");
  const [link, setLink] = useState("");
  const [icon, setIcon] = useState("");
  useEffect(() => {
    if(props.name) {
      console.log(`--HEADER.ITEMHEADERMENUNAVBARVERTICAL--  props = ${JSON.stringify(props)}`);
      setName(props.name);
    }
    if(props.link) {
      setLink(props.link);
    }
    if(props.icon) {
      setIcon(props.icon);
    }
  }, [props]);

  return (
    <li className="menu-item menu-item-type-custom menu-item-object-custom menu-item-43">
      <a href={link} data-tooltip={name}>
        <span className={"crt-icon " + icon}></span>
      </a>
    </li>
  );
}

export default ItemHeaderMenuNavBarVertical;
