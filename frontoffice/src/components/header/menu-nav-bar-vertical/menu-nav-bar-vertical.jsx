import React from 'react';
import ItemHeaderMenuNavBarVertical from './item-menu-nav-bar-vertical';

const HeaderMenuNavBarVertical = props => {
  return (
    <ul className="clear-list">
      <ItemHeaderMenuNavBarVertical name="Skills" icon="crt-icon-portfolio" link="#skills" />
      <ItemHeaderMenuNavBarVertical name="Experience" icon="crt-icon-experience" link="#exp" />
      <ItemHeaderMenuNavBarVertical name="References" icon="crt-icon-references" link="#ref" />
      <ItemHeaderMenuNavBarVertical name="Blog" icon="crt-icon-blog" link="#blog" />
      <ItemHeaderMenuNavBarVertical name="Contacts" icon="crt-icon-contact" link="#contacts" />
    </ul>
  );
}

export default HeaderMenuNavBarVertical;
