import React from 'react';

const HeaderLogo = props => {
  return (
    <div id="crtHeadCol1" className="crt-head-col text-left">
      <a id="crtLogo" className="crt-logo" href="index.html">
        <img src="resources/images/certy_logo_wh.png" alt="Certy &#8211; Developer" />
        <span>.Developer</span>
      </a>
    </div>
  );
}

export default HeaderLogo;
