import React, { useEffect, useState } from 'react';
import AvatarNavBarVertical from './avatar-nav-bar-vertical';
import HeaderMenuNavBarVertical from '../menu-nav-bar-vertical/menu-nav-bar-vertical';

const NavBarVertical = props => {
  const [avatarProfile, setAvatarProfile] = useState({});
  useEffect(() => {
    if(props) {
      console.log(`--HEADERNAVBARMOBILE.PROPS-- userInfo ${JSON.stringify(avatarProfile)}`);
      setAvatarProfile(props.avatarProfile);
    }
  }, [props, avatarProfile]);
  return (
    <div id="crtNavScroll">
      <nav id="crtNav" className="crt-nav">
        <AvatarNavBarVertical userAvatar={avatarProfile} />
        <HeaderMenuNavBarVertical />
      </nav>
    </div>
  );
}

export default NavBarVertical;
