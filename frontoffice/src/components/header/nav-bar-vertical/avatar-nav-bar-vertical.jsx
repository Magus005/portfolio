import React, { useEffect, useState } from 'react';


const AvatarNavBarVertical = props => {
  const [userAvatar, setUserAvatar] = useState({});
  useEffect(() => {
    if(props) {
      setUserAvatar(props.userAvatar);
    }
  }, [props, userAvatar]);
  return (
    <div className="crt-nav-img">
      <a href="index.html#about" data-tooltip="About"> 
      <img alt=""
          src={userAvatar.picture}
          srcset={userAvatar.picture + ' 2x'}
          className="avatar avatar-42" />
      </a>
    </div>
  );
}

export default AvatarNavBarVertical;
