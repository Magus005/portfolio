import React, { useEffect, useState } from 'react';
import NavBarVertical from './nav-bar-vertical';
import HeaderThreeDot from '../three-dot';
import Axios from 'axios';

const HeaderNavBarVertical = props => {
  const [me, setMe] = useState({})
  useEffect(() => {
    async function fetchDataMe() {
      let result = await Axios.get('http://127.0.0.1:8080/me/');
      console.log(`--NAVBAR.Component-- me ${JSON.stringify(result.data[0])}`);
      setMe(result.data[0]);
    }
    fetchDataMe();
  }, []);
  return (
    <div id="crtNavWrap" className="hidden-sm hidden-xs">
      <div id="crtNavInner" className="crt-sticky">
        <div className="crt-nav-cont">
          <NavBarVertical avatarProfile={me} />
          <HeaderThreeDot />
        </div>
        <div className="crt-nav-btm"></div>
      </div>
    </div>
  );
}

export default HeaderNavBarVertical;
