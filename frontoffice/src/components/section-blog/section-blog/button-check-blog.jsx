import React from 'react';

const ButtonCheckBlog = props => {
  return (
    <p style={{textAlign: 'center'}}>
      <a className="btn btn-lg btn-default" target="_blank" rel="noopener noreferrer" href="blog/index.html">Check out my Blog</a>
    </p>
  );
}

export default ButtonCheckBlog;
