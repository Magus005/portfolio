import React from 'react';
import Content from './content';
import TitleBlock from '../../shared/title-block';
import ButtonCheckBlog from './button-check-blog';

const SectionBlog = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="blog" className="section padd-box">
            <div className="row">
              <div className="col-sm-12 clear-mrg text-box">
                <TitleBlock />
                <Content />
                <ButtonCheckBlog />
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default SectionBlog;
