import React, { useState, useEffect } from 'react';
import ItemFaq from './item-faq/index';
import Axios from 'axios';


const SectionFaq = props => {
  const [allFaq, setAllFaq] = useState([{}]);

  useEffect(() => {
    async function fetchDataPortfolio() {
      let result = await Axios.get('http://127.0.0.1:5020/faq/');
      console.log(`--SECTION.FAQ-- faq ${JSON.stringify(result)}`);
      setAllFaq(result.data);
    }
    fetchDataPortfolio();
  }, []);

  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="editor" className="section padd-box">
            <div className="row">
              <div className="col-sm-12 clear-mrg text-box">
                <h2 className="title-lg text-upper">FAQ</h2>
                <ul className="togglebox">
                  {
                    allFaq.map((faq, index) => (
                      <ItemFaq key={index} value={faq} />
                    ))
                  }
                  <li>
                    <h3 class="togglebox-header">What are my favorite frameworks and libs</h3>
                    <div class="togglebox-content"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor.</div>
                  </li>
                </ul>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default SectionFaq;
