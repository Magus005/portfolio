import React, { useState, useEffect } from 'react';

const ItemFaqTitle = props => {
  const [name, setName] = useState("")
  useEffect(() => {
    console.log(`--SECTION.FAQ.ITEMFAQTITLE-- props = ${JSON.stringify(props)}`);
    setName(props.name);
  }, [props]);
  return (
    <h3 className="togglebox-header">{name}</h3>
  );
}

export default ItemFaqTitle;
