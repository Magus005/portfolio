import React, { useEffect, useState } from 'react';


const ItemFaqContent = props => {
  const [content, setContent] = useState("")
  useEffect(() => {
    console.log(`--SECTION.FAQ.ITEMFAQCONTENT-- props = ${JSON.stringify(props)}`);
    setContent(props.content);
  }, [props]);
  return (
    <div className="togglebox-content">{content}</div>
  );
}

export default ItemFaqContent;
