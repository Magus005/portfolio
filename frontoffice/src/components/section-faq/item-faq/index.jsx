import React, { useState, useEffect } from 'react';
import ItemFaqTitle from './title';
import ItemFaqContent from './content';

const ItemFaq = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.FAQ.ITEMFAQ-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);
  return (
    <li>
      <ItemFaqTitle name={object.name} key={object._id} />
      <ItemFaqContent content={object.content} key={object._id} />
    </li>
  );
}

export default ItemFaq;
