import React from 'react';
import Education from './education/index';
import Experience from './experience';

const CareerPath = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <Experience />
          <Education />
        </div>
      </div>
    </div>
  );
}

export default CareerPath;
