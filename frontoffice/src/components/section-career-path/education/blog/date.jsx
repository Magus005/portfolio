import React, { useState, useEffect } from 'react';

const EducationDate = props => {
  const [startDate, setStartDate] = useState("current");
  const [endDate, setEndDate] = useState("current");
  useEffect(() => {
    if(props.startDate) {
      console.log(`--SECTION.CAREER.PATH.EDUCATION.DATE--  props = ${JSON.stringify(props)}`);
      setStartDate(props.startDate.split('-')[0]);
    }
    if(props.endDate) {
      setEndDate(props.endDate.split('-')[0]);
    }
  }, [props]);
  return (
    <span className="education-date">
      <span>{startDate} - {endDate}</span>
    </span>
  );
}

export default EducationDate;
