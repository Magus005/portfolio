import React, { useState, useEffect } from 'react';

const EducationTitle = props => {
  const [title, setTitle] = useState("");
  useEffect(() => {
    if(props.title) {
      console.log(`--SECTION.CAREER.PATH.EDUCATION.TITLE--  props = ${JSON.stringify(props)}`);
      setTitle(props.title);
    }
  }, [props]);
  return (
    <h3>
      <a target="_blank" rel="noopener noreferrer" href="https://themeforest.net/user/px-lab?ref=PX-lab">
        {title}
      </a>
    </h3>
  );
}

export default EducationTitle;
