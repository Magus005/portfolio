import React, { useState, useEffect } from 'react';

const EducationCompany = props => {
  const [schoolName, setSchoolName] = useState("");
  useEffect(() => {
    if(props.schoolName) {
      console.log(`--SECTION.CAREER.PATH.EDUCATION.COMPANY--  props = ${JSON.stringify(props)}`);
      setSchoolName(props.schoolName);
    }
  }, [props]);
  return (
    <a target="_blank" rel="noopener noreferrer" href="https://themeforest.net/user/px-lab?ref=PX-lab">
      <span className="education-company">{schoolName}</span>
    </a>
  );
}

export default EducationCompany;
