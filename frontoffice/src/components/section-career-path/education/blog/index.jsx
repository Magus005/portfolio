import React, { Fragment, useEffect, useState } from 'react';
import EducationDate from './date';
import EducationTitle from './title';
import EducationCompany from './company';
import AbovePost from '../../../shared/above-post';
import BelowPost from '../../../shared/below-post';

const BlogEducation = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.CAREER.PATH.BLOG.EDUCATION-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <Fragment>
      <EducationDate key={object._id} startDate={object.startDate} endDate={object.endDate} />
      <EducationTitle key={object._id} title={object.title} />
      <EducationCompany key={object._id} schoolName={object.schoolName} />
      <AbovePost />
      <BelowPost />
    </Fragment>
  );
}

export default BlogEducation;
