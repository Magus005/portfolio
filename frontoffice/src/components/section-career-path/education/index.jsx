import React, { Fragment } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import BlogEducation from './blog';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_EDUCATIONS = gql`query getAllEducationsQuery {
  getAllEducations {
    _id
    title
    schoolName
    startDate
    endDate
  }
}`;

const Education = props => {
  return (
    <ApolloProvider client={client}>
      <section id="timeline" className="section padd-box">
        <h2 className="title-lg text-upper">Education</h2>
        <div className="education">
          <div className="education-box">
            <Query query={GET_ALL_EDUCATIONS}>
              {
                ({loading, error, data}) => {
                  if(loading) return <h2>Loading ...</h2>
                  if(error) console.log(`--SECTION.ABOUT.GET_ABOUT_ACTIVE-- ${JSON.stringify(error)}`);
                  return <Fragment>
                    {
                      data.getAllEducations.map((education, index) => (
                        <BlogEducation key={index} value={education} />
                      ))
                    }
                  </Fragment>
                }
              }
            </Query>
          </div>
        </div>
      </section>
    </ApolloProvider>
  );
}

export default Education;
