import React, { useState, useEffect } from 'react';

const ExperienceProfileTitle = props => {
  const [title, setTitle] = useState("");
  useEffect(() => {
    if(props.title) {
      console.log(`--SECTION.CAREER.PATH.EXPERIENCE.PROFILE.TITLE--  props = ${JSON.stringify(props)}`);
      setTitle(props.title);
    }
  }, [props]);
  return (
    <h3>{title}</h3>
  );
}

export default ExperienceProfileTitle;
