import React, { useState, useEffect } from 'react';


const ExperienceContent = props => {
  const [content, setContent] = useState("");
  useEffect(() => {
    if(props.content) {
      console.log(`--SECTION.CAREER.PATH.EXPERIENCE.ENTERPRISE.NAME--  props = ${JSON.stringify(props)}`);
      setContent(props.content);
    }
  }, [props]);
  return (
    <p>{content}</p>
  );
}

export default ExperienceContent;
