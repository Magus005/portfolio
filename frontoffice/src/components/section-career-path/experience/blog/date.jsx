import React, { useState, useEffect } from 'react';

const ExperienceDate = props => {
  const [startDate, setStartDate] = useState("current");
  const [endDate, setEndDate] = useState("current");
  useEffect(() => {
    if(props.startDate) {
      console.log(`--SECTION.CAREER.PATH.EXPERIENCE.DATE--  props = ${JSON.stringify(props)}`);
      setStartDate(props.startDate.split('-')[0]);
    }
    if(props.endDate) {
      setEndDate(props.endDate.split('-')[0]);
    }
  }, [props]);
  return (
    <span className="education-date">
      <span>{startDate} - {endDate}</span> 
    </span>
  );
}

export default ExperienceDate;
