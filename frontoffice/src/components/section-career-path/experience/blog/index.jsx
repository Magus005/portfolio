import React, { useState, useEffect } from 'react';
import ExperienceDate from './date';
import ExperienceProfileTitle from './profile-title';
import ExperienceEnterpriseName from './enterprise-name';
import ExperienceEnterpriseLogo from './enterprise-logo';
import AbovePost from '../../../shared/above-post';
import ExperienceContent from './content';
import BelowPost from '../../../shared/below-post';


const BlogExperience = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.CAREER.PATH.BLOG.EDUCATION-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);
  return (
    <div className="education-box">
      <ExperienceDate key={object._id} startDate={object.startDate} endDate={object.endDate} />
      <ExperienceProfileTitle key={object._id} title={object.title} />
      <ExperienceEnterpriseLogo key={object._id} logo={object.logoEnterprise} />
      <ExperienceEnterpriseName key={object._id} name={object.nameEnterprise} />
      <AbovePost />
      <ExperienceContent key={object._id} content={object.content} />
      <BelowPost />
    </div>
  );
}

export default BlogExperience;
