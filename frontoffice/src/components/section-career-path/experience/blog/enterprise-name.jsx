import React, { useState, useEffect } from 'react';

const ExperienceEnterpriseName = props => {
  const [name, setName] = useState("");
  useEffect(() => {
    if(props.name) {
      console.log(`--SECTION.CAREER.PATH.EXPERIENCE.ENTERPRISE.NAME--  props = ${JSON.stringify(props)}`);
      setName(props.name);
    }
  }, [props]);
  return (
    <a target="_blank" rel="noopener noreferrer" href="https://themeforest.net/user/px-lab/portfolio">
      <span className="education-company">{name}</span>
    </a>
  );
}

export default ExperienceEnterpriseName;
