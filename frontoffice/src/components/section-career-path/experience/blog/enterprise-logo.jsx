import React, { useState, useEffect } from 'react';

const ExperienceEnterpriseLogo = props => {
  const [logo, setLogo] = useState("");
  useEffect(() => {
    if(props.logo) {
      console.log(`--SECTION.CAREER.PATH.EXPERIENCE.ENTERPRISE.LOGO--  props = ${JSON.stringify(props)}`);
      setLogo(props.logo);
    }
  }, [props]);
  return (
    <div className="education-logo">
      <img width="82" height="17" src={logo} className="crt-bw wp-post-image"
        alt=""
        srcset={logo + ' 82w,'+ logo + ' 80w,'}
        sizes="(max-width: 82px) 100vw, 82px" />
    </div>
  );
}

export default ExperienceEnterpriseLogo;
