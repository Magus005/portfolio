import React, { Fragment } from 'react';
import BlogExperience from './blog';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5010/api'
});

const GET_ALL_EXPERIENCES = gql`query getAllExperiencesQuery {
  getAllExperiences {
    _id
    title
    logoEnterprise
    nameEnterprise
    content
    startDate
    endDate
  }
}`;

const Experience = props => {
  return (
    <ApolloProvider client={client}>
      <section id="exp" className="section padd-box brd-btm">
        <h2 className="title-lg text-upper">work experience</h2>

        <div className="education">
          <Query query={GET_ALL_EXPERIENCES}>
              {
                ({loading, error, data}) => {
                  if(loading) return <h2>Loading ...</h2>
                  if(error) console.log(`--SECTION.ABOUT.GET_ABOUT_ACTIVE-- ${JSON.stringify(error)}`);
                  return <Fragment>
                    {
                      data.getAllExperiences.map((experience, index) => (
                        <BlogExperience key={index} value={experience}  />
                      ))
                    }
                  </Fragment>
                }
              }
          </Query>
        </div>
      </section>
    </ApolloProvider>
  );
}

export default Experience;
