import React from 'react';

const MenuSearchForm = props => {
  return (
    <section className="widget clearfix widget_search">
      <h2 className="widget-title">Search</h2>

      <form role="search" method="get" className="search-again" action="https://certy.px-lab.com/developer/">
        <div className="form-item-wrap">
          <input type="search" id="search-form-5cccc00fcb467" className="form-item" placeholder="Search" value=""
            name="s" />
        </div>

        <div className="form-submit form-item-wrap">
          <input type="submit" id="submit" className="btn btn-primary" value="Try Again" />
        </div>
      </form>
    </section>
  );
}


export default MenuSearchForm;
