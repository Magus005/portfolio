import React from 'react';
import ItemMenuNavigation from './item-navigation';

const MenuNavigation = props => {
  return (
    <nav id="crtMainNavSm" className="hidden-lg hidden-md text-center">
      <ul className="clear-list">
        <ItemMenuNavigation />
      </ul>
    </nav>
  );
}

export default MenuNavigation;
