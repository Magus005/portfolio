import React from 'react';

const ItemMenuNavigation = props => {
  return (
    <li className="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-90 current_page_item menu-item-148">
      <a href="index.html">Home</a>
    </li>
  );
}

export default ItemMenuNavigation;
