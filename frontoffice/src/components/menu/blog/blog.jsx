import React from 'react';
import ItemMenuBlog from './item-blog';

const MenuBlog = props => {
  return (
    <section className="widget clearfix widget_recent_entries">
      <h2 className="widget-title">From the Blog</h2>
      <ul>
        <ItemMenuBlog />
      </ul>
    </section>
  );
}

export default MenuBlog;
