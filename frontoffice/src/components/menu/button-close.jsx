import React from 'react';

const MenuButtonClose = props => {
  return (
    <button id="crtSidebarClose" className="btn btn-icon btn-light btn-shade">
      <span className="crt-icon crt-icon-close"></span>
    </button>
  );
}

export default MenuButtonClose;
