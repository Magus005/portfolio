import React from 'react';
import UserProfile from '../shared/user-profile';


const MenuProfile = props => {
  return (
    <section className="widget clearfix certy_widget_card">
      <div className="crt-card bg-primary text-center">
        <UserProfile />
      </div>
    </section>
  );
}

export default MenuProfile;
