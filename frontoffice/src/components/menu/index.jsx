import React from 'react';
import MenuButtonClose from './button-close';
import MenuNavigation from './navigation/navigation';
import MenuProfile from './profile';
import MenuSearchForm from './search-form';
import Tag from '../tag/index';
import MenuBlog from './blog/blog';


const Menu = props => {
  return (
    <div id="crtSidebar">
      <MenuButtonClose />
      <div id="crtSidebarInner">
        <MenuNavigation />
        <aside className="widget-area clear-mrg">
          <MenuProfile />
          <MenuSearchForm />
          <Tag />
          <MenuBlog />
        </aside>
      </div>
    </div>  
  );
}

export default Menu;
