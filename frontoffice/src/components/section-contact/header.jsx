import React, { lazy, Suspense } from 'react';
const ListSocialNetwork = lazy(() => import('../shared/social-network'));


const SectionContactHeader = props => {
  return (
    <header className="contact-head">
      <Suspense fallback={<h2>Loading ...</h2>}>
        <ListSocialNetwork />
      </Suspense>
    </header>
  );
}

export default SectionContactHeader;
