import React from 'react';
import TextereaForm from './texterea-form';
import ButtonSubmitForm from './button-submit-form';
import InputForm from './input-form';

const SectionContactForm = props => {
  return (
    <div role="form" className="wpcf7" id="wpcf7-f46-p90-o1" lang="en-US" dir="ltr">
      <div className="screen-reader-response"></div>

      <form action="https://certy.px-lab.com/developer/#wpcf7-f46-p90-o1" method="post" className="wpcf7-form"
        noValidate="novalidate">
        <div style={{display: 'none'}}>
          <input type="hidden" name="_wpcf7" value="46" />
          <input type="hidden" name="_wpcf7_version" value="5.0.3" /> <input type="hidden"
            name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag"
            value="wpcf7-f46-p90-o1" />
          <input type="hidden" name="_wpcf7_container_post" value="90" />
        </div>
        <InputForm />
        <InputForm />
        <InputForm />
        <TextereaForm />
        <ButtonSubmitForm />

        <div className="wpcf7-response-output wpcf7-display-none"></div>
      </form>
    </div>
  );
}

export default SectionContactForm;
