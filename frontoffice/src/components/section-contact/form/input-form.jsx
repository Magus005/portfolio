import React from 'react';


const InputForm = props => {
  return (
    <p>
      <label> Your Email (required)<br />
        <span className="wpcf7-form-control-wrap your-email">
          <input type="email" name="your-email" value="" size="40"
            className="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
            aria-required="true" aria-invalid="false" />
        </span>
      </label>
    </p>
  );
}

export default InputForm;
