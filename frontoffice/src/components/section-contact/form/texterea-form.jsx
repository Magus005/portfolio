import React from 'react';

const TextereaForm = props => {
  return (
    <p>
      <label> Your Message<br />
        <span className="wpcf7-form-control-wrap your-message">
          <textarea name="your-message" cols="40" rows="10" className="wpcf7-form-control wpcf7-textarea"
            aria-invalid="false"></textarea>
        </span>
      </label>
    </p>
  );
}

export default TextereaForm;
