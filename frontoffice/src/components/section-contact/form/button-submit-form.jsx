import React from 'react';

const ButtonSubmitForm = props => {
  return (
    <p>
      <input type="submit" value="Send" className="wpcf7-form-control wpcf7-submit" />
    </p>
  );
}

export default ButtonSubmitForm;
