import React, { useState, useEffect } from 'react';
import Axios from 'axios';

const SectionContactMap = props => {
  const [location, setLocation] = useState({})
  useEffect(() => {
    async function fetchDataMe() {
      let result = await Axios.get('http://127.0.0.1:8080/location/');
      console.log(`--SECTION.CONTACT.MAP-- location ${JSON.stringify(result.data)}`);
      setLocation(result.data[0]);
    }
    fetchDataMe();
  }, []);

  return (
    <div id="map" data-latitude={location.latitude} data-longitude={location.longitude}></div>
  );
}

export default SectionContactMap;
