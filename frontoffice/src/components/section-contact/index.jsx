import React from 'react';
import SectionContactHeader from './header';
import SectionContactDescription from './description';
import SectionContactForm from './form/form';
import SectionContactMap from './map';


const SectionContact = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="contacts" className="section padd-box">
            <h2 className="title-lg text-upper">Contact Me</h2>
            <div className="padd-box-sm">
              <SectionContactHeader />
              <SectionContactDescription />
              <SectionContactForm />
            </div>
            <SectionContactMap />
          </section>
        </div>
      </div>
    </div>
  );
}

export default SectionContact;
