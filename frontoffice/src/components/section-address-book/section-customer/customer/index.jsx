import React from 'react';
import ItemCustomer from './item-customer';

const Customer = props => {
  return (
    <div className="padd-box-sm">
      <ul className="clients clear-list">
        <ItemCustomer />
        <ItemCustomer />
        <ItemCustomer />
        <ItemCustomer />
      </ul>
    </div>
  );
}


export default Customer;
