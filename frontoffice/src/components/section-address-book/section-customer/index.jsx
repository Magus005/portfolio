import React from 'react';
import Customer from './customer';


const SectionCustomer = props => {
  return (
    <section id="clients" className="section clear-mrg padd-box brd-btm">
      <h2 className="title-lg text-upper">Clients</h2>
      <Customer />
    </section>
  );
}


export default SectionCustomer;
