import React from 'react';
import SectionReference from './section-reference';
import SectionCustomer from './section-customer';

const SectionAddressBook = props => {
  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <SectionCustomer />
          <SectionReference />
        </div>
      </div>
    </div>
  );
}

export default SectionAddressBook;
