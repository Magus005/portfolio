import React from 'react';
import ReferenceAvatar from './avatar';
import ReferenceAuthor from './author';
import ReferenceContent from './content';
import AbovePost from '../../../shared/above-post';
import BelowPost from '../../../shared/below-post';

const Reference = props => {
  return (
    <div className="ref-box brd-btm hreview">
      <ReferenceAvatar />
      <div className="ref-info">
        <ReferenceAuthor />
        <blockquote className="ref-cont clear-mrg">
          <AbovePost />
          <ReferenceContent />
          <BelowPost />
        </blockquote>
      </div>
    </div>
  );
}

export default Reference;
