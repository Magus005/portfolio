import React from 'react';

const ReferenceAuthor = props => {
  return (
    <div className="ref-author">
      <strong>edwin ballard</strong>
      <span>West Alexandrine</span>
    </div>
  );
}


export default ReferenceAuthor;
