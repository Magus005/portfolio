import React from 'react';

const ReferenceAvatar = props => {
  return (
    <div className="ref-avatar">
      <img alt="" src="resources/images/tourguide-54x54.jpg"
        srcset="resources/images/tourguide-108x108 2x"
        className="avatar avatar-54 photo crt-bw" />
    </div>
  );
}

export default ReferenceAvatar;
