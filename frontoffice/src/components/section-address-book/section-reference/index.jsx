import React from 'react';
import Reference from './reference';


const SectionReference = props => {
  return (
    <section id="ref" className="section padd-box">
      <h2 className="title-lg text-upper">References</h2>

      <div className="ref-box-list padd-box-sm clear-mrg">
        <Reference />
      </div>
    </section>
  );
}

export default SectionReference;
