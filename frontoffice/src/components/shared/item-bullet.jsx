import React, { Fragment } from 'react';

const ItemBullet = props => {
  return (
    <Fragment>
      <span className="bullet fill"></span>
    </Fragment>
  );
}

export default ItemBullet;
