import React from 'react';

const TitleBlock = props => {
  return (
    <h2 className="title-lg text-upper">Blog</h2>
  );
}

export default TitleBlock;
