import React, { useState, useEffect } from 'react';

const UserProfileAvatar = props => {
  const [userAvatar, setUserAvatar] = useState({});

  useEffect(() => {
    if(props) {
      console.log(`--UserProfileAvatar.Props-- ${JSON.stringify(props.userAvatar)}`);
      setUserAvatar(props.userAvatar);
      console.log(`--UserProfileAvatar.Props-- userInfo ${JSON.stringify(userAvatar)}`);
    }
  }, [props, userAvatar]);

  return (
    <div className="crt-card-avatar">
      <span className="crt-avatar-state">
        <span className="crt-avatar-state1">
          <img className="avatar avatar-195" alt="" src={userAvatar.picture}
            srcset={userAvatar.picture + ' 2x'} />
        </span>

        <span className="crt-avatar-state2">
          <img className="avatar avatar-195" alt="" src="resources/images/certy-programmer-2-195x195.png"
            srcset="resources/images/magus-programmer-2.png 2x" />
        </span>
      </span>
    </div>
  );
}

export default UserProfileAvatar;
