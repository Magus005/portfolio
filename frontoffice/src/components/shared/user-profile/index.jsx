import React, { Fragment, useState, useEffect } from 'react';
import UserProfileAvatar from './avatar';
import UserProfileInfo from './info';
import Axios from 'axios';

const UserProfile = props => {

  const [me, setMe] = useState([{}])
  useEffect(() => {
    async function fetchDataMe() {
      let result = await Axios.get('http://127.0.0.1:8080/me/');
      console.log(`--UserProfileInfo-Component-- me ${JSON.stringify(result.data)}`);
      setMe(result.data[0]);
    }
    fetchDataMe();
  }, []);

  return (
    <Fragment>
      <UserProfileAvatar userAvatar={me} />
      <UserProfileInfo userInfo={me} />
    </Fragment>
  );
}

export default UserProfile;
