import React, { useState, useEffect, lazy, Suspense } from 'react';
const ListSocialNetwork = lazy(() => import('../social-network'));

const UserProfileInfo = props => {

  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    if(props) {
      console.log(`--UserProfileInfo.Props-- ${JSON.stringify(props.userInfo)}`);
      setUserInfo(props.userInfo);
      console.log(`--UserProfileInfo.Props-- userInfo ${JSON.stringify(userInfo)}`);
    }
  }, [props, userInfo]);

  return (
    <div className="crt-card-info clear-mrg">
      <h2 className="text-upper">{userInfo.name}</h2>
      <p className="text-muted">{userInfo.title}</p>
      <Suspense fallback={<h2>Loading ...</h2>}>
        <ListSocialNetwork />
      </Suspense>
    </div>
  );
}

export default UserProfileInfo;
