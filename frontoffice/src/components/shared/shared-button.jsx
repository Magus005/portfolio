import React from 'react';

const SharedButton = props => {
  return (
    <button className="share-btn btn btn-bordered btn-upper">
      <span className="crt-icon crt-icon-share"></span>
      Share
    </button>
  );
}

export default SharedButton;
