import React, { useState, useEffect } from 'react';

const ItemProgressBar = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--ITEM.PROGRESS.BAR-- props = ${JSON.stringify(props)}`);
    setObject(props);
  }, [props]);

  return (
    <div className="progress-line crt-animate" role="progressbar" aria-valuenow="76" aria-valuemin="0"
      aria-valuemax="100">
      <strong className="progress-title">{object.name}</strong>
      <div className="progress-bar" data-text="76%" data-value="0.76"></div>
    </div>
  );
}

export default ItemProgressBar;
