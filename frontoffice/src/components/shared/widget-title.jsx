import React, { Fragment } from 'react';

const WidgetTitle = props => {
  return (
    <Fragment>
      <h2 className="widget-title">Tags</h2>
    </Fragment>
  );
}

export default WidgetTitle;
