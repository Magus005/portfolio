import React, { useState, useEffect } from 'react';
import ItemUserProfileSocialNetwork from './item-social-network';
import Axios from 'axios';

const ListSocialNetwork = props => {

  const [socialNetwork, setSocialNetwork] = useState([{}]);

  useEffect(() => {
    async function fetchDateSocialNetwork() {
      let result = await Axios.get('http://127.0.0.1:5020/socialnetwork/');
      console.log(`--UserProfileInfo-Component-- social network ${JSON.stringify(result.data)}`);
      setSocialNetwork(result.data);
    }
    fetchDateSocialNetwork();
  }, [])

  return (
    <ul className="crt-social clear-list">
    {
      socialNetwork.map(item => (
        <ItemUserProfileSocialNetwork value={item} />
      ))
    }
    </ul>
  );
}

export default ListSocialNetwork;
