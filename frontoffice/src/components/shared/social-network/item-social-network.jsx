import React, { useState, useEffect } from 'react';


const ItemUserProfileSocialNetwork = props => {
  
  const [socialNetwork, setSocialNetwork] = useState({});

  useEffect(() => {
    if(props.value) {
      console.log(`--ItemUserProfileSocialNetwork.PROPS-- ${JSON.stringify(props.value)}`);
      console.log(`--ItemUserProfileSocialNetwork.PROPS-- ${props.value.uri}`);
      setSocialNetwork(props.value)
    }
  }, [props.value])
  return (
    <li>
      <a href={socialNetwork.uri} target="_blank" rel="noopener noreferrer" >
        <span className="crt-icon crt-icon-facebook"></span>
      </a>
    </li>
  );
}

export default ItemUserProfileSocialNetwork;
