import React, { Fragment } from 'react';

const BackgroundShape = props => {
  return (
    <Fragment>
      <svg id="crtBgShape1" className="hidden-sm hidden-xs" height="519" width="758">
        <polygon className="pol" points="0,455,693,352,173,0,92,0,0,71" />
      </svg>
  
      <svg id="crtBgShape2" className="hidden-sm hidden-xs" height="536" width="633">
        <polygon points="0,0,633,0,633,536" />
      </svg>
    </Fragment>
  );
}

export default BackgroundShape;
