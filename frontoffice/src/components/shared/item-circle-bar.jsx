import React, { useEffect, useState } from 'react';


const ItemCircleBar = props => {

  const [object, setObject] = useState({});

  useEffect(() => {
    if(props.value) {
      console.log(`--ITEM.CIRCLE.BAR-- props = ${JSON.stringify(props)}`);
      setObject(props.value)
    }
  }, [props, props.value])

  return (
    <div className="col-xs-4 text-center">
      <div className="progress-chart crt-animate" role="progressbar" aria-valuenow={object.level + '0'} aria-valuemin="0"
        aria-valuemax="100">
        <div className="progress-bar" data-text={object.level + '0%'} data-value={'0.' + object.level}></div>
        <strong className="progress-title">{object.name}</strong>
      </div>
    </div>
  );
}

export default ItemCircleBar;
