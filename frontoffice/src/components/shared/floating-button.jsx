import React from 'react';

const FloatingButton = props => {
  return (
    <button id="crtBtnUp" className="btn btn-icon btn-primary">
      <span className="crt-icon crt-icon-arrow-page-up"></span>
    </button>
  );
}

export default FloatingButton;
