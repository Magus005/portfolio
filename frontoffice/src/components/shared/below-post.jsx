import React from 'react';

const BelowPost = props => {
  return (
    <div className="at-below-post-homepage addthis_tool"
      data-url="https://certy.px-lab.com/developer/certy_timeline/design-university/"></div>
  );
}

export default BelowPost;
