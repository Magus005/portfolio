import React from 'react';

const AbovePost = props => {
  return (
    <div className="at-above-post-homepage addthis_tool"
      data-url="https://certy.px-lab.com/developer/certy_timeline/design-university/"></div>
  );
}

export default AbovePost;
