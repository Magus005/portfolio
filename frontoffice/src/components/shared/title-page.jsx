import React from 'react';

const TitlePage = props => {
  return (
    <header className="page-header padd-box">
      <h1 className="title-lg text-upper">Blog</h1>
    </header>
  );
}

export default TitlePage;
