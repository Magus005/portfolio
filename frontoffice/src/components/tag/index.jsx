import React from 'react';
import ItemTag from './item-tag';
import WidgetTitle from '../shared/widget-title';

const Tag = props => {
  return (
    <section className="widget clearfix widget_tag_cloud">
      <WidgetTitle />
      <div className="tagcloud">
        <ItemTag />
      </div>
    </section>
  );
}

export default Tag;
