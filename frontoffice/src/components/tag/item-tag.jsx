import React, { Fragment} from 'react';

const ItemTag = props => {
  return (
    <Fragment>
        <a href="tag/api/index.html" className="tag-cloud-link tag-link-4 tag-link-position-1"
          style={{fontSize: 16.4}} aria-label="API (2 items)">API</a>
    </Fragment>
  );
}

export default ItemTag;
