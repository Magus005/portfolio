import React, { useState, useEffect } from 'react';
import PictureArticle from './picture';
import HeaderArticle from './header';
import FooterArticle from './footer';


const ItemArticle = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--BLOG.ITEM.ARTICLE-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <article id="post-86" className="post-86 post type-post status-publish format-video has-post-thumbnail hentry category-category-2 tag-api tag-certy tag-google tag-key post_format-post-format-video">
      <PictureArticle value={object} />
      <div className="padd-box-sm">
      <HeaderArticle value={object} />
          <div className="post-content entry-content editor clearfix clear-mrg">
            <p></p>
          </div>
        <FooterArticle value={object} />
      </div>
    </article>
  );
}

export default ItemArticle;
