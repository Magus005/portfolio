import React, { useState, useEffect } from 'react';

const ButtonReadMoreArticle = props => {
  const [object, setObject] = useState({})
  useEffect(() => {
    console.log(`--BLOG.ITEM.LIST.ARTICLE-- ButtonReadMoreArticle = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <div className="post-more">
      <a className="btn btn-sm btn-primary" href={"/detail-blog/"+object._id} rel="bookmark">
        Read More
      </a>
    </div>
  );
}

export default ButtonReadMoreArticle;
