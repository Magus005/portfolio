import React, { useState, useEffect } from 'react';
import DateArticle from './date';
import AuthorArticle from './author';


const HeaderArticle = props => {
  const [object, setObject] = useState([{}])
  const [date, setDate] = useState([{}])
  useEffect(() => {
    console.log(`--BLOG.ITEM.LIST.ARTICLE-- props = ${JSON.stringify(props)}`);
    setDate(props.value.dateCreated);
    setObject(props.value);
  }, [props]);

  return (
    <header className="post-header text-center">
      <div className="post-header-info">
        <span className="posted-on">
          <span className="screen-reader-text">Posted on</span>
          <DateArticle date={date} />
        </span> by 
        <AuthorArticle value={object} />
      </div>
    </header>
  );
}

export default HeaderArticle;
