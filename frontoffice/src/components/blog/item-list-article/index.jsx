import React, { useState, useEffect } from 'react';
import ItemArticle from './item-article'

const ItemListArticle = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--BLOG.ITEM.LIST.ARTICLE-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <ItemArticle value={object} />
        </div>
      </div>
    </div>
  );
}

export default ItemListArticle;
