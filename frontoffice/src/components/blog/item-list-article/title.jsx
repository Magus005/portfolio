import React from 'react';

const TitleArticle = props => {
  return (
    <h2 className="post-title entry-title text-upper">
      <a rel="bookmark" href="../2017/06/20/how-to-create-google-font-developer-api-key/index.html">
        How to Create Google Font Developer API Key
      </a>
    </h2>
  );
}

export default TitleArticle;
