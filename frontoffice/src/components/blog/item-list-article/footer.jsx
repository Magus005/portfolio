import React, { useState, useEffect } from 'react';
import CategoryArticle from './category';
//import CommentArticle from './comment';
import ButtonReadMoreArticle from './button-read-more';


const FooterArticle = props => {
  const [object, setObject] = useState({})
  useEffect(() => {
    console.log(`--BLOG.ITEM.DATE.ARTICLE-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <footer className="post-footer">
      <div className="post-footer-top brd-btm clearfix">
        <div className="post-footer-info">
          <CategoryArticle value={1} />
          {/**
          <span className="post-line">|</span>
          <CommentArticle />
           */}
        </div>
        <ButtonReadMoreArticle value={object} />
      </div> 
    </footer>
  );
}

export default FooterArticle;
