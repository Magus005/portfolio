import React, { useState, useEffect } from 'react';

const DateArticle = props => {
  const [date, setDate] = useState("")
  useEffect(() => {
    console.log(`--BLOG.ITEM.DATE.ARTICLE-- props = ${JSON.stringify(props)}`);
    setDate(""+props.date);
  }, [props]);
  return (
    <a href="../2017/06/20/how-to-create-google-font-developer-api-key/index.html" rel="bookmark">
      <time className="post-date published updated" datetime="2017-06-20T12:04:18+00:00">
        {date}
      </time>
    </a>
  );
}

export default DateArticle;
