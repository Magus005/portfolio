import React, { useState, useEffect } from 'react';

const CategoryArticle = props => {
  const [value, setValue] = useState("")
  useEffect(() => {
    console.log(`--BLOG.ITEM.CATEGORY.ARTICLE-- props = ${JSON.stringify(props)}`);
    setValue(props.value);
  }, [props]);

  return (
    <span className="post-cat-links">
      <span className="screen-reader-text">Categories</span>
      <a href>Category {value}</a>
    </span>
  );
}


export default CategoryArticle;
