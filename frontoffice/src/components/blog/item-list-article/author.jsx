import React from 'react';

const AuthorArticle = props => {
  return (
    <span className="post-author vcard">
      <a href="../author/admin/index.html" rel="author">admin</a>
    </span>
  );
}

export default AuthorArticle;
