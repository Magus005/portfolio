import React, { useEffect, useState } from 'react';

const PictureArticle = props => {
  const [value, setValue] = useState({})
  useEffect(() => {
    console.log(`--BLOG.PICTURE.ARTICLE-- props = ${JSON.stringify(props)}`);
    setValue(props.value);
  }, [props]);

  return (
    <div className="post-media">
      <a href={value.picture} rel="bookmark"> 
        <figure className="post-figure">
          <img width="768" height="289" src={value.picture} className="attachment-certy-blog size-certy-blog wp-post-image" alt={value.title} />
        </figure>
        
        <span className="post-play">
          <i className="crt-icon crt-icon-play"></i>
        </span>
      </a>
      
    </div>
  );
}

export default PictureArticle;
