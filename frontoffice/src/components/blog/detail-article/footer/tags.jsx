import React from 'react';

const Tags = props => {
  return (
    <div className="post-footer-btm">
      <div className="post-tags">
        <span className="screen-reader-text">Tags </span>
        
        <a href="../../../../tag/api/index.html" rel="tag">API</a>
        <a href="../../../../tag/certy/index.html" rel="tag">Certy</a>

        <a href="../../../../tag/google/index.html" rel="tag">
          Google
        </a>
        
        <a href="../../../../tag/key/index.html" rel="tag">
          KEY
        </a>
      </div>
    </div>
  );
}

export default Tags;
