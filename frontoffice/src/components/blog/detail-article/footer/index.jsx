import React from 'react';
import Categories from './categories';
import Tags from './tags';

const FooterArticle = props => {
  return (
    <footer className="post-footer">
      <Categories />
      <Tags />
    </footer>
  );
}

export default FooterArticle;
