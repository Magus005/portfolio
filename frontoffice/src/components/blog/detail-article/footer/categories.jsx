import React from 'react';

const Categories = props => {

  return (
    <div className="post-footer-top brd-btm clearfix">
      <div className="post-footer-info">
        <span className="post-cat-links">
          <span className="screen-reader-text">Categories</span>
          
          <a href="../../../../category/category-2/index.html">
            Category 2
          </a>
          
        </span> 
          
        <span className="post-line">|</span>
        
        <a href="index.html#respond" className="post-comments-count">
          0 comments
        </a>
      
      </div>
    </div>
  );
}

export default Categories;
