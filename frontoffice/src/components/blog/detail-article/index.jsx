import React from 'react';
import HeaderArticle from './header';
import ContentArticle from './content';
import FooterArticle from './footer';
import MediaArticle from './media';

const DetailArticle = props => {
  return (
    <article id="post-84" className="post-84 post type-post status-publish format-video has-post-thumbnail hentry category-category-2 tag-api tag-certy tag-google tag-key post_format-post-format-video">
      <MediaArticle />
      <div className="padd-box-sm">
        <HeaderArticle />
        <ContentArticle />
        <FooterArticle />
      </div>
    </article>
  );
}

export default DetailArticle;
