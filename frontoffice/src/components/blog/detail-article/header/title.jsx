import React from 'react';


const Title = props => {
  return (
    <h2 className="post-title entry-title text-upper">
      <a rel="bookmark" href="index.html">
        How to Create Google Maps Developer API Key
      </a>
    </h2>
  );
}

export default Title;
