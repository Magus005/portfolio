import React from 'react';
import Title from './title';
import DateArticle from '../../item-list-article/date';
import AuthorArticle from '../../item-list-article/author';

const HeaderArticle = props => {
  return (   
    <header className="post-header text-center">
      <Title />
      <div className="post-header-info">
        <span className="posted-on">
          <span className="screen-reader-text">Posted on</span>
          <DateArticle />
        </span> by 
        <AuthorArticle />
      </div>
    </header>
  );
}

export default HeaderArticle;
