import React from 'react';
import Iframe from 'react-iframe'

const MediaArticle = props => {
  return (              
    <div className="post-media">
      <div className="post-video">

        <Iframe url="https://www.youtube.com/embed/GHQs25w16uo?feature=oembed"
          width="640px"
          height="360px"
          frameborder="0"
          id="myId"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
          className="myClassname"
          display="initial"
          position="relative"/>
      </div>
    </div>
  );
}

export default MediaArticle;
