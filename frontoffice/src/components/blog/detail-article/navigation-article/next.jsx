import React from 'react';
import ItemNavigationArticle from './item-navigation-article';


const NextArticle = props => {
  return (
    <div className="col-sm-5 col-sm-offset-2 col-xs-6">
      <div className="post-nav-next">
        <ItemNavigationArticle />
      </div>
    </div>
  );
}

export default NextArticle;
