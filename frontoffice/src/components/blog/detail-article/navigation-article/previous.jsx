import React from 'react';
import ItemNavigationArticle from './item-navigation-article';

const PreviousArticle = props => {
  return (
    <div className="col-sm-5 col-xs-6">
      <div className="post-nav-prev">
        <ItemNavigationArticle />
      </div>
    </div>
  );
}

export default PreviousArticle;
