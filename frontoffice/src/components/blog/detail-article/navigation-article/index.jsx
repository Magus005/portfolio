import React from 'react';
import PreviousArticle from './previous';
import NextArticle from './next';


const NavigationArticle = props => {
  return (
    <nav className="post-nav" role="navigation">
      <div className="padd-box-sm brd-btm">
        <h2 className="screen-reader-text">Post navigation</h2>
          <PreviousArticle />
          <NextArticle />
        <div className="row">
        </div>
      </div>
    </nav>
  );
}

export default NavigationArticle;
