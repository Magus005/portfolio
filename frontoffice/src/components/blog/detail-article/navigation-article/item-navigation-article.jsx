import React from 'react';

const ItemNavigationArticle = props => {
  return (
    <a href="../how-to-create-online-cv-and-resume/index.html" rel="prev">
      <span className="text-left text-muted">
        previous article
      </span>
      
      
      <figure>
        <img src="../../../../wp-content/themes/certy/assets/images/demo-image-default.jpg" alt="Previous" />
      </figure>
      
      <strong className="text-upper text-center">
        How to Create Online CV and Resume
      </strong>
      
    </a>
  );
}

export default ItemNavigationArticle;
