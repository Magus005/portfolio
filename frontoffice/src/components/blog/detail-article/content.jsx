import React from 'react';


const ContentArticle = props => {
  return (
    <div className="post-content entry-content editor clearfix clear-mrg">
      <div className="at-above-post addthis_tool" data-url="https://certy.px-lab.com/developer/2017/06/20/how-to-create-google-font-api-key/"></div>
      
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem eaque maxime illum commodi ab quisquam veniam, nihil nobis dolore impedit delectus similique a, perspiciatis error ex tenetur aspernatur quidem alias?</p>
      
      <div className="at-below-post addthis_tool" data-url="https://certy.px-lab.com/developer/2017/06/20/how-to-create-google-font-api-key/"></div>
    
    </div>
  );
}

export default ContentArticle;
