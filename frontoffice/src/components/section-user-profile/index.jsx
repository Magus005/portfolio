import React from 'react';
import UserProfileHeader from './header';
import UserProfileFooter from './footer';


const SectionUserProfile = props => {
  return (
    <div id="crtSideBoxWrap">
      <div id="crtSideBox" className="clear-mrg">
        <div className="crt-side-box-1 clear-mrg">
          <div id="wdg-sticky-start"></div>
          <UserProfileHeader />
          <UserProfileFooter />
        </div>
      </div>
    </div>
  );
}

export default SectionUserProfile;
