import React from 'react';
import UserProfile from '../shared/user-profile';

const UserProfileHeader = props => {
  return (
    <aside className="widget clearfix certy_widget_card">
      <div className="crt-card bg-primary text-center">
        <UserProfile />
      </div>
    </aside>
  );
}

export default UserProfileHeader;
