import React from 'react';
import ButtonDownload from './button-download';

const UserProfileFooter = props => {
  return (
    <aside className="widget clearfix widget_text">
      <ButtonDownload />
    </aside>
  );
}

export default UserProfileFooter;
