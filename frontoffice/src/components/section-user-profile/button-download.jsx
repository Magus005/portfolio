import React, { useEffect, useState } from 'react';
import axios from 'axios';

const ButtonDownload = props => {

  const [resume, setResume] = useState({})

  useEffect(() => {
    async function fetchData() {
      const result = await axios.get('http://127.0.0.1:8080/cv/');
      console.log(`--ButtonDownload-Component-- ${result.data}`);
      setResume(result.data[0]);
    }
    fetchData();
  }, []);

  return (
    <div className="textwidget">
      <p>
        <center>
          <a className="btn btn-lg btn-default" target="_blank" rel="noopener noreferrer"
            href={ resume.path }>DOWNLOAD
            CV 
          </a></center>
      </p>
    </div>
  );
}

export default ButtonDownload;
