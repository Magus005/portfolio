import React, { useEffect, useState } from 'react';
import PortfolioFilter from './filter/filter';
import PortfolioProject from './project/index';
import Axios from 'axios';

const SectionPortfolio = props => {
  const [allPortfolio, setAllPortfolio] = useState([{}]);

  useEffect(() => {
    async function fetchDataPortfolio() {
      let result = await Axios.get('http://127.0.0.1:8080/portfolio/');
      console.log(`--SECTION.PORTFOLIO-- portfolio ${JSON.stringify(result)}`);
      setAllPortfolio(result.data);
    }
    fetchDataPortfolio();
  }, []);

  return (
    <div className="crt-paper-layers crt-animate">
      <div className="crt-paper clear-mrg clearfix">
        <div className="crt-paper-cont paper-padd clear-mrg">
          <section id="portfolio" className="section padd-box">
            <h2 className="title-lg text-upper">Portfolio</h2>

            <div className="pf-wrap">
              <PortfolioFilter />

              <div className="pf-grid">
                <div className="pf-grid-sizer"></div>
                {
                  allPortfolio.map((portfolio, index) => (
                    <PortfolioProject key={index} value={portfolio} />
                  ))
                }
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default SectionPortfolio;
