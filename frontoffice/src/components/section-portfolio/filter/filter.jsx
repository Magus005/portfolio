import React from 'react';
import ItemPortfolioFilter from './item-filter';

const PortfolioFilter = props => {
  return (
    <div className="pf-filter padd-box">
      <ItemPortfolioFilter />
    </div>
  );
}

export default PortfolioFilter;
