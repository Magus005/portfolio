import React, { useState, useEffect } from 'react';

const ImageProject = props => {
  const [picture, setPicture] = useState("");
  useEffect(() => {
    if(props.picture) {
      console.log(`--SECTION.PORTFOLIO.IMAGE.PROJECT--  props = ${JSON.stringify(props)}`);
      setPicture(props.picture);
    }
  }, [props]);
  return (
    <figure className="pf-figure">
      <img width="380" height="253" src={picture}
        className="attachment-certy-portfolio size-certy-portfolio wp-post-image" alt="Certy Work"
        srcset={picture + ' 380w, '+ picture + ' 300w, '+ picture + ' 768w, '+  picture + ' 1024w, '+ picture + ' 120w' }
        sizes="(max-width: 380px) 100vw, 380px" />
    </figure>
  );
}

export default ImageProject;
