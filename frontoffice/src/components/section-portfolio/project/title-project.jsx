import React, { useState, useEffect } from 'react';

const TitleProject = props => {
  const [title, setTitle] = useState("");
  useEffect(() => {
    if(props.title) {
      console.log(`--SECTION.PORTFOLIO.IMAGE.PROJECT--  props = ${JSON.stringify(props)}`);
      setTitle(props.picture);
    }
  }, [props]);
  return (
    <h2 className="pf-title text-upper">{title}</h2>
  );
}

export default TitleProject;
