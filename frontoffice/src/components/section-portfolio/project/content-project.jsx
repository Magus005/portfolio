import React, { useState, useEffect } from 'react';
import AbovePost from '../../shared/above-post';
import BelowPost from '../../shared/below-post';

const ContentProject = props => {
  const [content, setContent] = useState("");
  useEffect(() => {
    if(props.content) {
      console.log(`--SECTION.PORTFOLIO.CONTENT.PROJECT--  props = ${JSON.stringify(props)}`);
      setContent(props.content);
    }
  }, [props]);
  return (
    <div className="pf-text clear-mrg">
      <AbovePost />

      <p>{content}</p>

      <BelowPost />

      <p></p>
    </div>
  );
}

export default ContentProject;
