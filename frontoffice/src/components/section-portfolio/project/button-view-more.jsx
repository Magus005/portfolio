import React from 'react';

const ButtonViewMore = props => {
  return (
    <span className="pf-btn btn btn-primary">View More</span>
  );
}

export default ButtonViewMore;
