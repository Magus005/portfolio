import React, { useState, useEffect } from 'react';
import ImageProject from './image-project';
import TitleProject from './title-project';
import ContentProject from './content-project';
import ButtonViewMore from './button-view-more';
import PortfolioPopupProject from '../popup-project/index';


const PortfolioProject = props => {
  const [object, setObject] = useState([{}]);
  useEffect(() => {
    console.log(`--SECTION.PORTFOLIO.PROJECT-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <article className="pf-grid-item pf-work">
      <a className="pf-project" href={'#pf-popup-' + object.id}>

        <ImageProject key={object.id} picture={object.picture} />

        <div className="pf-caption text-center">
          <div className="valign-table">
            <div className="valign-cell">
              <TitleProject key={object.id} content={object.title} />

              <ContentProject key={object.id} content={object.content} />

              <ButtonViewMore />
            </div>
          </div>
        </div>
      </a>

      <PortfolioPopupProject key={object.id} value={object} />
    </article>
  );
}

export default PortfolioProject;
