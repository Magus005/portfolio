import React from 'react';

const Title = props => {
  return (
    <h2 className="pf-rel-title text-upper">More Projects</h2>
  );
}

export default Title;
