import React from 'react';
import Title from './title';
import Picture from './picture';
import ButtonView from './button-view';


const MoreProject = props => {
  return (
    <div className="pf-popup-rel">
      <Title />
      <div className="pf-rel-carousel cr-carousel" data-init="none">
        <div className="pf-rel-project">
          <a className="pf-rel-href" href="#pf-popup-76">
            <Picture />

            <ButtonView />
          </a>
        </div>
      </div>
    </div>
  );
}

export default MoreProject;
