import React, { Fragment } from 'react';

const ButtonView = props => {
  return (
    <Fragment>
      <span className="pf-rel-cover">
        <span className="btn btn-primary btn-sm">View</span>
      </span>
    </Fragment>
  );
}

export default ButtonView;
