import React, { Fragment } from 'react';

const Picture = props => {
  return (
    <Fragment>
      <img width="123" height="92"
        src="wp-content/uploads/sites/6/2017/06/certy-work-123x92.jpg"
        className="attachment-certy-portfolio-popup-related size-certy-portfolio-popup-related wp-post-image"
        alt="Certy Work"
        srcset="resources/images/magus-work-123x92.jpg 123w, resources/images/magus-work-80x60.jpg 80w"
        sizes="(max-width: 123px) 100vw, 123px" />
    </Fragment>
  );
}

export default Picture;
