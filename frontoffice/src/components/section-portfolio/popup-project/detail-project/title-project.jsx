import React, { Fragment, useState, useEffect } from 'react';

const TitleProject = props => {
  const [title, setTitle] = useState("");
  useEffect(() => {
    if(props.title) {
      console.log(`--SECTION.PORTFOLIO.POPUP.TITLE.PROJECT--  props = ${JSON.stringify(props)}`);
      setTitle(props.title);
    }
  }, [props]);
  return (
    <Fragment>
      <h2 className="pf-popup-title text-upper">{title}</h2>
      <p className="text-muted">
        <strong>Work / </strong>
      </p>
    </Fragment>
  );
}

export default TitleProject;
