import React, { useState, useEffect } from 'react';
import TitleProject from './title-project';
import DetailProjectContent from './detail-project';
import AbovePost from '../../../shared/above-post';
import BelowPost from '../../../shared/below-post';

const PopupDetailProject = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.PORTFOLIO.POPUP.DETAIL.PROJECT-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);
  return (
    <div className="pf-popup-info">
      <TitleProject title={object.title} />
      <DetailProjectContent value={object} />
      <AbovePost />
      <BelowPost />
    </div>
  );
}

export default PopupDetailProject;
