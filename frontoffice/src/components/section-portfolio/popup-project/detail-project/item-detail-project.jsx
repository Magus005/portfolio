import React, { Fragment, useState, useEffect } from 'react';

const ItemDetailProject = props => {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  useEffect(() => {
    if(props.name) {
      console.log(`--SECTION.PORTFOLIO.POPUP.TITLE.PROJECT--  props = ${JSON.stringify(props)}`);
      setName(props.name);
    }
    if(props.value) {
      setValue(props.value);
    }
  }, [props]);
  return (
    <Fragment>
      <dt>{name}</dt>
      <dd>{value}</dd>
    </Fragment>
  );
}

export default ItemDetailProject;
