import React, { useState, useEffect } from 'react';
import ItemDetailProject from './item-detail-project';


const DetailProjectContent = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.PORTFOLIO.POPUP.DETAIL.PROJECT.CONTENT-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);
  return (
    <dl className="dl-horizontal">
      <ItemDetailProject name="Project" value={object.title} />
      <ItemDetailProject name="Customer" value={object.customer} />
      <ItemDetailProject name="Delivered" value={object.delivered} />
    </dl>
  );
}

export default DetailProjectContent;
