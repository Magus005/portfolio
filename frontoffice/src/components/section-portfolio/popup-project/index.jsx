import React, { useEffect, useState } from 'react';
import SlideProject from './slide-project/slide-project';
import PopupDetailProject from './detail-project/index';
//import MoreProject from './more-project/index';

const PortfolioPopupProject = props => {
  const [object, setObject] = useState([{}])
  useEffect(() => {
    console.log(`--SECTION.PORTFOLIO.POPUP.PROJECT-- props = ${JSON.stringify(props)}`);
    setObject(props.value);
  }, [props]);

  return (
    <div id={'pf-popup-' + object.id} className="pf-popup clearfix">
      <SlideProject key={object.id} picture={object.picture} />

      <div className="pf-popup-col2">

        <PopupDetailProject key={object.id} value={object} />
        {/** 
        <MoreProject />
        */}
      </div>
    </div>
  );
}

export default PortfolioPopupProject;
