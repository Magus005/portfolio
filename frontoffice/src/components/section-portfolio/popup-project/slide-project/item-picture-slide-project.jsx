import React, { useState, useEffect } from 'react';

const ItemPictureSlideProject = props => {
  const [picture, setPicture] = useState("");
  useEffect(() => {
    if(props.picture) {
      console.log(`--SECTION.PORTFOLIO.IMAGE.PROJECT--  props = ${JSON.stringify(props)}`);
      setPicture(props.picture);
    }
  }, [props]);
  return (
    <div className="pf-popup-slide">
      <div className="pf-popup-embed" data-type="image" data-width="419" data-height="293"
        data-url={picture}>
      </div>
    </div>
  );
}

export default ItemPictureSlideProject;
