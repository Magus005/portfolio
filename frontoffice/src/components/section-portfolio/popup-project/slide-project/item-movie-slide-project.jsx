import React from 'react';

const ItemMovieSlideProject = props => {
  return (
    <div className="pf-popup-slide">
      <div className="pf-popup-embed" data-type="iframe" data-width="419" data-height="293"
        data-url="https://www.youtube.com/embed/rP7YArQsDFY"></div>
    </div>
  );
}

export default ItemMovieSlideProject;
