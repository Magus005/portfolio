import React, { useState, useEffect } from 'react';
import ItemMovieSlideProject from './item-movie-slide-project';
import ItemPictureSlideProject from './item-picture-slide-project';


const SlideProject = props => {
  const [picture, setPicture] = useState("");
  useEffect(() => {
    if(props.picture) {
      console.log(`--SECTION.PORTFOLIO.IMAGE.PROJECT--  props = ${JSON.stringify(props)}`);
      setPicture(props.picture);
    }
  }, [props]);
  return (
    <div className="pf-popup-col1">
      <div className="pf-popup-media cr-slider" data-init="none">
        <ItemMovieSlideProject />

        <ItemPictureSlideProject picture={picture} />
      </div>
    </div>
  );
}

export default SlideProject;
