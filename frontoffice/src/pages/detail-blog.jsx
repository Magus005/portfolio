import React, { Fragment, lazy, Suspense } from 'react';
const Header = lazy(() => import('../components/header'));
const Top = lazy(() => import('./shared/top'));
const Bottom = lazy(() => import('./shared/bottom'));
const NavigationArticle = lazy(() => import('../components/blog/detail-article/navigation-article/index'));
const DetailArticle = lazy(() => import('../components/blog/detail-article'));

const DetailBlog = props => {
  return (
    <Fragment>
      <Suspense fallback={<h2>Loading ...</h2>}>
        <Header />
    
        <div id="crtContainer" className="crt-container">
    
          <Top />
          <div className="crt-container-sm">
            <div className="crt-paper-layers">
              <div className="crt-paper clearfix">
                <div className="crt-paper-cont paper-padd clear-mrg">
                  
                  <DetailArticle />
                  
                  <NavigationArticle />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Bottom />
      </Suspense>
    </Fragment>
  );
}

export default DetailBlog;
