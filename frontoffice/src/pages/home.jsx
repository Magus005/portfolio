import React, { Fragment } from 'react';
import Header from '../components/header';
import SectionAbout from '../components/section-about';
import SectionCompetence from '../components/section-competence';
import CareerPath from '../components/section-career-path';
//import SectionAddressBook from '../components/section-address-book';
import SectionPortfolio from '../components/section-portfolio';
import SectionInterestCenter from '../components/section-interest-center';
import SectionFaq from '../components/section-faq';
import SectionBlog from '../components/section-blog/section-blog';
import SectionContact from '../components/section-contact';
import Bottom from './shared/bottom';
import Top from './shared/top';

const Home = props => {
  return (
    <Fragment>
      <Header />
  
      <div id="crtContainer" className="crt-container">
  
        <Top />
  
        <div className="crt-container-sm">
  
          <SectionAbout />
  
          <SectionCompetence />
  
          <CareerPath />
          {/**
          <SectionAddressBook />
          */}
  
          <SectionPortfolio />
  
          <SectionInterestCenter />
  
          <SectionFaq />
  
          <SectionBlog />
  
          <SectionContact />
        </div>
      </div>

      <Bottom />
    </Fragment>
  );
}

export default Home;
