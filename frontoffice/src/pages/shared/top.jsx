import React, { Fragment } from 'react';
import SectionUserProfile from '../../components/section-user-profile';
import HeaderNavBarVertical from '../../components/header/nav-bar-vertical';



const Top = props => {
  return (
    <Fragment>
      <SectionUserProfile />
      <HeaderNavBarVertical />
    </Fragment>
  );
}

export default Top;
