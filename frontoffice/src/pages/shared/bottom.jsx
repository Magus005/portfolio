import React, { Fragment } from 'react';
import Menu from '../../components/menu';
import Footer from '../../components/footer';
import FloatingButton from '../../components/shared/floating-button';
import BackgroundShape from '../../components/shared/background-shape';

const Bottom = props => {
  return (
    <Fragment>
      <Menu />
      <Footer />
      <FloatingButton />
      <BackgroundShape />
    </Fragment>
  );
}

export default Bottom;
