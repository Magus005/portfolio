import React, { Fragment } from 'react';
import TitlePage from '../components/shared/title-page';
import Header from '../components/header';
import Top from './shared/top';
import Bottom from './shared/bottom';
import ItemListArticle from '../components/blog/item-list-article';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const client = new ApolloClient({
  uri: 'http://127.0.0.1:5000/api'
});

const GET_ALL_POSTS = gql`query getAllpostsQuery {
  getAllposts {
    _id
    picture
    movie
    content
    title
    userId
    dateCreated
    categories
  }
}`;


const ListBlog = props => {
  return (
    <ApolloProvider client={client}>
      <Fragment>
        <Header />
    
        <div id="crtContainer" className="crt-container">
    
          <Top />
    
          <div className="crt-container-sm">
            <TitlePage />
            <Query query={GET_ALL_POSTS}>
              {
                ({loading, error, data}) => {
                  if(loading) return <h2>Loading ...</h2>
                  if(error) console.log(`--LIST.BLOG.GET_ALL_POSTS-- ${JSON.stringify(error)}`);
                  console.log(`--LIST.BLOG.GET_ALL_POSTS-- ${JSON.stringify(data)}`);
                  return <Fragment>
                    {
                      data.getAllposts.map((post, index) => (
                        <ItemListArticle value={post} key={index} />
                      ))
                    }
                  </Fragment>
                }
              }
            </Query>
          </div>
        </div>

        <Bottom />
      </Fragment>
    </ApolloProvider>
  );
}

export default ListBlog;
