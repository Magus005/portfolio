from flask import Flask, request, jsonify, render_template
import os
import dialogflow
import requests
import json
import pusher

app = Flask(__name__)

def detect_intent_texts(project_id, session_id, text, language_code):
  session_client = dialogflow.sessionClient()
  session = session_client.session_path(project_id, session_id)
  if text:
    text_input = dialogflow.types.TextInput(text=text, language_code=language_code)
    query_input = dialogflow.types.QueryInput(text=text_input)
    response = session_client.detect_intent(session=session, query_input=query_input)

    return response.query_result.fulfillment_text

@app.route('/', methods=['GET'])
def index():
  return jsonify({"message": "hello word!"})

@app.route('/send_message', methods=['POST'])
def send_message():
  message = request.form['message']
  print(message)
  project_id = os.getenv('DIALOGFLOW_PROJECT_ID')
  fulfillment_text = detect_intent_texts(project_id, "unique", message, "fr")
  response_text = {"message": fulfillment_text}
  return jsonify(response_text)


@app.route('/webhook', methods=['POST'])
def webhook():
  data = request.get_json(silent= True)
  profile = data['queryResult']['parameters']['profile']
  print(profile)
  return jsonify({"fulfillment_text": profile})



if __name__ == "__main__":
  app.run