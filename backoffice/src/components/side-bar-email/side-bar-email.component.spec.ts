import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarEmailComponent } from './side-bar-email.component';

describe('SideBarEmailComponent', () => {
  let component: SideBarEmailComponent;
  let fixture: ComponentFixture<SideBarEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
