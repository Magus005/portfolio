import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { Category } from '../category';

const CREATE_CATEGORY_MUTATION = gql`mutation createCategoryMutation($name: String!) {
  createCategory(name: $name) {
    _id
    name
  }
}`;


@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.sass']
})
export class AddCategoryComponent implements OnInit {
  category: Category;
  categoryForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required ],
    });
  }

  submit() {
    const name = this.data.name;
    this.apollo.mutate({
      mutation: CREATE_CATEGORY_MUTATION,
      variables: { name },
      optimisticResponse: {
        __typename: 'Mutation',
        createCategory: {
          __typename: 'Category',
          name: name,
        },
      },
    }).subscribe(
      res => {
        console.log(`--CATEGORY.ADD-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/category/index']);
      },
      err => { console.log(`--CATEGORY.ADD-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/category/index']);
      }
    );
  }
}
