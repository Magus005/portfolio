import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Category } from '../category';
import { Router } from '@angular/router';


const GET_ALL_CATEGORIES_QUERY = gql`query getCategoriesQuery {
  getCategories {
    _id,
    name,
  }
}`;

const DELETE_CATEGORY_MUTATION = gql`mutation deleteCategoryMutation($id: ID!) {
  deleteCategory(id: $id) {
    _id
  }
}`;


@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.sass']
})
export class ListCategoryComponent implements OnInit {
  allCategories: Category[] = [];
  category: Category;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--CATEGORY.LIST-- Get all categories `);
    this.apollo.watchQuery({
      query: GET_ALL_CATEGORIES_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--CATEGORY.LIST-- Get all categories ${JSON.stringify(response)}`);
      this.allCategories = response.data['getCategories'];
      console.log(`--CATEGORY.LIST-- Get all categories finally ${JSON.stringify(this.allCategories)}`);
    });
  }

  delete(id) {
    console.log(`--CATEGORY.LIST-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_CATEGORY_MUTATION,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteCategory: {
          __typename: 'Category',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--CATEGORY.LIST-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/category/index']);
      },
      err => { console.log(`--CATEGORY.LIST-- submit error ${err}`); },
      () => this.router.navigate(['/category/index'])
    );
  }
}
