import { Routes } from '@angular/router';
import { AddCategoryComponent } from './add-category/add-category.component';
import { ListCategoryComponent } from './list-category/list-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';

export const CategoryRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'add', component: AddCategoryComponent },
  { path: 'index', component: ListCategoryComponent },
  { path: 'edit/:id', component: EditCategoryComponent },
];
