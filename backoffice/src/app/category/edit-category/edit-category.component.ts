import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Category } from '../category';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_CATEGORY_MUTATION = gql`mutation updateCategoryMutation($id: ID!, $name: String!) {
  updateCategory(id: $id, name: $name) {
    _id
  }
}`;

const GET_CATEGORY_QUERY = gql`query getCategoryQuery($id: ID!) {
  getCategory(id: $id) {
    _id,
    name,
  }
}`;

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.sass']
})
export class EditCategoryComponent implements OnInit {
  category: Category;
  categoryForm: FormGroup;
  data: any = {};
  id: string;
  name = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--CATEGORY.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getCategoryById();
  }

  updateForm() {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required ],
    });
  }

  getCategoryById() {
    console.log(`--CATEGORY.EDIT-- Get all category `);
    this.apollo.watchQuery({
      query: GET_CATEGORY_QUERY,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--CATEGORY.EDIT-- Get all category ${JSON.stringify(response)}`);
      this.category = response.data['getCategory'];
      this.name.setValue(this.category.name);
      console.log(`--CATEGORY.EDIT-- Get all category finally ${JSON.stringify(this.category)}`);
    });
  }

  submit() {
    const name = this.name.value;
    this.apollo.mutate({
      mutation: UPDATE_CATEGORY_MUTATION,
      variables: {
        id: this.id,
        name
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateCategory: {
          __typename: 'Category',
          name: name,
        },
      },
    }).subscribe(
      res => {
        console.log(`--CATEGORY.EDIT-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/category/index']);
      },
      err => { console.log(`--CATEGORY.EDIT-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/category/index']);
      }
    );
  }

}
