import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { CenterOfInterest } from '../center-of-interest';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_CENTER_OF_INTEREST_MUTATION = gql`mutation updateCenterOfInterestMutation($id: ID!,
  $centerOfInterest: InputCenterOfInterest!) {
  updateCenterOfInterest(id: $id, centerOfInterest: $centerOfInterest) {
    _id
    name
    icon
    createdAt
  }
}`;

const GET_CENTER_OF_INTEREST_QUERY = gql`query getCenterOfInterestQuery($id: ID!) {
  getCenterOfInterest(id: $id) {
    _id
    name
    icon
  }
}`;

@Component({
  selector: 'app-edit-center-of-interest',
  templateUrl: './edit-center-of-interest.component.html',
  styleUrls: ['./edit-center-of-interest.component.sass']
})
export class EditCenterOfInterestComponent implements OnInit {
  centerOfInterest: CenterOfInterest;
  centerOfInterestForm: FormGroup;
  data: any = {};
  id: string;
  name = new FormControl('');
  icon = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--CENTER_OF_INTEREST.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getCenterOfInterestById();
  }

  updateForm() {
    this.centerOfInterestForm = this.formBuilder.group({
      name: ['', Validators.required ],
      icon: ['', Validators.required],
    });
  }

  getCenterOfInterestById() {
    console.log(`--CENTER_OF_INTEREST.EDIT-- Get all center of interest `);
    this.apollo.watchQuery({
      query: GET_CENTER_OF_INTEREST_QUERY,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--CENTER_OF_INTEREST.EDIT-- Get all center of interest ${JSON.stringify(response)}`);
      this.centerOfInterest = response.data['getCenterOfInterest'];
      this.name.setValue(this.centerOfInterest.name);
      this.icon.setValue(this.centerOfInterest.icon);
      console.log(`--CENTER_OF_INTEREST.EDIT-- Get all center of interest finally ${JSON.stringify(this.centerOfInterest)}`);
    });
  }

  submit() {
    const centerOfInterest = {
      name: this.name.value,
      icon: this.icon.value,
    };
    this.apollo.mutate({
      mutation: UPDATE_CENTER_OF_INTEREST_MUTATION,
      variables: {
        id: this.id,
        centerOfInterest
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateCenterOfInterest: {
          __typename: 'CenterOfInterest',
          centerOfInterest: centerOfInterest,
        },
      },
    }).subscribe(
      res => {
        console.log(`--EDIT.CENTER_OF_INTEREST-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/center-of-interest/index']);
      },
      err => { console.log(`--EDIT.CENTER_OF_INTEREST-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/center-of-interest/index']);
      }
    );
  }
}
