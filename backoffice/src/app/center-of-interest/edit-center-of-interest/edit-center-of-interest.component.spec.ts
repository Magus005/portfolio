import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCenterOfInterestComponent } from './edit-center-of-interest.component';

describe('EditCenterOfInterestComponent', () => {
  let component: EditCenterOfInterestComponent;
  let fixture: ComponentFixture<EditCenterOfInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCenterOfInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCenterOfInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
