import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CenterOfInterestRoutes } from './center-of-interest.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListCenterOfInterestComponent } from './list-center-of-interest/list-center-of-interest.component';
import { AddCenterOfInterestComponent } from './add-center-of-interest/add-center-of-interest.component';
import { EditCenterOfInterestComponent } from './edit-center-of-interest/edit-center-of-interest.component';

@NgModule({
  declarations: [
    ListCenterOfInterestComponent,
    AddCenterOfInterestComponent,
    EditCenterOfInterestComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(CenterOfInterestRoutes),
    SharedModule
  ]
})
export class CenterOfInterestModule {}
