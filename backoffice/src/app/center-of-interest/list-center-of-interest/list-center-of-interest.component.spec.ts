import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCenterOfInterestComponent } from './list-center-of-interest.component';

describe('ListCenterOfInterestComponent', () => {
  let component: ListCenterOfInterestComponent;
  let fixture: ComponentFixture<ListCenterOfInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCenterOfInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCenterOfInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
