import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { CenterOfInterest } from '../center-of-interest';
import { Router } from '@angular/router';


const GET_ALL_CENTER_OF_INTERESTS_QUERY = gql`query getAllCenterOfInterestsQuery {
  getAllCenterOfInterests {
    _id,
    name,
    icon,
    createdAt,
  }
}`;

const DELETE_CENTER_OF_INTEREST_MUTATION = gql`mutation deleteCenterOfInterestMutation($id: ID!) {
  deleteCenterOfInterest(id: $id) {
    _id
  }
}`;


@Component({
  selector: 'app-list-center-of-interest',
  templateUrl: './list-center-of-interest.component.html',
  styleUrls: ['./list-center-of-interest.component.sass']
})
export class ListCenterOfInterestComponent implements OnInit {
  allCenterOfInterests: CenterOfInterest[] = [];
  centerOfInterest: CenterOfInterest;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--CENTER_OF_INTEREST.LIST-- Get all centerOfInterest `);
    this.apollo.watchQuery({
      query: GET_ALL_CENTER_OF_INTERESTS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--CENTER_OF_INTEREST.LIST-- Get all centerOfInterest ${JSON.stringify(response)}`);
      this.allCenterOfInterests = response.data['getAllCenterOfInterests'];
      console.log(`--CENTER_OF_INTEREST.LIST-- Get all centerOfInterest finally ${JSON.stringify(this.allCenterOfInterests)}`);
    });
  }

  delete(id) {
    console.log(`--LIST.CENTER_OF_INTEREST-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_CENTER_OF_INTEREST_MUTATION,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteCenterOfInterest: {
          __typename: 'CenterOfInterest',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.CenterOfInterest-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/center-of-interest/index']);
      },
      err => { console.log(`--ADD.CenterOfInterest-- submit error ${err}`); },
      () => this.router.navigate(['/center-of-interest/index'])
    );
  }

}
