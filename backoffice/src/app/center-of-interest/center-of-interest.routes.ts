import { Routes } from '@angular/router';
import { ListCenterOfInterestComponent } from './list-center-of-interest/list-center-of-interest.component';
import { AddCenterOfInterestComponent } from './add-center-of-interest/add-center-of-interest.component';
import { EditCenterOfInterestComponent } from './edit-center-of-interest/edit-center-of-interest.component';

export const CenterOfInterestRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListCenterOfInterestComponent },
  { path: 'add', component: AddCenterOfInterestComponent },
  { path: 'edit/:id', component: EditCenterOfInterestComponent },
];
