import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCenterOfInterestComponent } from './add-center-of-interest.component';

describe('AddCenterOfInterestComponent', () => {
  let component: AddCenterOfInterestComponent;
  let fixture: ComponentFixture<AddCenterOfInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCenterOfInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCenterOfInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
