import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { CenterOfInterest } from '../center-of-interest';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_CENTER_OF_INTEREST_MUTATION = gql`mutation createCenterOfInterestMutation($centerOfInterest: InputCenterOfInterest!) {
  createCenterOfInterest(centerOfInterest: $centerOfInterest) {
    _id,
    name,
    icon,
    createdAt,
  }
}`;

@Component({
  selector: 'app-add-center-of-interest',
  templateUrl: './add-center-of-interest.component.html',
  styleUrls: ['./add-center-of-interest.component.sass']
})
export class AddCenterOfInterestComponent implements OnInit {
  centerOfInterest: CenterOfInterest;
  centerOfInterestForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.centerOfInterestForm = this.formBuilder.group({
      name: ['', Validators.required ],
      icon: ['', Validators.required],
    });
  }

  submit() {
    const centerOfInterest = {
      name: this.data.name,
      icon: this.data.icon,
      state: true,
    };
    this.apollo.mutate({
      mutation: CREATE_CENTER_OF_INTEREST_MUTATION,
      variables: { centerOfInterest },
      optimisticResponse: {
        __typename: 'Mutation',
        createCenterOfInterest: {
          __typename: 'CenterOfInterest',
          centerOfInterest: centerOfInterest,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.CENTER_OF_INTEREST-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/center-of-interest/index']);
      },
      err => { console.log(`--ADD.CENTER_OF_INTEREST-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/center-of-interest/index']);
      }
    );
  }

}
