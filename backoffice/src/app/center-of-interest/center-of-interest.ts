export class CenterOfInterest {
  _id: string;
  name: string;
  icon: string;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
