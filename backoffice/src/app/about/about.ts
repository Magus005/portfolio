export class About {
  _id: string;
  title: string;
  content: string;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
