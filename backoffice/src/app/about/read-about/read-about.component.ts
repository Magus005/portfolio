import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { About } from '../about';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';


const GET_ABOUT_BY_ID = gql`query getAboutQuery($id: ID!) {
  getAbout(id: $id) {
    _id,
    title,
    content,
    createdAt,
    updatedAt
  }
}`;

@Component({
  selector: 'app-read-about',
  templateUrl: './read-about.component.html',
  styleUrls: ['./read-about.component.sass']
})
export class ReadAboutComponent implements OnInit {
  id: string;
  about: About[] = [];

  constructor(private route: ActivatedRoute, private apollo: Apollo) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--ABOUT.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getAboutById();
  }

  getAboutById() {
    console.log(`--ABOUT.LIST-- Get all about `);
    this.apollo.watchQuery({
      query: GET_ABOUT_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--ABOUT.LIST-- Get all about ${JSON.stringify(response)}`);
      this.about = response.data['getAbout'];
      console.log(`--ABOUT.LIST-- Get all about finally ${JSON.stringify(this.about)}`);
    });
  }

}
