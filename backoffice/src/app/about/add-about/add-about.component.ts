import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { About } from '../about';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const createAbout = gql`mutation createAbout($about: InputAbout!) {
  createAbout(about: $about) {
    _id
    title
    content
    state
    createdAt
    updatedAt
  }
}`;

@Component({
  selector: 'app-add-about',
  templateUrl: './add-about.component.html',
  styleUrls: ['./add-about.component.sass']
})
export class AddAboutComponent implements OnInit {
  about: About;
  aboutForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.aboutForm = this.formBuilder.group({
      title: ['', Validators.required ],
      content: ['', Validators.required]
    });
  }

  submit() {
    const title = this.data.title;
    const content = this.data.content;
    const about = {
      title,
      content,
      state: true
    };
    console.log(`Title = ${title} / Content = ${content}`);
    this.apollo.mutate({
      mutation: createAbout,
      variables: { about },
      optimisticResponse: {
        __typename: 'Mutation',
        createAbout: {
          __typename: 'About',
          about: about,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.ABOUT-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/about/index']);
      },
      err => { console.log(`--ADD.ABOUT-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/about/index']);
      }
    );
  }

}
