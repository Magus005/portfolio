import { Routes } from '@angular/router';
import { AddAboutComponent } from './add-about/add-about.component';
import { ListAboutComponent } from './list-about/list-about.component';
import { ReadAboutComponent } from './read-about/read-about.component';
import { EditAboutComponent } from './edit-about/edit-about.component';

export const AboutRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'add', component: AddAboutComponent },
  { path: 'index', component: ListAboutComponent },
  { path: 'read/:id', component: ReadAboutComponent },
  { path: 'edit/:id', component: EditAboutComponent }
];
