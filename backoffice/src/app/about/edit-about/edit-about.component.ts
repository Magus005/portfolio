import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { About } from '../about';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_ABOUT_QUERY = gql`mutation updateAboutQuery($id: ID!, $about: InputAbout!) {
  updateAbout(id: $id, about: $about) {
    _id
    title
    content
    state
    createdAt
    updatedAt
  }
}`;

const GET_ABOUT_BY_ID = gql`query getAboutQuery($id: ID!) {
  getAbout(id: $id) {
    _id,
    title,
    content,
    createdAt,
    updatedAt
  }
}`;


@Component({
  selector: 'app-edit-about',
  templateUrl: './edit-about.component.html',
  styleUrls: ['./edit-about.component.sass']
})
export class EditAboutComponent implements OnInit {
  about: About;
  aboutForm: FormGroup;
  data: any = {};
  id: string;
  title = new FormControl('');
  content = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--ABOUT.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getAboutById();
  }

  updateForm() {
    this.aboutForm = this.formBuilder.group({
      title: ['', Validators.required ],
      content: ['', Validators.required]
    });
  }

  getAboutById() {
    console.log(`--ABOUT.EDIT-- Get all about `);
    this.apollo.watchQuery({
      query: GET_ABOUT_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--ABOUT.EDIT-- Get all about ${JSON.stringify(response)}`);
      this.about = response.data['getAbout'];
      this.title.setValue(this.about.title);
      this.content.setValue(this.about.content);
      console.log(`--ABOUT.EDIT-- Get all about finally ${JSON.stringify(this.about)}`);
    });
  }

  submit() {
    const title = this.title.value;
    const content = this.content.value;
    const about = {
      title,
      content,
      state: true
    };
    console.log(`Title = ${this.title.value} / Content = ${this.content.value}`);
    this.apollo.mutate({
      mutation: UPDATE_ABOUT_QUERY,
      variables: {
        id: this.id,
        about
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateAbout: {
          __typename: 'About',
          about: about,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.ABOUT-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/about/index']);
      },
      err => { console.log(`--ADD.ABOUT-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/about/index']);
      }
    );
  }
}
