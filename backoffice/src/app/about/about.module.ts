import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AboutRoutes } from './about.routes';
import { AddAboutComponent } from './add-about/add-about.component';
import { ListAboutComponent } from './list-about/list-about.component';
import { ReadAboutComponent } from './read-about/read-about.component';
import { EditAboutComponent } from './edit-about/edit-about.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AddAboutComponent,
    ListAboutComponent,
    ReadAboutComponent,
    EditAboutComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(AboutRoutes),
    SharedModule
  ]
})
export class AboutModule {}
