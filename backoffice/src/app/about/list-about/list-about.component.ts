import { Component, OnInit, OnDestroy } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { About } from '../about';
import { Router } from '@angular/router';


const GET_ALL_ABOUTS_QUERY = gql`query getAllAboutsQuery {
  getAllAbouts {
    _id,
    title,
    content,
    createdAt,
    updatedAt
  }
}`;

const DELETE_ABOUT_QUERY = gql`mutation deleteAboutQuery($id: ID!) {
  deleteAbout(id: $id) {
    _id
    title
    content
    state
    createdAt
    updatedAt
  }
}`;

@Component({
  selector: 'app-list-about',
  templateUrl: './list-about.component.html',
  styleUrls: ['./list-about.component.sass']
})
export class ListAboutComponent implements OnInit, OnDestroy {
  allAbouts: About[] = [];
  about: About;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--ABOUT.LIST-- Get all about `);
    this.apollo.watchQuery({
      query: GET_ALL_ABOUTS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--ABOUT.LIST-- Get all about ${JSON.stringify(response)}`);
      this.allAbouts = response.data['getAllAbouts'];
      console.log(`--ABOUT.LIST-- Get all about finally ${JSON.stringify(this.allAbouts)}`);
    });
  }

  ngOnDestroy() {

  }

  delete(id) {
    console.log(`--LIST.ABOUT-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_ABOUT_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteAbout: {
          __typename: 'About',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.ABOUT-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/about/index']);
      },
      err => { console.log(`--ADD.ABOUT-- submit error ${err}`); },
      () => this.router.navigate(['/about/index'])
    );
  }

}
