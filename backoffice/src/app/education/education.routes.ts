import { Routes } from '@angular/router';
import { ListEducationComponent } from './list-education/list-education.component';
import { AddEducationComponent } from './add-education/add-education.component';
import { EditEducationComponent } from './edit-education/edit-education.component';

export const EducationRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListEducationComponent },
  { path: 'add', component: AddEducationComponent },
  { path: 'edit/:id', component: EditEducationComponent },
];
