import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EducationRoutes } from './education.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListEducationComponent } from './list-education/list-education.component';
import { AddEducationComponent } from './add-education/add-education.component';
import { EditEducationComponent } from './edit-education/edit-education.component';

@NgModule({
  declarations: [
    ListEducationComponent,
    AddEducationComponent,
    EditEducationComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(EducationRoutes),
    SharedModule
  ]
})
export class EducationModule {}
