export class Education {
  _id: string;
  title: string;
  schoolName: string;
  startDate: Date;
  endDate: Date;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
