import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Education } from '../education';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_EDUCATION_MUTATION = gql`mutation updateEducationMutation($id: ID!, $education: InputEducation!) {
  updateEducation(id: $id, education: $education) {
    _id
    title
    schoolName
    startDate
    endDate
  }
}`;

const GET_EDUCATION_BY_ID = gql`query getEducationQuery($id: ID!) {
  getEducation(id: $id) {
    _id
    title
    schoolName
    startDate
    endDate
  }
}`;


@Component({
  selector: 'app-edit-education',
  templateUrl: './edit-education.component.html',
  styleUrls: ['./edit-education.component.sass']
})
export class EditEducationComponent implements OnInit {
  education: Education;
  educationForm: FormGroup;
  data: any = {};
  id: string;
  title = new FormControl('');
  schoolName = new FormControl('');
  startDate = new FormControl('');
  endDate = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--EDUCATION.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getEducationById();
  }

  updateForm() {
    this.educationForm = this.formBuilder.group({
      title: ['', Validators.required ],
      schoolName: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });
  }

  getEducationById() {
    console.log(`--EDUCATION.EDIT-- Get all education `);
    this.apollo.watchQuery({
      query: GET_EDUCATION_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--EDUCATION.EDIT-- Get all education ${JSON.stringify(response)}`);
      this.education = response.data['getEducation'];
      this.title.setValue(this.education.title);
      this.schoolName.setValue(this.education.schoolName);
      this.startDate.setValue(this.education.startDate);
      this.endDate.setValue(this.education.endDate);
      console.log(`--EDUCATION.EDIT-- Get all education finally ${JSON.stringify(this.education)}`);
    });
  }

  submit() {
    const education = {
      title: this.title.value,
      schoolName: this.schoolName.value,
      startDate: this.startDate.value,
      endDate: this.endDate.value,
    };
    this.apollo.mutate({
      mutation: UPDATE_EDUCATION_MUTATION,
      variables: {
        id: this.id,
        education
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateEducation: {
          __typename: 'Education',
          education: education,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.EDUCATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/education/index']);
      },
      err => { console.log(`--ADD.EDUCATION-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/education/index']);
      }
    );
  }
}
