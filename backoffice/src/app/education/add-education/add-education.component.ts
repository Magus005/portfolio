import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Education } from '../education';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_EDUCATION_MUTATION = gql`mutation createEducationMutation($education: InputEducation!) {
  createEducation(education: $education) {
    _id,
    title,
    schoolName,
    startDate,
    endDate,
    state,
    createdAt,
    updatedAt,
  }
}`;


@Component({
  selector: 'app-add-education',
  templateUrl: './add-education.component.html',
  styleUrls: ['./add-education.component.sass']
})
export class AddEducationComponent implements OnInit {
  education: Education;
  educationForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.educationForm = this.formBuilder.group({
      title: ['', Validators.required ],
      schoolName: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });
  }

  submit() {
    const education = {
      title: this.data.title,
      schoolName: this.data.schoolName,
      startDate: this.data.startDate,
      endDate: this.data.endDate,
      state: true,
    };
    this.apollo.mutate({
      mutation: CREATE_EDUCATION_MUTATION,
      variables: { education },
      optimisticResponse: {
        __typename: 'Mutation',
        createEducation: {
          __typename: 'Education',
          education: education,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.EDUCATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/education/index']);
      },
      err => { console.log(`--ADD.EDUCATION-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/education/index']);
      }
    );
  }

}
