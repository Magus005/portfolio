import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Education } from '../education';
import { Router } from '@angular/router';


const GET_ALL_EDUCATIONS_QUERY = gql`query getAllEducationsQuery {
  getAllEducations {
    _id,
    title,
    schoolName,
    startDate,
    endDate,
  }
}`;

const DELETE_EDUCATION_MUTATION = gql`mutation deleteEducationMutation($id: ID!) {
  deleteEducation(id: $id) {
    _id
  }
}`;

@Component({
  selector: 'app-list-education',
  templateUrl: './list-education.component.html',
  styleUrls: ['./list-education.component.sass']
})
export class ListEducationComponent implements OnInit {
  allEducations: Education[] = [];
  education: Education;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--EDUCATION.LIST-- Get all education `);
    this.apollo.watchQuery({
      query: GET_ALL_EDUCATIONS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--EDUCATION.LIST-- Get all education ${JSON.stringify(response)}`);
      this.allEducations = response.data['getAllEducations'];
      console.log(`--EDUCATION.LIST-- Get all education finally ${JSON.stringify(this.allEducations)}`);
    });
  }

  delete(id) {
    console.log(`--LIST.EDUCATION-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_EDUCATION_MUTATION,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteEducation: {
          __typename: 'Education',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.EDUCATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/education/index']);
      },
      err => { console.log(`--ADD.EDUCATION-- submit error ${err}`); },
      () => this.router.navigate(['/education/index'])
    );
  }

}
