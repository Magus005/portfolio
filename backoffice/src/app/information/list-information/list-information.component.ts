import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Information } from '../information';
import { Router } from '@angular/router';



const GET_ALL_INFORMATIONS_QUERY = gql`query getAllInformationsQuery {
  getAllInformations {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;

const DELETE_INFORMATION_QUERY = gql`mutation deleteInformationQuery($id: ID!) {
  deleteInformation(id: $id) {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-list-information',
  templateUrl: './list-information.component.html',
  styleUrls: ['./list-information.component.sass']
})
export class ListInformationComponent implements OnInit {
  allInformations: Information[] = [];

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--INFORMATION.LIST-- Get all informations `);
    this.apollo.watchQuery({
      query: GET_ALL_INFORMATIONS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--INFORMATION.LIST-- Get all informations ${JSON.stringify(response)}`);
      this.allInformations = response.data['getAllInformations'];
      console.log(`--INFORMATION.LIST-- Get all information finally ${JSON.stringify(this.allInformations)}`);
    });
  }

  delete(id) {
    console.log(`--DELETE.INFORMATION-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_INFORMATION_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteInformation: {
          __typename: 'Information',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--DELETE.INFORMATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/information/index']);
      },
      err => { console.log(`--DELETE.INFORMATION-- submit error ${err}`); },
      () => this.router.navigate(['/information/index'])
    );
  }

}
