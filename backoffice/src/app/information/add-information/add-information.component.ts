import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Information } from '../information';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_INFORMATION_MUTATION = gql`mutation createInformationMutation($information: InputInformation!) {
  createInformation(information: $information) {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-add-information',
  templateUrl: './add-information.component.html',
  styleUrls: ['./add-information.component.sass']
})
export class AddInformationComponent implements OnInit {
  information: Information;
  informationForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.informationForm = this.formBuilder.group({
      fullname: ['', Validators.required ],
      dateOfBirth: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });
  }

  submit() {
    const title = this.data.title;
    const content = this.data.content;
    const information = {
      fullname: this.data.fullname,
      dateOfBirth: this.data.dateOfBirth,
      address: this.data.address,
      email: this.data.email,
      phoneNumber: this.data.phoneNumber,
      state: true
    };
    console.log(`Title = ${title} / Content = ${content}`);
    this.apollo.mutate({
      mutation: CREATE_INFORMATION_MUTATION,
      variables: { information },
      optimisticResponse: {
        __typename: 'Mutation',
        createInformation: {
          __typename: 'Information',
          information: information,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.INFORMATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/information/index']);
      },
      err => { console.log(`--ADD.INFORMATION-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/information/index']);
      }
    );
  }

}
