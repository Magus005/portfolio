export class Information {
  _id: string;
  fullname: string;
  dateOfBirth: string;
  address: string;
  email: Date;
  phoneNumber: Date;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
