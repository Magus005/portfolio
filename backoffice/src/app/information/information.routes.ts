import { Routes } from '@angular/router';
import { ListInformationComponent } from './list-information/list-information.component';
import { AddInformationComponent } from './add-information/add-information.component';
import { EditInformationComponent } from './edit-information/edit-information.component';
import { ReadInformationComponent } from './read-information/read-information.component';

export const InformationRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListInformationComponent },
  { path: 'add', component: AddInformationComponent },
  { path: 'edit/:id', component: EditInformationComponent },
  { path: 'read/:id', component: ReadInformationComponent }
];
