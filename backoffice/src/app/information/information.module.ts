import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InformationRoutes } from './information.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListInformationComponent } from './list-information/list-information.component';
import { AddInformationComponent } from './add-information/add-information.component';
import { ReadInformationComponent } from './read-information/read-information.component';
import { EditInformationComponent } from './edit-information/edit-information.component';

@NgModule({
  declarations: [
    ListInformationComponent,
    AddInformationComponent,
    ReadInformationComponent,
    EditInformationComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(InformationRoutes),
    SharedModule
  ]
})
export class InformationModule {}
