import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Information } from '../information';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_INFORMATION_QUERY = gql`mutation updateInformationQuery($id: ID!, $information: InputInformation!) {
  updateInformation(id: $id, information: $information) {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;

const GET_INFORMATION_BY_ID = gql`query getInformationQuery($id: ID!) {
  getInformation(id: $id) {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-edit-information',
  templateUrl: './edit-information.component.html',
  styleUrls: ['./edit-information.component.sass']
})
export class EditInformationComponent implements OnInit {
  information: Information;
  informationForm: FormGroup;
  data: any = {};
  id: string;
  fullname = new FormControl('');
  dateOfBirth = new FormControl('');
  address = new FormControl('');
  email = new FormControl('');
  phoneNumber = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--INFORMATION.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getInformationById();
  }

  updateForm() {
    this.informationForm = this.formBuilder.group({
      fullname: ['', Validators.required ],
      dateOfBirth: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });
  }

  getInformationById() {
    console.log(`--INFORMATION.EDIT-- Get all information `);
    this.apollo.watchQuery({
      query: GET_INFORMATION_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--INFORMATION.EDIT-- Get all information ${JSON.stringify(response)}`);
      this.information = response.data['getInformation'];
      this.fullname.setValue(this.information.fullname);
      this.dateOfBirth.setValue(this.information.dateOfBirth);
      this.address.setValue(this.information.address);
      this.email.setValue(this.information.email);
      this.phoneNumber.setValue(this.information.phoneNumber);
      console.log(`--INFORMATION.EDIT-- Get all information finally ${JSON.stringify(this.information)}`);
    });
  }

  submit() {
    const fullname = this.fullname.value;
    const dateOfBirth = this.dateOfBirth.value;
    const address = this.address.value;
    const email = this.email.value;
    const phoneNumber = this.phoneNumber.value;
    const information = {
      fullname,
      dateOfBirth,
      address,
      email,
      phoneNumber,
      state: true
    };
    console.log(`nom prenom = ${this.fullname.value} / date anniversaire = ${this.dateOfBirth.value}`);
    this.apollo.mutate({
      mutation: UPDATE_INFORMATION_QUERY,
      variables: {
        id: this.id,
        information
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateInformation: {
          __typename: 'Information',
          information: information,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.INFORMATION-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/information/index']);
      },
      err => { console.log(`--ADD.INFORMATION-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/information/index']);
      }
    );
  }

}
