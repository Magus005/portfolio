import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadInformationComponent } from './read-information.component';

describe('ReadInformationComponent', () => {
  let component: ReadInformationComponent;
  let fixture: ComponentFixture<ReadInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
