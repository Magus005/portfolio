import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Information } from '../information';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';


const GET_INFORMATION_BY_ID = gql`query getInformationQuery($id: ID!) {
  getInformation(id: $id) {
    _id,
    fullname,
    dateOfBirth,
    address,
    email,
    phoneNumber,
    createdAt,
    updatedAt,
  }
}`;


@Component({
  selector: 'app-read-information',
  templateUrl: './read-information.component.html',
  styleUrls: ['./read-information.component.sass']
})
export class ReadInformationComponent implements OnInit {
  id: string;
  information: Information;

  constructor(private route: ActivatedRoute, private apollo: Apollo) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--INFORMATION.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getInformationById();
  }

  getInformationById() {
    console.log(`--INFORMATION.LIST-- Get all information `);
    this.apollo.watchQuery({
      query: GET_INFORMATION_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--INFORMATION.LIST-- Get all information ${JSON.stringify(response)}`);
      this.information = response.data['getInformation'];
      console.log(`--INFORMATION.LIST-- Get all information finally ${JSON.stringify(this.information)}`);
    });
  }
}
