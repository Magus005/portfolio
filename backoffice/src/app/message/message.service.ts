import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create message
  create(faq): Observable<Message> {
    return this.http.post<Message>(`${environment.serviceNetwork.uri}message`, JSON.stringify(faq), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  find(): Observable<Message> {
    return this.http.get<Message>(`${environment.serviceNetwork.uri}message/`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<Message>(`${environment.serviceNetwork.uri}message/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(id, message) {
    return this.http.put<Message>(`${environment.serviceNetwork.uri}message/${id}`, JSON.stringify(message), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<Message>(`${environment.serviceNetwork.uri}message/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
