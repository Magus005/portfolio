import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MessageRoutes } from './message.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListMessageComponent } from './list-message/list-message.component';
import { ReadMessageComponent } from './read-message/read-message.component';

@NgModule({
  declarations: [
    ListMessageComponent,
    ReadMessageComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(MessageRoutes),
    SharedModule
  ]
})
export class MessageModule {}
