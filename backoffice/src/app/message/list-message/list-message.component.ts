import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-list-message',
  templateUrl: './list-message.component.html',
  styleUrls: ['./list-message.component.sass']
})
export class ListMessageComponent implements OnInit {
  allMessages: any = [];

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.listMessages();
  }

  // Get messages list
  listMessages() {
    return this.messageService.find().subscribe((data: {}) => {
      this.allMessages = data;
      console.log(`--MESSAGE.LIST-- list messages = ${JSON.stringify(this.allMessages)}`);
    });
  }

  // Delete Message
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.messageService.delete(id).subscribe(data => {
        this.listMessages();
      });
    }
  }

}
