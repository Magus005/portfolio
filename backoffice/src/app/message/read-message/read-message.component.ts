import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-read-message',
  templateUrl: './read-message.component.html',
  styleUrls: ['./read-message.component.sass']
})
export class ReadMessageComponent implements OnInit {
  message: any;
  id: string;

  constructor(private messageService: MessageService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--MESSAGE.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getMessage();
  }

  // Get messages list
  getMessage() {
    return this.messageService.findOne(this.id).subscribe((data: {}) => {
      this.message = data;
      console.log(`--MESSAGE.LIST-- list messages = ${JSON.stringify(this.message)}`);
    });
  }

}
