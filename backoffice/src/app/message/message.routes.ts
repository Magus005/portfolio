import { Routes } from '@angular/router';
import { ListMessageComponent } from './list-message/list-message.component';
import { ReadMessageComponent } from './read-message/read-message.component';

export const MessageRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListMessageComponent },
  { path: 'read/:id', component: ReadMessageComponent },
];
