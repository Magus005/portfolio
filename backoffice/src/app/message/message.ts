export class Message {
  _id: string;
  name: string;
  email: string;
  subject: string;
  content: string;
  state: boolean;
}
