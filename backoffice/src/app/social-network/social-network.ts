export class SocialNetwork {
  _id: string;
  icon: string;
  name: string;
  uri: string;
}
