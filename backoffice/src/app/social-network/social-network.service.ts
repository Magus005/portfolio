import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { SocialNetwork } from './social-network';
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SocialNetworkService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create social-network
  create(socialnetwork): Observable<SocialNetwork> {
    return this.http.post<SocialNetwork>(`${environment.serviceNetwork.uri}socialnetwork`, JSON.stringify(socialnetwork), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  find(): Observable<SocialNetwork> {
    return this.http.get<SocialNetwork>(`${environment.serviceNetwork.uri}socialnetwork`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<SocialNetwork>(`${environment.serviceNetwork.uri}socialnetwork/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(id, socialnetwork) {
    return this.http.put<SocialNetwork>(`${environment.serviceNetwork.uri}socialnetwork/${id}`,
    JSON.stringify(socialnetwork), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<SocialNetwork>(`${environment.serviceNetwork.uri}socialnetwork/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
