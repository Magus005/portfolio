import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SocialNetworkService } from '../social-network.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-social-network',
  templateUrl: './add-social-network.component.html',
  styleUrls: ['./add-social-network.component.sass']
})
export class AddSocialNetworkComponent implements OnInit {
  socialNetworkForm: FormGroup;
  data: any = {};

  constructor(private socialNetworkService: SocialNetworkService, private router: Router,
  private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.socialNetworkForm = this.formBuilder.group({
      name: ['', Validators.required ],
      icon: ['', Validators.required],
      uri: ['', Validators.required],
    });
  }

  submit() {
    const socialNetwrok = {
      name: this.data.name,
      icon: this.data.icon,
      uri: this.data.uri,
    };
    this.socialNetworkService.create(socialNetwrok).subscribe((data: {}) => {
      console.log(`--SOCIAL_NETWORK.ADD-- object social network = ${JSON.stringify(data)}`);
      this.router.navigate(['/social-network/index']);
    });
  }

}
