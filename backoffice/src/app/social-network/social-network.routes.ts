import { Routes } from '@angular/router';

import { ListSocialNetworkComponent } from './list-social-network/list-social-network.component';
import { AddSocialNetworkComponent } from './add-social-network/add-social-network.component';
import { EditSocialNetworkComponent } from './edit-social-network/edit-social-network.component';

export const SocialNetworkRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListSocialNetworkComponent },
  { path: 'add', component: AddSocialNetworkComponent },
  { path: 'edit/:id', component: EditSocialNetworkComponent },
];
