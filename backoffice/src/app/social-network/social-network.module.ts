import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SocialNetworkRoutes } from './social-network.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListSocialNetworkComponent } from './list-social-network/list-social-network.component';
import { AddSocialNetworkComponent } from './add-social-network/add-social-network.component';
import { EditSocialNetworkComponent } from './edit-social-network/edit-social-network.component';

@NgModule({
  declarations: [
    ListSocialNetworkComponent,
    AddSocialNetworkComponent,
    EditSocialNetworkComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(SocialNetworkRoutes),
    SharedModule
  ]
})
export class SocialNetworkModule {}
