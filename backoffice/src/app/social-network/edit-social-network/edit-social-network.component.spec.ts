import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSocialNetworkComponent } from './edit-social-network.component';

describe('EditSocialNetworkComponent', () => {
  let component: EditSocialNetworkComponent;
  let fixture: ComponentFixture<EditSocialNetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSocialNetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSocialNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
