import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SocialNetworkService } from '../social-network.service';

@Component({
  selector: 'app-edit-social-network',
  templateUrl: './edit-social-network.component.html',
  styleUrls: ['./edit-social-network.component.sass']
})
export class EditSocialNetworkComponent implements OnInit {
  socialNetwork: any;
  socialNetworkForm: FormGroup;
  id: string;
  name = new FormControl('');
  icon = new FormControl('');
  uri = new FormControl('');

  constructor(private socialNetworService: SocialNetworkService, private formBuilder: FormBuilder,
  private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--SOCIAL_NETWORK.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getSocialNetwork();
  }

  updateForm() {
    this.socialNetworkForm = this.formBuilder.group({
      name: ['', Validators.required ],
      icon: ['', Validators.required],
      uri: ['', Validators.required],
    });
  }

  // Get social network list
  getSocialNetwork() {
    return this.socialNetworService.findOne(this.id).subscribe((data: {}) => {
      this.socialNetwork = data;
      this.name.setValue(this.socialNetwork.name);
      this.icon.setValue(this.socialNetwork.icon);
      this.uri.setValue(this.socialNetwork.uri);
      console.log(`--SOCIAL_NETWORK.EDIT-- get social network = ${JSON.stringify(this.socialNetwork)}`);
    });
  }

  update() {
    const socialNetwork = {
      name: this.name.value,
      icon: this.icon.value,
      uri: this.uri.value,
    };
    return this.socialNetworService.update(this.id, socialNetwork).subscribe((data: {}) => {
      console.log(`--SOCIAL_NETWORK.EDIT-- list social network = ${JSON.stringify(data)}`);
      this.router.navigate(['/social-network/index']);
    });
  }

}
