import { Component, OnInit } from '@angular/core';
import { SocialNetworkService } from '../social-network.service';

@Component({
  selector: 'app-list-social-network',
  templateUrl: './list-social-network.component.html',
  styleUrls: ['./list-social-network.component.sass']
})
export class ListSocialNetworkComponent implements OnInit {
  allSocialNetworks: any = [];

  constructor(private socialNetworkService: SocialNetworkService) {
    this.listSocialNetwork();
  }

  ngOnInit() {
  }

  // Get social network list
  listSocialNetwork() {
    return this.socialNetworkService.find().subscribe((data: {}) => {
      this.allSocialNetworks = data;
      console.log(`--SOCIAL_NETWORK.LIST-- list social networks = ${JSON.stringify(this.allSocialNetworks)}`);
    });
  }

  // Delete social network
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.socialNetworkService.delete(id).subscribe(data => {
        this.listSocialNetwork();
      });
    }
  }

}
