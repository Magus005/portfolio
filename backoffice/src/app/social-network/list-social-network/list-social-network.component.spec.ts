import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSocialNetworkComponent } from './list-social-network.component';

describe('ListSocialNetworkComponent', () => {
  let component: ListSocialNetworkComponent;
  let fixture: ComponentFixture<ListSocialNetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSocialNetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSocialNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
