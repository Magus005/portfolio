import { Routes } from '@angular/router';
import { ListLanguageComponent } from './list-language/list-language.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { EditLanguageComponent } from './edit-language/edit-language.component';
import { ReadLanguageComponent } from './read-language/read-language.component';

export const LanguageRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListLanguageComponent },
  { path: 'add', component: AddLanguageComponent },
  { path: 'edit/:id', component: EditLanguageComponent },
  { path: 'read/:id', component: ReadLanguageComponent },
];
