import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Language } from '../language';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_LANGUAGE_MUTATION = gql`mutation createLanguageMutation($language: InputLanguage!) {
  createLanguage(language: $language) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-add-language',
  templateUrl: './add-language.component.html',
  styleUrls: ['./add-language.component.sass']
})
export class AddLanguageComponent implements OnInit {
  language: Language;
  languageForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.languageForm = this.formBuilder.group({
      name: ['', Validators.required ],
      level: ['', Validators.required],
    });
  }

  submit() {
    const language = {
      name: this.data.name,
      level: `${this.data.level}`,
    };
    console.log(`--LANGUAGE.ADD-- object language ${JSON.stringify(language)}`);
    this.apollo.mutate({
      mutation: CREATE_LANGUAGE_MUTATION,
      variables: { language },
      optimisticResponse: {
        __typename: 'Mutation',
        createLanguage: {
          __typename: 'Language',
          language: language,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.LANGUAGE-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/language/index']);
      },
      err => { console.log(`--ADD.LANGUAGE-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/language/index']);
      }
    );
  }
}
