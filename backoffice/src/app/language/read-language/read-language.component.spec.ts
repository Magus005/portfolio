import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadLanguageComponent } from './read-language.component';

describe('ReadLanguageComponent', () => {
  let component: ReadLanguageComponent;
  let fixture: ComponentFixture<ReadLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
