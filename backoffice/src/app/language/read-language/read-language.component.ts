import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Language } from '../language';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';


const GET_LANGUAGE_BY_ID = gql`query getLanguageQuery($id: ID!) {
  getLanguage(id: $id) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-read-language',
  templateUrl: './read-language.component.html',
  styleUrls: ['./read-language.component.sass']
})
export class ReadLanguageComponent implements OnInit {
  language: Language;
  id: string;

  constructor(private route: ActivatedRoute, private apollo: Apollo) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--LANGUAGE.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getLanguageById();
  }

  getLanguageById() {
    console.log(`--LANGUAGE.READ-- Get all language `);
    this.apollo.watchQuery({
      query: GET_LANGUAGE_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--LANGUAGE.READ-- Get all language ${JSON.stringify(response)}`);
      this.language = response.data['getLanguage'];
      console.log(`--LANGUAGE.READ-- Get all language finally ${JSON.stringify(this.language)}`);
    });
  }

}
