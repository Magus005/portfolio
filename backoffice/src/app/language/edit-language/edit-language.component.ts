import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Language } from '../language';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_LANGUAGE_QUERY = gql`mutation updateLanguageMutation($id: ID!, $language: InputLanguage!) {
  updateLanguage(id: $id, language: $language) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

const GET_LANGUAGE_BY_ID = gql`query getLanguageQuery($id: ID!) {
  getLanguage(id: $id) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-edit-language',
  templateUrl: './edit-language.component.html',
  styleUrls: ['./edit-language.component.sass']
})
export class EditLanguageComponent implements OnInit {
  language: Language;
  languageForm: FormGroup;
  data: any = {};
  id: string;
  name = new FormControl('');
  level = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--LANGUAGE.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getLanguageById();
  }

  updateForm() {
    this.languageForm = this.formBuilder.group({
      name: ['', Validators.required ],
      level: ['', Validators.required],
    });
  }

  getLanguageById() {
    console.log(`--LANGUAGE.EDIT-- Get all language `);
    this.apollo.watchQuery({
      query: GET_LANGUAGE_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--LANGUAGE.EDIT-- Get all language ${JSON.stringify(response)}`);
      this.language = response.data['getLanguage'];
      this.name.setValue(this.language.name);
      this.level.setValue(this.language.level);
      console.log(`--LANGUAGE.EDIT-- Get all language finally ${JSON.stringify(this.language)}`);
    });
  }

  submit() {
    const name = this.name.value;
    const level = this.level.value;
    const language = {
      name,
      level,
      state: true
    };
    this.apollo.mutate({
      mutation: UPDATE_LANGUAGE_QUERY,
      variables: {
        id: this.id,
        language
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateLanguage: {
          __typename: 'Language',
          language: language,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.LANGUAGE-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/language/index']);
      },
      err => { console.log(`--ADD.LANGUAGE-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/language/index']);
      }
    );
  }
}
