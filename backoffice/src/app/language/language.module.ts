import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LanguageRoutes } from './language.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListLanguageComponent } from './list-language/list-language.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { ReadLanguageComponent } from './read-language/read-language.component';
import { EditLanguageComponent } from './edit-language/edit-language.component';

@NgModule({
  declarations: [
    ListLanguageComponent,
    AddLanguageComponent,
    ReadLanguageComponent,
    EditLanguageComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(LanguageRoutes),
    SharedModule
  ]
})
export class LanguageModule {}
