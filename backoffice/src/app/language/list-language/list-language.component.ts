import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Language } from '../language';
import { Router } from '@angular/router';


const GET_ALL_LANGUAGES_QUERY = gql`query getAllLanguagesQuery {
  getAllLanguages {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

const DELETE_LANGUAGE_QUERY = gql`mutation deleteLanguageQuery($id: ID!) {
  deleteLanguage(id: $id) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-list-language',
  templateUrl: './list-language.component.html',
  styleUrls: ['./list-language.component.sass']
})
export class ListLanguageComponent implements OnInit {
  allLanguages: Language[] = [];

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--LANGUAGE.LIST-- Get all languages `);
    this.apollo.watchQuery({
      query: GET_ALL_LANGUAGES_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--LANGUAGE.LIST-- Get all languages ${JSON.stringify(response)}`);
      this.allLanguages = response.data['getAllLanguages'];
      console.log(`--LANGUAGE.LIST-- Get all language finally ${JSON.stringify(this.allLanguages)}`);
    });
  }

  delete(id) {
    console.log(`--DELETE.LANGUAGE-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_LANGUAGE_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteLanguage: {
          __typename: 'Language',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--DELETE.LANGUAGE-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/language/index']);
      },
      err => { console.log(`--DELETE.LANGUAGE-- submit error ${err}`); },
      () => this.router.navigate(['/language/index'])
    );
  }
}
