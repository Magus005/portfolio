export class Language {
  _id: string;
  name: string;
  level: string;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
