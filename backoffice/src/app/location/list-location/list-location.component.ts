import { Component, OnInit } from '@angular/core';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-list-location',
  templateUrl: './list-location.component.html',
  styleUrls: ['./list-location.component.sass']
})
export class ListLocationComponent implements OnInit {
  allLocations: any = [];

  constructor(private locationService: LocationService) { }

  ngOnInit() {
    this.listLocations();
  }

  // Get locations list
  listLocations() {
    return this.locationService.find().subscribe((data: {}) => {
      this.allLocations = data;
      console.log(`--LOCATION.LIST-- list locations = ${JSON.stringify(this.allLocations)}`);
    });
  }

  // Delete location
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.locationService.delete(id).subscribe(data => {
        this.listLocations();
      });
    }
  }
}
