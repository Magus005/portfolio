import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  find(): Observable<Location> {
    return this.http.get<Location>(`${environment.servicePersonal.uri}location/`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<Location>(`${environment.servicePersonal.uri}location/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<Location>(`${environment.servicePersonal.uri}location/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API post() method => Create location
  create(location): Observable<Location> {
    return this.http.post<Location>(`${environment.servicePersonal.uri}location/`, JSON.stringify(location), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
