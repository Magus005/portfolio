import { Routes } from '@angular/router';
import { ListLocationComponent } from './list-location/list-location.component';
import { AddLocationComponent } from './add-location/add-location.component';

export const LocationRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListLocationComponent },
  { path: 'add', component: AddLocationComponent },
];
