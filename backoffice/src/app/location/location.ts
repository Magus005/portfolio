export class Location {
  id: string;
  latitude: string;
  longitude: string;
  zoom: string;
}
