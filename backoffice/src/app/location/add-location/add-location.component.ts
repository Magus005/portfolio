import { Component, OnInit } from '@angular/core';
import { LocationService } from '../location.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.sass']
})
export class AddLocationComponent implements OnInit {
  locationForm: FormGroup;
  data: any = {};

  constructor(private locationService: LocationService, private router: Router,
  private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.locationForm = this.formBuilder.group({
      latitude: ['', Validators.required ],
      longitude: ['', Validators.required],
      zoom: ['', Validators.required],
    });
  }

  submit() {
    const location = {
      latitude: this.data.latitude,
      longitude: this.data.longitude,
      zoom: this.data.zoom,
    };
    this.locationService.create(location).subscribe((data: {}) => {
      console.log(`--LOCATION.ADD-- object location = ${JSON.stringify(data)}`);
      this.router.navigate(['/location/index']);
    });
  }

}
