import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocationRoutes } from './location.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListLocationComponent } from './list-location/list-location.component';
import { AddLocationComponent } from './add-location/add-location.component';

@NgModule({
  declarations: [
    ListLocationComponent,
    AddLocationComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(LocationRoutes),
    SharedModule
  ]
})
export class LocationModule {}
