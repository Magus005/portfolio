import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MeService } from '../me.service';
import { Router } from '@angular/router';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-me',
  templateUrl: './add-me.component.html',
  styleUrls: ['./add-me.component.sass']
})
export class AddMeComponent implements OnInit {
  meForm: FormGroup;
  data: any = {};
  selectedPicture: FileList;
  selectedLogo: FileList;
  currentFileUpload: File;
  picture: string;
  logo: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private meService: MeService, private router: Router,
  private formBuilder: FormBuilder, private cd: ChangeDetectorRef) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.meForm = this.formBuilder.group({
      name: ['', Validators.required ],
      picture: ['', Validators.required],
      logo: ['', Validators.required],
      title: ['', Validators.required],
    });
  }

  selectPicture(event) {
    this.selectedPicture = event.target.files;
  }

  selectLogo(event) {
    this.selectedLogo = event.target.files;
  }
  upload() {
    this.uploadPicture();
  }
  uploadPicture() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedPicture.item(0);
    this.meService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.picture = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.picture}`);
        this.uploadLogo();
      }
    });

    this.selectedPicture = undefined;
  }

  uploadLogo() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedLogo.item(0);
    this.meService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.logo = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.logo}`);
        this.addMe();
      }
    });

    this.selectedLogo = undefined;
  }

  addMe() {
    const me = {
      name: this.data.name,
      picture: this.picture,
      logo: this.logo,
      title: this.data.title,
    };
    this.meService.create(me).subscribe((data: {}) => {
      console.log(`--ME.ADD-- object me = ${JSON.stringify(data)}`);
      this.router.navigate(['/me/index']);
    });
  }

}
