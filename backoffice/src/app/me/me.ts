export class Me {
  id: string;
  picture: string;
  name: string;
  logo: string;
  title: string;
  state: string;
}
