import { Routes } from '@angular/router';
import { AddMeComponent } from './add-me/add-me.component';
import { ListMeComponent } from './list-me/list-me.component';


export const MeRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'add', component: AddMeComponent },
  { path: 'index', component: ListMeComponent },
];
