import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MeRoutes } from './me.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AddMeComponent } from './add-me/add-me.component';
import { ListMeComponent } from './list-me/list-me.component';

@NgModule({
  declarations: [
    AddMeComponent,
    ListMeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(MeRoutes),
    SharedModule
  ]
})
export class MeModule {}
