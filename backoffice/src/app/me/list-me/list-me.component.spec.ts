import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMeComponent } from './list-me.component';

describe('ListMeComponent', () => {
  let component: ListMeComponent;
  let fixture: ComponentFixture<ListMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
