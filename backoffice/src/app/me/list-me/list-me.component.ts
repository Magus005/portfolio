import { Component, OnInit } from '@angular/core';
import { MeService } from '../me.service';

@Component({
  selector: 'app-list-me',
  templateUrl: './list-me.component.html',
  styleUrls: ['./list-me.component.sass']
})
export class ListMeComponent implements OnInit {
  allMe: any = [];

  constructor(private meService: MeService) { }

  ngOnInit() {
    this.listMe();
  }

  // Get me list
  listMe() {
    return this.meService.find().subscribe((data: {}) => {
      this.allMe = data;
      console.log(`--ME.LIST-- list me = ${JSON.stringify(this.allMe)}`);
    });
  }

  // Delete me
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.meService.delete(id).subscribe(data => {
        this.listMe();
      });
    }
  }

}
