import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

export const DashboardRoutes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'index'
  },
  {
    path: 'index',
    component: DashboardComponent
  }
];
