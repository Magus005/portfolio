import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PortfolioService } from '../portfolio.service';
import { Router } from '@angular/router';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-portfolio',
  templateUrl: './add-portfolio.component.html',
  styleUrls: ['./add-portfolio.component.sass']
})
export class AddPortfolioComponent implements OnInit {
  portfolioForm: FormGroup;
  data: any = {};
  selectedPicture: FileList;
  currentFileUpload: File;
  uriPicture: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private portfolioService: PortfolioService, private router: Router,
  private formBuilder: FormBuilder, private cd: ChangeDetectorRef) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.portfolioForm = this.formBuilder.group({
      title: ['', Validators.required ],
      customer: ['', Validators.required],
      content: ['', Validators.required],
      delivered: ['', Validators.required],
    });
  }

  selectPicture(event) {
    this.selectedPicture = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedPicture.item(0);
    this.portfolioService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
        console.log(`File is completely uploaded! pourcentage : ${this.progress.percentage}`);
      } else if (event instanceof HttpResponse) {
        console.log(`File is completely uploaded! error : ${JSON.stringify(event)}`);
        this.uriPicture = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriPicture}`);
        this.addPortfolio();
      }
    });

    this.selectedPicture = undefined;
  }

  addPortfolio() {
    const portfolio = {
      title: this.data.title,
      picture: this.uriPicture,
      customer: this.data.customer,
      delivered: this.data.delivered,
      content: this.data.content,
    };
    this.portfolioService.create(portfolio).subscribe((data: {}) => {
      console.log(`--ME.ADD-- object portfolio = ${JSON.stringify(data)}`);
      this.router.navigate(['/portfolio/index']);
    });
  }


}
