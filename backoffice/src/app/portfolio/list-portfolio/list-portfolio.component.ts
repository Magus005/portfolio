import { Component, OnInit } from '@angular/core';
import { PortfolioService } from '../portfolio.service';

@Component({
  selector: 'app-list-portfolio',
  templateUrl: './list-portfolio.component.html',
  styleUrls: ['./list-portfolio.component.sass']
})
export class ListPortfolioComponent implements OnInit {
  allPortfolio: any = [];

  constructor(private portfolioService: PortfolioService) { }

  ngOnInit() {
    this.listPortfolio();
  }

  // Get portfolio list
  listPortfolio() {
    return this.portfolioService.find().subscribe((data: {}) => {
      this.allPortfolio = data;
      console.log(`--PORTFOLIO.LIST-- list portfolio = ${JSON.stringify(this.allPortfolio)}`);
    });
  }

  // Delete me
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.portfolioService.delete(id).subscribe(data => {
        this.listPortfolio();
      });
    }
  }
}
