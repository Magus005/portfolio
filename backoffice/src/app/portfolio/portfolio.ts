export class Portfolio {
  id: string;
  title: string;
  picture: string;
  customer: string;
  delivered: Date;
  content: string;
  state: boolean;
}
