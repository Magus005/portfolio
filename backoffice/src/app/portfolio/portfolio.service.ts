import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { HttpRequest, HttpEvent, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Portfolio } from './portfolio';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create portfolio
  create(portfolio): Observable<Portfolio> {
    return this.http.post<Portfolio>(`${environment.servicePersonal.uri}portfolio/`, JSON.stringify(portfolio), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  find(): Observable<Portfolio> {
    return this.http.get<Portfolio>(`${environment.servicePersonal.uri}portfolio/`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<Portfolio>(`${environment.servicePersonal.uri}portfolio/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(id, portfolio) {
    return this.http.put<Portfolio>(`${environment.servicePersonal.uri}portfolio/${id}`, JSON.stringify(portfolio), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<Portfolio>(`${environment.servicePersonal.uri}portfolio/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', `${environment.servicePersonal.uri}api/files`, formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
