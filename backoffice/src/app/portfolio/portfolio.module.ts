import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PortfolioRoutes } from './portfolio.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListPortfolioComponent } from './list-portfolio/list-portfolio.component';
import { AddPortfolioComponent } from './add-portfolio/add-portfolio.component';

@NgModule({
  declarations: [
    ListPortfolioComponent,
    AddPortfolioComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(PortfolioRoutes),
    SharedModule
  ]
})
export class PortfolioModule {}
