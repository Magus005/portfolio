import { Routes } from '@angular/router';
import { ListPortfolioComponent } from './list-portfolio/list-portfolio.component';
import { AddPortfolioComponent } from './add-portfolio/add-portfolio.component';


export const PortfolioRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListPortfolioComponent },
  { path: 'add', component: AddPortfolioComponent },
];
