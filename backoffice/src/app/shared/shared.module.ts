import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from '../../components/footer/footer.component';
import { HeaderComponent } from '../../components/header/header.component';
import { LoadingComponent } from '../../components/loading/loading.component';
import { AsideComponent } from '../../components/aside/aside.component';
import { SideBarEmailComponent } from '../../components/side-bar-email/side-bar-email.component';
import { HeaderEmailComponent } from '../../components/header-email/header-email.component';
import { SearchFilterPipe } from '../../components/filter/search-filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  exports: [
      CommonModule,
      FooterComponent,
      HeaderComponent,
      LoadingComponent,
      AsideComponent,
      SideBarEmailComponent,
      HeaderEmailComponent,
      SearchFilterPipe,
  ],
  imports: [
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    LoadingComponent,
    AsideComponent,
    SideBarEmailComponent,
    HeaderEmailComponent,
    SearchFilterPipe,
  ]
})
export class SharedModule { }
