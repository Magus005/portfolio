import { Routes } from '@angular/router';
import { ListUserComponent } from './list-user/list-user.component';
import { AddUserComponent } from './add-user/add-user.component';


export const UserRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListUserComponent },
  { path: 'add', component: AddUserComponent },
];
