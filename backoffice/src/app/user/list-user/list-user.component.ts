import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.sass']
})
export class ListUserComponent implements OnInit {
  allUsers: any = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.listUsers();
  }

  // Get user list
  listUsers() {
    return this.userService.find().subscribe((data: {}) => {
      this.allUsers = data;
      console.log(`--USER.LIST-- list users = ${JSON.stringify(this.allUsers)}`);
    });
  }

  // Delete User
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.userService.delete(id).subscribe(data => {
        this.listUsers();
      });
    }
  }

}
