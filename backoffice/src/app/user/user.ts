export class User {
  id: string;
  name: string;
  password: string;
  email: string;
  role: string;
  state: boolean;
}
