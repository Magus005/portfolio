import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.sass']
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  data: any = {};

  constructor(private userService: UserService, private router: Router,
  private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required ],
      password: ['', Validators.required],
      email: ['', Validators.required],
      role: ['', Validators.required],
    });
  }

  submit() {
    const user = {
      name: this.data.name,
      password: this.data.password,
      email: this.data.email,
      role: this.data.role,
    };
    this.userService.create(user).subscribe((data: {}) => {
      console.log(`--USER.ADD-- object user = ${JSON.stringify(data)}`);
      this.router.navigate(['/user/index']);
    });
  }
}
