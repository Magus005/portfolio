import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Faq } from './faq';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create faq
  create(faq): Observable<Faq> {
    return this.http.post<Faq>(`${environment.serviceNetwork.uri}faq`, JSON.stringify(faq), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  find(): Observable<Faq> {
    return this.http.get<Faq>(`${environment.serviceNetwork.uri}faq`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<Faq>(`${environment.serviceNetwork.uri}faq/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(id, faq) {
    return this.http.put<Faq>(`${environment.serviceNetwork.uri}faq/${id}`, JSON.stringify(faq), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<Faq>(`${environment.serviceNetwork.uri}faq/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
