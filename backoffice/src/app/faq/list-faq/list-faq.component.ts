import { Component, OnInit } from '@angular/core';
import { FaqService } from '../faq.service';

@Component({
  selector: 'app-list-faq',
  templateUrl: './list-faq.component.html',
  styleUrls: ['./list-faq.component.sass']
})
export class ListFaqComponent implements OnInit {
  allFaqs: any = [];

  constructor(private faqService: FaqService) { }

  ngOnInit() {
    this.listFaq();
  }

  // Get faq list
  listFaq() {
    return this.faqService.find().subscribe((data: {}) => {
      this.allFaqs = data;
      console.log(`--FAQ.LIST-- list faqs = ${JSON.stringify(this.allFaqs)}`);
    });
  }

  // Delete Faq
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.faqService.delete(id).subscribe(data => {
        this.listFaq();
      });
    }
  }

}
