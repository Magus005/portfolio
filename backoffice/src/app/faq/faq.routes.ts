import { Routes } from '@angular/router';
import { ListFaqComponent } from './list-faq/list-faq.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { EditFaqComponent } from './edit-faq/edit-faq.component';

export const FaqRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListFaqComponent },
  { path: 'add', component: AddFaqComponent },
  { path: 'edit/:id', component: EditFaqComponent },
];
