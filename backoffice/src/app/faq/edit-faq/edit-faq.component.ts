import { Component, OnInit } from '@angular/core';
import { FaqService } from '../faq.service';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { Faq } from '../faq';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-faq',
  templateUrl: './edit-faq.component.html',
  styleUrls: ['./edit-faq.component.sass']
})
export class EditFaqComponent implements OnInit {
  faq: any;
  faqForm: FormGroup;
  id: string;
  name = new FormControl('');
  content = new FormControl('');

  constructor(private faqService: FaqService, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private router: Router) {
      this.updateForm();
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--FAQ.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getFaq();
  }

  updateForm() {
    this.faqForm = this.formBuilder.group({
      name: ['', Validators.required ],
      content: ['', Validators.required],
    });
  }

  // Get faq list
  getFaq() {
    return this.faqService.findOne(this.id).subscribe((data: {}) => {
      this.faq = data;
      this.name.setValue(this.faq.name);
      this.content.setValue(this.faq.content);
      console.log(`--FAQ.EDIT-- get faq = ${JSON.stringify(this.faq)}`);
    });
  }

  update() {
    const faq = {
      name: this.name.value,
      content: this.content.value
    };
    return this.faqService.update(this.id, faq).subscribe((data: {}) => {
      console.log(`--FAQ.EDIT-- list faq = ${JSON.stringify(data)}`);
      this.router.navigate(['/faq/index']);
    });
  }
}
