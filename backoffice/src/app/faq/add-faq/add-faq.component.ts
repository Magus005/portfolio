import { Component, OnInit } from '@angular/core';
import { FaqService } from '../faq.service';
import { Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.sass']
})
export class AddFaqComponent implements OnInit {
  faqForm: FormGroup;
  data: any = {};

  constructor(private faqService: FaqService, private router: Router,
  private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.faqForm = this.formBuilder.group({
      name: ['', Validators.required ],
      content: ['', Validators.required],
    });
  }

  submit() {
    const faq = {
      name: this.data.name,
      content: this.data.content,
    };
    this.faqService.create(faq).subscribe((data: {}) => {
      console.log(`--FAQ.ADD-- object faq = ${JSON.stringify(data)}`);
      this.router.navigate(['/faq/index']);
    });
  }
}
