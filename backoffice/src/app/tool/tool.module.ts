import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToolRoutes } from './tool.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListToolComponent } from './list-tool/list-tool.component';
import { AddToolComponent } from './add-tool/add-tool.component';
import { EditToolComponent } from './edit-tool/edit-tool.component';

@NgModule({
  declarations: [
    ListToolComponent,
    AddToolComponent,
    EditToolComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(ToolRoutes),
    SharedModule
  ]
})
export class ToolModule {}
