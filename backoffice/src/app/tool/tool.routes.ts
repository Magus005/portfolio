import { Routes } from '@angular/router';
import { ListToolComponent } from './list-tool/list-tool.component';
import { AddToolComponent } from './add-tool/add-tool.component';
import { EditToolComponent } from './edit-tool/edit-tool.component';

export const ToolRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListToolComponent },
  { path: 'add', component: AddToolComponent },
  { path: 'edit/:id', component: EditToolComponent },
];
