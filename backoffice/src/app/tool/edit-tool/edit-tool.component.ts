import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Tool } from '../tool';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_TOOL_QUERY = gql`mutation updateToolMutation($id: ID!, $tool: InputTool!) {
  updateTool(id: $id, tool: $tool) {
    _id,
    name,
    style,
    state,
    createdAt,
    updatedAt,
  }
}`;

const GET_TOOL_BY_ID = gql`query getToolQuery($id: ID!) {
  getTool(id: $id) {
    _id,
    name,
    style,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-edit-tool',
  templateUrl: './edit-tool.component.html',
  styleUrls: ['./edit-tool.component.sass']
})
export class EditToolComponent implements OnInit {
  tool: Tool;
  toolForm: FormGroup;
  data: any = {};
  id: string;
  name = new FormControl('');
  style = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--TOOL.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getToolById();
  }

  updateForm() {
    this.toolForm = this.formBuilder.group({
      name: ['', Validators.required ],
      style: ['', Validators.required],
    });
  }

  getToolById() {
    console.log(`--TOOL.EDIT-- Get all tool `);
    this.apollo.watchQuery({
      query: GET_TOOL_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--TOOL.EDIT-- Get all tool ${JSON.stringify(response)}`);
      this.tool = response.data['getTool'];
      this.name.setValue(this.tool.name);
      this.style.setValue(this.tool.style);
      console.log(`--TOOL.EDIT-- Get all tool finally ${JSON.stringify(this.tool)}`);
    });
  }

  submit() {
    const name = this.name.value;
    const style = this.data.style;
    const tool = {
      name,
      style,
      state: true
    };
    this.apollo.mutate({
      mutation: UPDATE_TOOL_QUERY,
      variables: {
        id: this.id,
        tool
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateTool: {
          __typename: 'Tool',
          tool: tool,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.TOOL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/tool/index']);
      },
      err => { console.log(`--ADD.TOOL-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/tool/index']);
      }
    );
  }
}
