export class Tool {
  _id: string;
  name: string;
  style: string;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
