import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Tool } from '../tool';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_TOOL_MUTATION = gql`mutation createToolMutation($tool: InputTool!) {
  createTool(tool: $tool) {
    _id,
    name,
    style,
    state,
    createdAt,
    updatedAt,
  }
}`;


@Component({
  selector: 'app-add-tool',
  templateUrl: './add-tool.component.html',
  styleUrls: ['./add-tool.component.sass']
})
export class AddToolComponent implements OnInit {
  tool: Tool;
  toolForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.toolForm = this.formBuilder.group({
      name: ['', Validators.required ],
      style: ['', Validators.required],
    });
  }

  submit() {
    const tool = {
      name: this.data.name,
      style: this.data.style,
    };
    this.apollo.mutate({
      mutation: CREATE_TOOL_MUTATION,
      variables: { tool },
      optimisticResponse: {
        __typename: 'Mutation',
        createTool: {
          __typename: 'Tool',
          tool: tool,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.TOOL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/tool/index']);
      },
      err => { console.log(`--ADD.TOOL-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/tool/index']);
      }
    );
  }
}
