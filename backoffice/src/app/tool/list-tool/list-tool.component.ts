import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Tool } from '../tool';
import { Router } from '@angular/router';


const GET_ALL_TOOLS_QUERY = gql`query getAllToolsQuery {
  getAllTools {
    _id,
    name,
    style,
    state,
    createdAt,
    updatedAt,
  }
}`;

const DELETE_TOOL_QUERY = gql`mutation deleteToolMutation($id: ID!) {
  deleteTool(id: $id) {
    _id,
    name,
    style,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-list-tool',
  templateUrl: './list-tool.component.html',
  styleUrls: ['./list-tool.component.sass']
})
export class ListToolComponent implements OnInit {
  allTools: Tool[] = [];

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--TOOL.LIST-- Get all tools `);
    this.apollo.watchQuery({
      query: GET_ALL_TOOLS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--TOOL.LIST-- Get all tools ${JSON.stringify(response)}`);
      this.allTools = response.data['getAllTools'];
      console.log(`--TOOL.LIST-- Get all tool finally ${JSON.stringify(this.allTools)}`);
    });
  }

  delete(id) {
    console.log(`--DELETE.TOOL-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_TOOL_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteTool: {
          __typename: 'Tool',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--DELETE.TOOL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/tool/index']);
      },
      err => { console.log(`--DELETE.TOOL-- submit error ${err}`); },
      () => this.router.navigate(['/tool/index'])
    );
  }
}
