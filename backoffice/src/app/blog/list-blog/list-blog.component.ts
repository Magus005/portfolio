import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Blog } from '../blog';
import { Router } from '@angular/router';


const GET_ALL_POSTS_QUERY = gql`query getAllpostsQuery {
  getAllposts {
    _id
    picture
    movie
    content
    title
    userId
    dateCreated
    categories
    state
    createdAt
  }
}`;

const DELETE_POST_MUTATION = gql`mutation deletePostMutation($id: ID!) {
  deletePost(id: $id) {
    _id
  }
}`;


@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.sass']
})
export class ListBlogComponent implements OnInit {
  allBlog: Blog[] = [];
  blog: Blog;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    this.listBlog();
  }

  listBlog() {
    console.log(`--BLOG.LIST-- Get all blog `);
    this.apollo.watchQuery({
      query: GET_ALL_POSTS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--BLOG.LIST-- Get all blog ${JSON.stringify(response)}`);
      this.allBlog = response.data['getAllposts'];
      console.log(`--BLOG.LIST-- Get all blog finally ${JSON.stringify(this.allBlog)}`);
    });
  }
  delete(id) {
    console.log(`--BLOG.LIST-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_POST_MUTATION,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deletePost: {
          __typename: 'Post',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--BLOG.DELETE-- submit response ${JSON.stringify(res)}`);
        this.listBlog();
      },
      err => { console.log(`--BLOG.DELETE-- submit error ${err}`); },
      () => this.listBlog()
    );
  }


}
