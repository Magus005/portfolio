import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Blog } from '../blog';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { BlogService } from '../blog.service';
import { environment } from '../../../environments/environment';
import { Category } from '../../category/category';
import { UserService } from '../../user/user.service';
import { User } from '../../user/user';

const CREATE_POST_MUTATION = gql`mutation createPostMutation($post: InputPost) {
  createPost(post: $post) {
    _id
  }
}`;

const GET_ALL_CATEGORIES_QUERY = gql`query getCategoriesQuery {
  getCategories {
    _id,
    name,
  }
}`;

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.sass']
})
export class AddBlogComponent implements OnInit {
  blog: Blog;
  allCategories: Category[] = [];
  allUsers: any;
  blogForm: FormGroup;
  data: any = {};
  selectedPicture: FileList;
  selectedMovie: FileList;
  currentFileUpload: File;
  uriPicture: string;
  uriMovie: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
  private router: Router, private userService: UserService,
  private blogService: BlogService) {
    this.createForm();
  }

  ngOnInit() {
    this.listCategories();
    this.listUsers();
  }

  listCategories() {
    console.log(`--CATEGORY.LIST-- Get all categories `);
    this.apollo.watchQuery({
      query: GET_ALL_CATEGORIES_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--CATEGORY.LIST-- Get all categories ${JSON.stringify(response)}`);
      this.allCategories = response.data['getCategories'];
      console.log(`--CATEGORY.LIST-- Get all categories finally ${JSON.stringify(this.allCategories)}`);
    });
  }

  listUsers() {
    return this.userService.find().subscribe((data: {}) => {
      this.allUsers = data;
      console.log(`--USER.LIST-- list users = ${JSON.stringify(this.allUsers)}`);
    });
  }

  createForm() {
    this.blogForm = this.formBuilder.group({
      title: ['', Validators.required ],
      content: ['', Validators.required],
      dateCreated: ['', Validators.required],
      categories: ['', Validators.required],
      userId: ['', Validators.required],
    });
  }

  submit() {
    const categories = [{
      name: this.data.categories,
    }];
    const post = {
      title: this.data.title,
      content: this.data.content,
      dateCreated: this.data.dateCreated,
      categories: categories,
      userId: this.data.userId,
      picture: this.uriPicture,
      movie: this.uriMovie,
      state: true
    };
    this.apollo.mutate({
      mutation: CREATE_POST_MUTATION,
      variables: { post },
      optimisticResponse: {
        __typename: 'Mutation',
        createPost: {
          __typename: 'Post',
          post: post,
        },
      },
    }).subscribe(
      res => {
        console.log(`--BLOG.ADD-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/blog/index']);
      },
      err => { console.log(`--BLOG.ADD-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/blog/index']);
      }
    );
  }

  selectPicture(event) {
    this.selectedPicture = event.target.files;
  }

  selectMovie(event) {
    this.selectedMovie = event.target.files;
  }

  upload() {
    if (this.selectedPicture) {
      this.uploadPicture();
    } else {
      this.uploadMovie();
    }
  }

  uploadPicture() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedPicture.item(0);
    this.blogService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.uriPicture = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriPicture}`);
        if (this.selectedMovie) {
          this.uploadMovie();
        } else {
          this.submit();
        }
      }
    });

    this.selectedPicture = undefined;
  }

  uploadMovie() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedMovie.item(0);
    this.blogService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.uriMovie = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriMovie}`);
        this.submit();
      }
    });

    this.selectedMovie = undefined;
  }

}
