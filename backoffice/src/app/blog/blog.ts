export class Blog {
  _id: string;
  picture: string;
  movie: string;
  content: string;
  title: string;
  userId: string;
  dateCreated: string;
  categories: [string];
  state: boolean;
}
