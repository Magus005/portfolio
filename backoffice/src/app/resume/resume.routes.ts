import { Routes } from '@angular/router';
import { ListResumeComponent } from './list-resume/list-resume.component';
import { AddResumeComponent } from './add-resume/add-resume.component';


export const ResumeRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListResumeComponent },
  { path: 'add', component: AddResumeComponent },
];
