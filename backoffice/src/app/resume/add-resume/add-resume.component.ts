import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResumeService } from '../resume.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-resume',
  templateUrl: './add-resume.component.html',
  styleUrls: ['./add-resume.component.sass']
})
export class AddResumeComponent implements OnInit {
  resumeForm: FormGroup;
  data: any = {};
  selectedResume: FileList;
  currentFileUpload: File;
  uriResume: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private resumeService: ResumeService, private router: Router,
  private formBuilder: FormBuilder, private cd: ChangeDetectorRef) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.resumeForm = this.formBuilder.group({
      name: ['', Validators.required ],
      path: ['', Validators.required],
    });
  }

  selectResume(event) {
    this.selectedResume = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedResume.item(0);
    this.resumeService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
        console.log(`File is completely uploaded! pourcentage : ${this.progress.percentage}`);
      } else if (event instanceof HttpResponse) {
        console.log(`File is completely uploaded! error : ${JSON.stringify(event)}`);
        this.uriResume = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriResume}`);
        this.addResume();
      }
    });

    this.selectedResume = undefined;
  }

  addResume() {
    const resume = {
      name: this.data.name,
      path: this.uriResume,
    };
    console.log(`--ME.ADD-- object resume = ${JSON.stringify(resume)}`);
    this.resumeService.create(resume).subscribe((data: {}) => {
      console.log(`--ME.ADD-- object resume = ${JSON.stringify(data)}`);
      this.router.navigate(['/resume/index']);
    });
  }


}
