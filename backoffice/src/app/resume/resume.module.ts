import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResumeRoutes } from './resume.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListResumeComponent } from './list-resume/list-resume.component';
import { AddResumeComponent } from './add-resume/add-resume.component';

@NgModule({
  declarations: [
    ListResumeComponent,
    AddResumeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(ResumeRoutes),
    SharedModule
  ]
})
export class ResumeModule {}
