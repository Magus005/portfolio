export class Resume {
  id: string;
  path: string;
  name: string;
  state: boolean;
}
