import { Component, OnInit } from '@angular/core';
import { ResumeService } from '../resume.service';

@Component({
  selector: 'app-list-resume',
  templateUrl: './list-resume.component.html',
  styleUrls: ['./list-resume.component.sass']
})
export class ListResumeComponent implements OnInit {
  allResume: any = [];

  constructor(private resumeService: ResumeService) { }

  ngOnInit() {
    this.listResume();
  }

  // Get resume list
  listResume() {
    return this.resumeService.find().subscribe((data: {}) => {
      this.allResume = data;
      console.log(`--RESUME.LIST-- list resume = ${JSON.stringify(this.allResume)}`);
    });
  }

  // Delete me
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.resumeService.delete(id).subscribe(data => {
        this.listResume();
      });
    }
  }
}
