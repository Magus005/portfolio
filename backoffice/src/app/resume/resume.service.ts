import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { Resume } from './resume';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // HttpClient API post() method => Create resume
  create(resume): Observable<Resume> {
    return this.http.post<Resume>(`${environment.servicePersonal.uri}cv/`, JSON.stringify(resume), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  find(): Observable<Resume> {
    return this.http.get<Resume>(`${environment.servicePersonal.uri}cv/`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  findOne(id) {
    return this.http.get<Resume>(`${environment.servicePersonal.uri}cv/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(id, resume) {
    return this.http.put<Resume>(`${environment.servicePersonal.uri}cv/${id}`, JSON.stringify(resume), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete<Resume>(`${environment.servicePersonal.uri}cv/${id}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', `${environment.servicePersonal.uri}api/files`, formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
