export class Experience {
  _id: string;
  title: string;
  logoEnterprise: string;
  nameEnterprise: string;
  content: string;
  startDate: Date;
  endDate: Date;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
