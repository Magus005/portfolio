import { Routes } from '@angular/router';
import { ListExperienceComponent } from './list-experience/list-experience.component';
import { AddExperienceComponent } from './add-experience/add-experience.component';

export const ExperienceRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListExperienceComponent },
  { path: 'add', component: AddExperienceComponent },
];
