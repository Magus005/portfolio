import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Experience } from '../experience';
import { Router } from '@angular/router';


const GET_ALL_EXPERIENCES_QUERY = gql`query getAllExperiencesQuery {
  getAllExperiences {
    _id,
    title,
    logoEnterprise,
    nameEnterprise,
    content,
    startDate,
    endDate,
    state,
    createdAt,
    updatedAt,
  }
}`;

const DELETE_EXPERIENCE_QUERY = gql`mutation deleteExperienceQuery($id: ID!) {
  deleteExperience(id: $id) {
    _id,
    title,
    logoEnterprise,
    nameEnterprise,
    content,
    startDate,
    endDate,
    state,
    createdAt,
    updatedAt,
  }
}`;


@Component({
  selector: 'app-list-experience',
  templateUrl: './list-experience.component.html',
  styleUrls: ['./list-experience.component.sass']
})
export class ListExperienceComponent implements OnInit {
  allExperiences: Experience[] = [];
  experience: Experience;

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--EXPERIENCE.LIST-- Get all experience `);
    this.apollo.watchQuery({
      query: GET_ALL_EXPERIENCES_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--EXPERIENCE.LIST-- Get all experience ${JSON.stringify(response)}`);
      this.allExperiences = response.data['getAllExperiences'];
      console.log(`--EXPERIENCE.LIST-- Get all experience finally ${JSON.stringify(this.allExperiences)}`);
    });
  }

  delete(id) {
    console.log(`--LIST.EXPERIENCE-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_EXPERIENCE_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteExperience: {
          __typename: 'Experience',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--DELETE.EXPERIENCE-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/experience/index']);
      },
      err => { console.log(`--DELETE.EXPERIENCE-- submit error ${err}`); },
      () => this.router.navigate(['/experience/index'])
    );
  }

}
