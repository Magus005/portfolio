import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Experience } from '../experience';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { ExperienceService } from '../experience.service';
import { environment } from '../../../environments/environment';

const CREATE_EXPERIENCE_QUERY = gql`mutation createExperienceQuery($experience: InputExperience!) {
  createExperience(experience: $experience) {
    _id,
    title,
    logoEnterprise,
    nameEnterprise,
    content,
    startDate,
    endDate,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-add-experience',
  templateUrl: './add-experience.component.html',
  styleUrls: ['./add-experience.component.sass']
})
export class AddExperienceComponent implements OnInit {
  experience: Experience;
  experienceForm: FormGroup;
  data: any = {};
  selectedFiles: FileList;
  currentFileUpload: File;
  uriFile: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
    private router: Router, private cd: ChangeDetectorRef,
    private experienceService: ExperienceService) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.experienceForm = this.formBuilder.group({
      title: ['', Validators.required ],
      logoEnterprise: ['', Validators.required],
      nameEnterprise: ['', Validators.required],
      content: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });
  }

  submit() {
    const experience = {
      title: this.data.title,
      logoEnterprise: this.uriFile,
      nameEnterprise: this.data.nameEnterprise,
      content: this.data.content,
      startDate: this.data.startDate,
      endDate: this.data.endDate,
      state: true
    };
    this.apollo.mutate({
      mutation: CREATE_EXPERIENCE_QUERY,
      variables: { experience },
      optimisticResponse: {
        __typename: 'Mutation',
        createExperience: {
          __typename: 'Experience',
          experience: experience,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.EXPERIENCE-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/experience/index']);
      },
      err => { console.log(`--ADD.EXPERIENCE-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/experience/index']);
      }
    );
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.experienceService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.uriFile = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriFile}`);
        this.submit();
      }
    });

    this.selectedFiles = undefined;
  }
}
