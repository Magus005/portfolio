import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SkillRoutes } from './skill.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { ListSkillComponent } from './list-skill/list-skill.component';
import { EditSkillComponent } from './edit-skill/edit-skill.component';
import { ReadSkillComponent } from './read-skill/read-skill.component';
import { AddSkillComponent } from './add-skill/add-skill.component';

@NgModule({
  declarations: [
    ListSkillComponent,
    EditSkillComponent,
    ReadSkillComponent,
    AddSkillComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(SkillRoutes),
    SharedModule
  ]
})
export class SkillModule {}
