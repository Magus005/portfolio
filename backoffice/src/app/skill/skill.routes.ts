import { Routes } from '@angular/router';
import { ListSkillComponent } from './list-skill/list-skill.component';
import { EditSkillComponent } from './edit-skill/edit-skill.component';
import { ReadSkillComponent } from './read-skill/read-skill.component';
import { AddSkillComponent } from './add-skill/add-skill.component';

export const SkillRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListSkillComponent },
  { path: 'add', component: AddSkillComponent },
  { path: 'read/:id', component: ReadSkillComponent },
  { path: 'edit/:id', component: EditSkillComponent },
];
