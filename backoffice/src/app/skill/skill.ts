export class Skill {
  _id: string;
  name: string;
  level: string;
  type: string;
  state: boolean;
  createdAt: Date;
  updatedAt: Date;
}
