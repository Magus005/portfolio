import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadSkillComponent } from './read-skill.component';

describe('ReadSkillComponent', () => {
  let component: ReadSkillComponent;
  let fixture: ComponentFixture<ReadSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
