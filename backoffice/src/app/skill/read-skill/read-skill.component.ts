import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Skill } from '../skill';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const GET_SKILL_BY_ID = gql`query getSkillQuery($id: ID!) {
  getSkill(id: $id) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-read-skill',
  templateUrl: './read-skill.component.html',
  styleUrls: ['./read-skill.component.sass']
})
export class ReadSkillComponent implements OnInit {
  skill: Skill;
  id: string;

  constructor(private route: ActivatedRoute, private apollo: Apollo) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--SKILL.READ-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getSkillById();
  }

  getSkillById() {
    console.log(`--SKILL.READ-- Get all skill `);
    this.apollo.watchQuery({
      query: GET_SKILL_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--SKILL.READ-- Get all skill ${JSON.stringify(response)}`);
      this.skill = response.data['getSkill'];
      console.log(`--SKILL.READ-- Get all skill finally ${JSON.stringify(this.skill)}`);
    });
  }

}
