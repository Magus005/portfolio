import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Skill } from '../skill';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router';


const UPDATE_Skill_QUERY = gql`mutation updateSkillMutation($id: ID!, $skill: InputSkill!) {
  updateSkill(id: $id, skill: $skill) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

const GET_SKILL_BY_ID = gql`query getSkillQuery($id: ID!) {
  getSkill(id: $id) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;


@Component({
  selector: 'app-edit-skill',
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.sass']
})
export class EditSkillComponent implements OnInit {
  skill: Skill;
  skillForm: FormGroup;
  data: any = {};
  id: string;
  name = new FormControl('');
  level = new FormControl('');
  type = new FormControl('');

  constructor(private apollo: Apollo, private formBuilder: FormBuilder,
    private route: ActivatedRoute, private router: Router) {
    this.updateForm();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(`--SKILL.EDIT-- params id ${params.id}`);
      this.id = params.id;
    });
    this.getSkillById();
  }

  updateForm() {
    this.skillForm = this.formBuilder.group({
      name: ['', Validators.required ],
      level: ['', Validators.required],
      type: ['', Validators.required],
    });
  }

  getSkillById() {
    console.log(`--SKILL.EDIT-- Get all skill `);
    this.apollo.watchQuery({
      query: GET_SKILL_BY_ID,
      variables: {id: this.id}
    }).valueChanges.subscribe((response) => {
      console.log(`--SKILL.EDIT-- Get all skill ${JSON.stringify(response)}`);
      this.skill = response.data['getSkill'];
      this.name.setValue(this.skill.name);
      this.level.setValue(this.skill.level);
      this.type.setValue(this.skill.type);
      console.log(`--SKILL.EDIT-- Get all skill finally ${JSON.stringify(this.skill)}`);
    });
  }

  submit() {
    const name = this.name.value;
    const level = this.level.value;
    const type = this.data.type;
    const skill = {
      name,
      level,
      type,
      state: true
    };
    this.apollo.mutate({
      mutation: UPDATE_Skill_QUERY,
      variables: {
        id: this.id,
        skill
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateSkill: {
          __typename: 'Skill',
          skill: skill,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.SKILL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/skill/index']);
      },
      err => { console.log(`--ADD.SKILL-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/skill/index']);
      }
    );
  }
}
