import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Skill } from '../skill';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';

const CREATE_SKILL_MUTATION = gql`mutation createSkillMutation($skill: InputSkill!) {
  createSkill(skill: $skill) {
    _id,
    name,
    level,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.sass']
})
export class AddSkillComponent implements OnInit {
  skill: Skill;
  skillForm: FormGroup;
  data: any = {};

  constructor(private apollo: Apollo, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.skillForm = this.formBuilder.group({
      name: ['', Validators.required ],
      level: ['', Validators.required],
      type: ['', Validators.required],
    });
  }

  submit() {
    const skill = {
      name: this.data.name,
      level: `${this.data.level}`,
      type: this.data.type,
    };
    console.log(`--SKILL.ADD-- object skill ${JSON.stringify(skill)}`);
    this.apollo.mutate({
      mutation: CREATE_SKILL_MUTATION,
      variables: { skill },
      optimisticResponse: {
        __typename: 'Mutation',
        createSkill: {
          __typename: 'Skill',
          skill: skill,
        },
      },
    }).subscribe(
      res => {
        console.log(`--ADD.SKILL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/skill/index']);
      },
      err => { console.log(`--ADD.SKILL-- submit error ${err}`); },
      () => {
        return this.router.navigate(['/skill/index']);
      }
    );
  }
}
