import { Component, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Skill } from '../skill';
import { Router } from '@angular/router';


const GET_ALL_SKILLS_QUERY = gql`query getAllSkillsQuery {
  getAllSkills {
    _id,
    name,
    level,
    type,
    state,
    createdAt,
    updatedAt,
  }
}`;

const DELETE_SKILL_QUERY = gql`mutation deleteSkillMutation($id: ID!) {
  deleteSkill(id: $id) {
    _id,
    name,
    level,
    type,
    state,
    createdAt,
    updatedAt,
  }
}`;

@Component({
  selector: 'app-list-skill',
  templateUrl: './list-skill.component.html',
  styleUrls: ['./list-skill.component.sass']
})
export class ListSkillComponent implements OnInit {
  allSkills: Skill[] = [];

  constructor(private apollo: Apollo, private router: Router) { }

  ngOnInit() {
    console.log(`--SKILL.LIST-- Get all skills `);
    this.apollo.watchQuery({
      query: GET_ALL_SKILLS_QUERY
    }).valueChanges.subscribe((response) => {
      console.log(`--SKILL.LIST-- Get all skills ${JSON.stringify(response)}`);
      this.allSkills = response.data['getAllSkills'];
      console.log(`--SKILL.LIST-- Get all skill finally ${JSON.stringify(this.allSkills)}`);
    });
  }

  delete(id) {
    console.log(`--DELETE.SKILL-- deleted id ${id}`);
    this.apollo.mutate({
      mutation: DELETE_SKILL_QUERY,
      variables: { id },
      optimisticResponse: {
        __typename: 'Mutation',
        deleteSkill: {
          __typename: 'Skill',
          id: id,
        },
      },
    }).subscribe(
      res => {
        console.log(`--DELETE.SKILL-- submit response ${JSON.stringify(res)}`);
        return this.router.navigate(['/skill/index']);
      },
      err => { console.log(`--DELETE.SKILL-- submit error ${err}`); },
      () => this.router.navigate(['/skill/index'])
    );
  }
}
