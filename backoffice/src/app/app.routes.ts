import { Routes } from '@angular/router';
import { AppComponent } from './app.component';

export const AppRoutes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: ''
  },
  {
    path: '',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'about',
    loadChildren: './about/about.module#AboutModule'
  },
  {
    path: 'information',
    loadChildren: './information/information.module#InformationModule'
  },
  {
    path: 'language',
    loadChildren: './language/language.module#LanguageModule'
  },
  {
    path: 'skill',
    loadChildren: './skill/skill.module#SkillModule'
  },
  {
    path: 'tool',
    loadChildren: './tool/tool.module#ToolModule'
  },
  {
    path: 'experience',
    loadChildren: './experience/experience.module#ExperienceModule'
  },
  {
    path: 'education',
    loadChildren: './education/education.module#EducationModule'
  },
  {
    path: 'reference',
    loadChildren: './reference/reference.module#ReferenceModule'
  },
  {
    path: 'center-of-interest',
    loadChildren: './center-of-interest/center-of-interest.module#CenterOfInterestModule'
  },
  {
    path: 'faq',
    loadChildren: './faq/faq.module#FaqModule'
  },
  {
    path: 'social-network',
    loadChildren: './social-network/social-network.module#SocialNetworkModule'
  },
  {
    path: 'location',
    loadChildren: './location/location.module#LocationModule'
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  },
  {
    path: 'me',
    loadChildren: './me/me.module#MeModule'
  },
  {
    path: 'resume',
    loadChildren: './resume/resume.module#ResumeModule'
  },
  {
    path: 'portfolio',
    loadChildren: './portfolio/portfolio.module#PortfolioModule'
  },
  {
    path: 'category',
    loadChildren: './category/category.module#CategoryModule'
  },
  {
    path: 'blog',
    loadChildren: './blog/blog.module#BlogModule'
  },
  {
    path: 'message',
    loadChildren: './message/message.module#MessageModule'
  }
];
