export class Reference {
  _id: string;
  userPicture: string;
  userName: string;
  userEnterprise: string;
  content: string;
  state: boolean;
}
