import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ReferenceService } from '../reference.service';
import { Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-reference',
  templateUrl: './add-reference.component.html',
  styleUrls: ['./add-reference.component.sass']
})
export class AddReferenceComponent implements OnInit {
  referenceForm: FormGroup;
  data: any = {};
  selectedFiles: FileList;
  currentFileUpload: File;
  uriFile: string;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private referenceService: ReferenceService, private router: Router,
  private formBuilder: FormBuilder, private cd: ChangeDetectorRef) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.referenceForm = this.formBuilder.group({
      userName: ['', Validators.required ],
      userPicture: ['', Validators.required],
      userEnterprise: ['', Validators.required],
      content: ['', Validators.required],
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.referenceService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.uriFile = `${environment.servicePersonal.uri}images/${event.body}`;
        console.log(`File is completely uploaded! event : ${this.uriFile}`);
        this.addReference();
      }
    });

    this.selectedFiles = undefined;
  }

  addReference() {
    const reference = {
      userName: this.data.userName,
      userPicture: this.uriFile,
      userEnterprise: this.data.userEnterprise,
      content: this.data.content,
    };
    this.referenceService.create(reference).subscribe((data: {}) => {
      console.log(`--REFERENCE.ADD-- object referencer = ${JSON.stringify(data)}`);
      this.router.navigate(['/reference/index']);
    });
  }
}
