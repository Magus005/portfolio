import { Routes } from '@angular/router';
import { ListReferenceComponent } from './list-reference/list-reference.component';
import { AddReferenceComponent } from './add-reference/add-reference.component';

export const ReferenceRoutes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'index' },
  { path: 'index', component: ListReferenceComponent },
  { path: 'add', component: AddReferenceComponent },
];
