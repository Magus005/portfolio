import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReferenceRoutes } from './reference.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListReferenceComponent } from './list-reference/list-reference.component';
import { AddReferenceComponent } from './add-reference/add-reference.component';

@NgModule({
  declarations: [
    ListReferenceComponent,
    AddReferenceComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(ReferenceRoutes),
    SharedModule
  ]
})
export class ReferenceModule {}
