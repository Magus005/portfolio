import { Component, OnInit } from '@angular/core';
import { ReferenceService } from '../reference.service';

@Component({
  selector: 'app-list-reference',
  templateUrl: './list-reference.component.html',
  styleUrls: ['./list-reference.component.sass']
})
export class ListReferenceComponent implements OnInit {
  allReferences: any = [];

  constructor(private referenceService: ReferenceService) { }

  ngOnInit() {
    this.listReferences();
  }

  // Get references list
  listReferences() {
    return this.referenceService.find().subscribe((data: {}) => {
      this.allReferences = data;
      console.log(`--REFERENCE.LIST-- list references = ${JSON.stringify(this.allReferences)}`);
    });
  }

  // Delete reference
  delete(id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.referenceService.delete(id).subscribe(data => {
        this.listReferences();
      });
    }
  }

}
